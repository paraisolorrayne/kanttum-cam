package br.com.teachgrowth.androidapp.ui.adapter;

import com.genius.groupie.Item;

import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ItemGaleryBinding;
import br.com.teachgrowth.androidapp.service.model.GaleryModel;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.ItemGaleryViewModel;

/**
 * Created by lucas on 06/03/18.
 */

public class GalleryAdapter extends Item<ItemGaleryBinding> {

    private List<GaleryModel> list;
    private CallbackBasicViewModel callback;
    private String cycle;

    public GalleryAdapter(String cycle, List<GaleryModel> list, CallbackBasicViewModel callback) {
        this.list = list;
        this.callback = callback;
        this.cycle = cycle;
    }

    @Override
    public int getLayout() {
        return R.layout.item_galery;
    }

    @Override
    public void bind(ItemGaleryBinding viewBinding, int position) {
        viewBinding.setViewModel(new ItemGaleryViewModel(cycle, list.get(position), position%3!=2, callback));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
