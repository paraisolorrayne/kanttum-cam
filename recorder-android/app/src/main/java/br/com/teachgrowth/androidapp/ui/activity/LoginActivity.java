package br.com.teachgrowth.androidapp.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ActivityLoginBinding;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.LoginViewModel;

public class LoginActivity extends BaseActivityViewModel<ActivityLoginBinding, LoginViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mViewModel = new LoginViewModel(this);
        mBinding.setViewModel(mViewModel);
    }

    @Override
    public void onBackPressed() {

    }
}
