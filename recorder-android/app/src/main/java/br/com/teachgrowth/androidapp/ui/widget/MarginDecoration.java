package br.com.teachgrowth.androidapp.ui.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MarginDecoration extends RecyclerView.ItemDecoration {
    private int marginVertical, marginHorizontal;

    public MarginDecoration(Context context, int marginHorizontal, int marginVertical) {
        this.marginVertical = marginVertical;
        this.marginHorizontal = marginHorizontal;
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(marginHorizontal, marginVertical, marginHorizontal, marginVertical);
    }
}