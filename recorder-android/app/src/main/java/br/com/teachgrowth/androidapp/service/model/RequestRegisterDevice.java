package br.com.teachgrowth.androidapp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import br.com.teachgrowth.androidapp.service.model.request.ResetPasswordRequest;

/**
 * Created by lucas on 11/01/2018.
 */

public class RequestRegisterDevice implements Serializable {

    @SerializedName("attributes")
    private Attributes attributes;

    public RequestRegisterDevice(String token, String type) {
        this.attributes = new Attributes(token, type);
    }

    public class Attributes {
        @SerializedName("platform")
        @Expose
        private String device_os;

        @SerializedName("token")
        @Expose
        private String device_token;

        public Attributes(String token, String type) {
            this.device_token = token;
            this.device_os = type;
        }

        public String getDevice_os() {
            return device_os;
        }

        public void setDevice_os(String device_os) {
            this.device_os = device_os;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }
    }
}