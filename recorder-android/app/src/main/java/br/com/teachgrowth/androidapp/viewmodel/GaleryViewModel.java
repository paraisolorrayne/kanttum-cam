package br.com.teachgrowth.androidapp.viewmodel;

import com.genius.groupie.GroupAdapter;

import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.service.model.GaleryModel;
import br.com.teachgrowth.androidapp.ui.adapter.GalleryAdapter;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;

/**
 * Created by lucas on 06/03/18.
 */

public class GaleryViewModel extends BaseViewModel {
    public final GroupAdapter groupAdapter = new GroupAdapter();
    private String cicle;

    public GaleryViewModel(String cicle, CallbackBasicViewModel callback) {
        super(callback);
        this.cicle = cicle;
        setList();
    }

    public void setList() {
        List<GaleryModel> list = new ArrayList<>();
        list.addAll(Utils.getVideos());
        groupAdapter.clear();
        groupAdapter.add(new GalleryAdapter(cicle, list, callback));
    }

    public String getCicle() {
        return cicle;
    }
}
