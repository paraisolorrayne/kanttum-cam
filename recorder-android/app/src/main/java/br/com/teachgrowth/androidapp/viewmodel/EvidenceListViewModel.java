package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableFloat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.genius.groupie.GroupAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.fcm.FirebaseService;
import br.com.teachgrowth.androidapp.service.UploadVideoService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.response.AttributesVideoResponse;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.activity.GalleryActivity;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivity;
import br.com.teachgrowth.androidapp.ui.adapter.EvidenceListItem;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucas on 12/03/18.
 */

public class EvidenceListViewModel extends BaseViewModel {
    public final GroupAdapter groupAdapter = new GroupAdapter();
    public ObservableBoolean listIsEmpty = new ObservableBoolean(false);
    public ObservableBoolean showSnackbar = new ObservableBoolean(false);
    CustomCallback callback;
    private boolean firstRun = true;
    private String cycle;
    private List<OfflineEvidenceModel> evidenceModels;
    private List<Integer> itemsSelected = new ArrayList<Integer>();
    private AdapterController adapterController = (position, isSelected) -> updateListItemsState();
    private ObservableBoolean allItemsAlready = new ObservableBoolean(false);

    public ObservableFloat alpha = new ObservableFloat();

    public EvidenceListViewModel(String cycle, CustomCallback callback) {
        super(callback);
        this.callback = callback;
        this.cycle = cycle;
        init();
        FirebaseService service = new FirebaseService();
        if (!TeachGrowthApplication.getInstance().isTokenSaved()) service.registerDevice();
        alpha.set(0.0f);
    }

    private void init() {
        getEvidences();
    }

    private void getEvidences() {
        showProgress();
        RetrofitBase.getInterfaceRetrofit()
                .getVideosEvidence()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::getEvidencesSuccess,
                        this::handlerErrors,
                        () -> hideProgress());
    }

    private void getEvidencesSuccess(DataModel<List<AttributesVideoResponse>, Object, Object> attributesEvidenceResponseListObjectDataModel) {

        List<AttributesVideoResponse> videosOnline = attributesEvidenceResponseListObjectDataModel.getData();
        List<OfflineEvidenceModel> videosOffline = TeachGrowthApplication.getInstance().getEvidences(Constants.Cycle);

        if (videosOnline != null) {
            for (AttributesVideoResponse attributesVideoResponse : videosOnline) {
                boolean isSaved = false;
                for (OfflineEvidenceModel evidenceModel : videosOffline) {
                    if (attributesVideoResponse.getId() == evidenceModel.getId()) {
                        isSaved = true;
                        OfflineEvidenceModel newEvidence = new OfflineEvidenceModel(attributesVideoResponse);
                        TeachGrowthApplication.getInstance().updateEvidence(newEvidence, Constants.Cycle);
                    }
                }
                if (!isSaved) {
                    if (attributesVideoResponse.getVideoResponse().getAvailable())
                        TeachGrowthApplication.getInstance()
                                .saveEvidence(new OfflineEvidenceModel(attributesVideoResponse), Constants.Cycle);
                }
            }
        }

        setList();
        animatedTransitionFadeIn(alpha);
    }

    private void handlerErrors(Throwable t) {
        hideProgress();
        showError(t, (dialog, which) -> {
        });
        setList();
        animatedTransitionFadeIn(alpha);
    }

    public void addEvidence(View view) {
        GalleryActivity.openGalleryActivity((BaseActivity) view.getContext(), cycle);
    }

    public void addEvidence(BaseActivity activity) {
        GalleryActivity.openGalleryActivity(activity, cycle);
    }

    private void setList() {
        evidenceModels = TeachGrowthApplication.getInstance().getEvidences(Constants.Cycle);
        groupAdapter.clear();
        //Applying reverse order to evidence that came from backend.
        Collections.reverse(evidenceModels);
        for (OfflineEvidenceModel evidenceModel : evidenceModels) {
            groupAdapter.add(new EvidenceListItem(cycle, evidenceModel, callback, adapterController));
        }
        listIsEmpty.set(evidenceModels.size() == 0);
        Log.i("teste", groupAdapter.getItemCount() + "");
    }

    public void needHelp(View view) {

    }

    public void updateProgressList(OfflineEvidenceModel evidenceModel) {
        if (evidenceModels != null) {
            for (int i = 0; i < evidenceModels.size(); i++) {
                if (!TextUtils.isEmpty(evidenceModels.get(i).getVideoPath()) && evidenceModels.get(i).getVideoPath().equals(evidenceModel.getVideoPath())) {
                    evidenceModels.get(i).setVideoStatus(evidenceModel.getVideoStatus());
                    EvidenceListItem itemEvidencesViewModel = (EvidenceListItem) groupAdapter.getItem(i);
                    evidenceModels.get(i).setTransferObserverId(evidenceModel.getTransferObserverId());
                    if (itemEvidencesViewModel != null) {
                        itemEvidencesViewModel.updateProgress(evidenceModel);
                    }
                    break;
                }
            }
        }
        UploadVideoService.checkIfAllItemsAlready();
    }

    private void updateListItemsState() {
        if (itemsSelected.size() > 0)
            for (int i = 0; i < groupAdapter.getItemCount(); i++)
                ((EvidenceListItem) groupAdapter.getItem(i)).updateStateSelectedPossibility(true);
        else
            for (int i = 0; i < groupAdapter.getItemCount(); i++)
                ((EvidenceListItem) groupAdapter.getItem(i)).updateStateSelectedPossibility(false);
    }

    Integer searchIfItemHasSelected(int position) {
        for (int i : itemsSelected) {
            if (i == position)
                return position;
        }
        return null;
    }

    public void clearSelectedList() {
        for (int i : itemsSelected) {
            ((EvidenceListItem) groupAdapter.getItem(i)).updateStateSelected(false);
        }
        itemsSelected.clear();
        firstRun = false;
    }

    public boolean hasItemsSelected() {
        return itemsSelected.size() > 0;
    }

    public void deleteSelectedItems() {
        callback.showDialogProgress();
        ArrayList<Integer> aux = new ArrayList<>();
        aux.addAll(itemsSelected);
        for (int i : aux) {
            if (!evidenceModels.get(i).getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHED.getId()) && !evidenceModels.get(i).getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHING.getId())) {
                TeachGrowthApplication.getInstance().deleteEvidence.onNext(evidenceModels.get(i));
                removeItems(i, true, false);
            }
        }

        deleteEvidenceOnLine();
    }

    private void deleteEvidenceOnLine() {
        if (itemsSelected.size() > 0) {
            RetrofitBase.getInterfaceRetrofit()
                    .deleteVideosEvidence(evidenceModels.get(itemsSelected.get(itemsSelected.size() - 1)).getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        if (response.code() == 200) {
                            removeItems(response.body().getMetaResponse().getId(), true, true);
                            deleteEvidenceOnLine();
                        } else {
                            handlerErrorsDelete(null, evidenceModels.get(itemsSelected.get(itemsSelected.size() - 1)).getVideoTitle());
                        }
                    }, throwable -> {
                        handlerErrorsDelete(throwable, evidenceModels.get(itemsSelected.get(itemsSelected.size() - 1)).getVideoTitle());
                    });
        }
    }

    private void handlerErrorsDelete(Throwable throwable, String evidenceName) {
        if (throwable != null)
            throwable.printStackTrace();
        itemsSelected.clear();
        updateListSate();
        callback.showSimpleDialog("Ocorreu um erro ao excluir a evidencia: " + evidenceName);
        callback.hideDialogProgress();
    }

    void removeItems(int i, boolean success, boolean isVideoId) {
        if (isVideoId)
            i = findIndexEvidenceById(i);

        itemsSelected.remove(itemsSelected.indexOf(i));
        if (success)
            TeachGrowthApplication.getInstance().removeEvidence(evidenceModels.get(i), cycle);

        if (itemsSelected.size() == 0)
            updateListSate();
    }

    private void updateListSate() {
        evidenceModels = TeachGrowthApplication.getInstance().getEvidences(Constants.Cycle);
        listIsEmpty.set(evidenceModels.size() == 0);
        setList();
        super.animatedTransitionFadeIn(alpha);
        callback.hideDialogProgress();
    }

    private int findIndexEvidenceById(int id) {
        for (int i : itemsSelected)
            if (evidenceModels.get(i).getId() == id)
                return i;

        return 0;
    }

    public void setUploadInProgress(Boolean allItemsAlready) {
        this.allItemsAlready.set(allItemsAlready);
    }

    public interface AdapterController {
        void onSelectedItem(int position, boolean isSelected);
    }

    public interface CustomCallback extends CallbackBasicViewModel {
    }
}
