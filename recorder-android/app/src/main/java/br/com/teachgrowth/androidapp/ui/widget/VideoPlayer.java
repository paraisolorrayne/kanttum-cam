package br.com.teachgrowth.androidapp.ui.widget;

import android.content.Context;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import java.util.List;

import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.databinding.VideoPlayerLayoutBinding;
import br.com.teachgrowth.androidapp.helper.AnimationsUtils;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.ui.interfaces.OnPlayerIsPreparedListener;

/**
 * Created by lucas on 19/03/18.
 */

public class VideoPlayer extends RelativeLayout implements SeekBar.OnSeekBarChangeListener {

    private static final int REFRESH_RATE = 100;
    VideoPlayerLayoutBinding mBinding;
    ObservableBoolean playerIsPrepared = new ObservableBoolean();

    private MediaPlayer mediaPlayer;

    private List<Point> points;

    Runnable runnable;

    private String uri_video;

    private FullScreenCallback fullScreenCallback;

    private Handler handler;
    private int elapsedTime = 0;

    Runnable getRunnable() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    int total = mediaPlayer.getDuration();
                    mBinding.fullTimeVideo.setText(Utils.formatMilliseconds(String.valueOf(total)));
                    mBinding.seekCurrentPosition.setProgress(mBinding.videoview.getCurrentPosition());
                    elapsedTime = mBinding.videoview.getCurrentPosition();
                    handler.postDelayed(this, REFRESH_RATE);
                    Log.i("teste", "" + mBinding.videoview.getCurrentPosition());
                    mBinding.currentTimeVideo.setText(Utils.formatMilliseconds(String.valueOf(elapsedTime)) + " / ");
                } catch (Exception e) {
                    if (e != null && e.getMessage() != null) {
                        Log.e("VideoatlasPlayActivity", e.getMessage());
                    }
                }
            }
        };

        return runnable;
    }

    public VideoPlayer(Context context) {
        super(context);
    }

    public VideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(String uri_video, FullScreenCallback fullScreenCallback, ObservableBoolean playerIsPrepared) {
        this.playerIsPrepared = playerIsPrepared;
        if (playerIsPrepared != null)
            playerIsPrepared.set(false);
        init(uri_video, fullScreenCallback);
    }

    public Integer getCurrentTimer(){
        return mBinding.seekCurrentPosition.getProgress();
    }

    public void setCurrentTimer(int currentTimer) {
        onProgressChanged(mBinding.seekCurrentPosition, currentTimer, true);
        mBinding.seekCurrentPosition.setProgress(currentTimer);
        mBinding.currentTimeVideo.setText(Utils.formatMilliseconds(String.valueOf(currentTimer)) + " / ");
    }

    public void init(String uri_video, FullScreenCallback fullScreenCallback) {
        this.fullScreenCallback = fullScreenCallback;
        this.uri_video = uri_video;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        mBinding = VideoPlayerLayoutBinding.inflate(inflater, this, true);
        mBinding.setViewModel(this);
        mBinding.videoview.setVideoURI(Uri.parse(uri_video));
        mBinding.videoview.requestFocus();
        elapsedTime = TeachGrowthApplication.getInstance().getVideoCurrentPlaying();
        mBinding.videoview.seekTo((int) elapsedTime);
        mBinding.seekCurrentPosition.setOnSeekBarChangeListener(this);
        mBinding.containerProgress.setVisibility(INVISIBLE);
        mBinding.seekCurrentPosition.setVisibility(INVISIBLE);
        mBinding.videoview.setOnPreparedListener(mp -> {
            mediaPlayer = mp;
            mBinding.seekCurrentPosition.setMax(mediaPlayer.getDuration());
            int total = mediaPlayer.getDuration();
            mBinding.fullTimeVideo.setText(Utils.formatMilliseconds(String.valueOf(total)));
            mBinding.seekCurrentPosition.setProgress((int) elapsedTime);
            mBinding.currentTimeVideo.setText(Utils.formatMilliseconds(String.valueOf(elapsedTime))+" / ");
            if (points != null && points.size() > 0){
                mBinding.progressPointsBar.initPoints(points, getDuration());
            }
            mBinding.containerProgress.setVisibility(VISIBLE);
            mBinding.seekCurrentPosition.setVisibility(VISIBLE);

            if (playerIsPrepared != null) {
                playerIsPrepared.set(true);
            }
            mBinding.progressBar.setVisibility(GONE);

            if (TeachGrowthApplication.isVideoPlaying())
                AnimationsUtils.fadeIn(mBinding.btnPlayPause);
            else
                startVideo();


            int videoWidth = mediaPlayer.getVideoWidth();
            int videoHeight = mediaPlayer.getVideoHeight();
            float videoProportion = (float) videoWidth / (float) videoHeight;
            int screenWidth = mBinding.videoview.getWidth();
            int screenHeight = mBinding.videoview.getHeight();
            float screenProportion = (float) screenWidth / (float) screenHeight;
            android.view.ViewGroup.LayoutParams lp = mBinding.videoview.getLayoutParams();

            if (videoProportion > screenProportion) {
                lp.width = screenWidth;
                lp.height = (int) ((float) screenWidth / videoProportion);
            } else {
                lp.width = (int) (videoProportion * (float) screenHeight);
                lp.height = screenHeight;
            }
            mBinding.videoview.setLayoutParams(lp);


        });

        mBinding.videoview.setOnCompletionListener(mp -> pauseVideo());

    }

    public int getDuration() {
        if (mediaPlayer != null)
            return mediaPlayer.getDuration();

        return 0;
    }

    public String getPath() {
        return uri_video;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
        if (playerIsPrepared.get() && points != null && points.size() > 0) {
            mBinding.progressPointsBar.initPoints(points, getDuration());
        }
    }

    private void pauseVideo() {
        if (handler != null)
            handler.removeCallbacks(runnable);
        handler = null;

        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
        AnimationsUtils.fadeIn(mBinding.btnPlayPause);
    }

    public void startVideo() {
        AnimationsUtils.fadeOut(mBinding.btnPlayPause);
        mediaPlayer.start();

        handler = new Handler();
        handler.postDelayed(getRunnable(), REFRESH_RATE);
    }

    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    public void clickPlayPause(View view) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying())
                pauseVideo();
            else
                startVideo();
        }
    }

    public void onClickVolume(View view) {
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && mediaPlayer != null) {
            mediaPlayer.seekTo(progress);
            if (handler != null)
                handler.removeCallbacks(runnable);
            handler = null;

        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        handler = new Handler();
        handler.postDelayed(getRunnable(), REFRESH_RATE);
    }

    public void openCloseFullScreen(View view){
        if (fullScreenCallback != null) {
            fullScreenCallback.onCLickFullScreen();
            TeachGrowthApplication.getInstance().setVideoPlaying(mediaPlayer.isPlaying());
        }
    }

    public boolean getPlayerIsPrepared() {
        return playerIsPrepared.get();
    }

    public void setOnPlayerIsPreparedListener(OnPlayerIsPreparedListener onPlayerIsPreparedListener) {
        playerIsPrepared.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (playerIsPrepared.get() && onPlayerIsPreparedListener != null)
                    onPlayerIsPreparedListener.isPrepared();
            }
        });
    }

    public void onPause() {
        if (handler != null)
            handler.removeCallbacks(runnable);
        handler = null;
        if (mediaPlayer != null && playerIsPrepared.get()) {
            TeachGrowthApplication.getInstance().setVideoCurrentPlaying(getCurrentTimer());

            try {
                TeachGrowthApplication.getInstance().setVideoPlaying(isPlaying());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onDestroy() {
        TeachGrowthApplication.getInstance().setVideoPlaying(false);
        TeachGrowthApplication.getInstance().setVideoCurrentPlaying(0);
    }

    public interface FullScreenCallback {
        void onCLickFullScreen();
    }

    public class ViewModel {

    }
}
