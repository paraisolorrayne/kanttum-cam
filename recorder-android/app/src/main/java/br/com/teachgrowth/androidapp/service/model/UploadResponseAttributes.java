package br.com.teachgrowth.androidapp.service.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by lucas on 03/04/18.
 */
@Parcel(Parcel.Serialization.BEAN)
public class UploadResponseAttributes {

    @SerializedName("output_file_name")
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
