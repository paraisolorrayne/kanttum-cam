package br.com.teachgrowth.androidapp.service.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 26/04/18.
 */

public class IncludedCommentsModel {
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("attributes")
    private Attributes attributes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getAttributesTitle() {
        return attributes.getTitle();
    }

    public void setAttributesTitle(String title) {
        this.attributes.setTitle(title);
    }

    public String getAttributesName() {
        return attributes.getName();
    }

    public void setAttributesName(String name) {
        this.attributes.setName(name);
    }

    public String getAttributesThumbUrl() {
        return attributes.getThumbUrl();
    }

    public void setAttributesThumbUrl(String ThumbUrl) {
        this.attributes.setThumbUrl(ThumbUrl);
    }

    private class Attributes {
        @SerializedName("title")
        private String title;
        @SerializedName("name")
        private String name;
        @SerializedName("thumb_url")
        private String thumbUrl;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getThumbUrl() {
            return thumbUrl;
        }

        public void setThumbUrl(String thumbUrl) {
            this.thumbUrl = thumbUrl;
        }
    }

}
