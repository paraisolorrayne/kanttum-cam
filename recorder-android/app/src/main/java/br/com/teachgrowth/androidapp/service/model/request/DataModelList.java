package br.com.teachgrowth.androidapp.service.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by lucas on 02/03/18.
 */

public class DataModelList<T, E, I> extends Object {

    @SerializedName("data")
    ArrayList<T> data;

    @SerializedName("included")
    E include;

    @SerializedName("meta")
    I meta;

    public DataModelList(ArrayList<T> data) {
        this.data = data;
    }

    public DataModelList(ArrayList<T> data, E include) {
        this.data = data;
        this.include = include;
    }

    public DataModelList(ArrayList<T> data, E include, I meta) {
        this.data = data;
        this.include = include;
        this.meta = meta;
    }

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }

    public E getInclude() {
        return include;
    }

    public void setInclude(E include) {
        this.include = include;
    }

    public I getMeta() {
        return meta;
    }

    public void setMeta(I meta) {
        this.meta = meta;
    }
}
