package br.com.teachgrowth.androidapp.ui.activity.base;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.helper.AnimationsUtils;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceListActivity;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceUploadActivity;
import br.com.teachgrowth.androidapp.ui.activity.SettingsActivity;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gabrielaraujo on 17/11/2017.
 */

public class BaseActivity extends AppCompatActivity implements CallbackBasicViewModel, NavigationView.OnNavigationItemSelectedListener {

    protected Toolbar toolbar;
    protected DrawerLayout drawer;
    private FrameLayout progressBar;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initDialogProgress() {
        progressBar = findViewById(R.id.progress_bar);
    }

    private void setUpToolbar(boolean isBack) {
        toolbar = findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            if (isBack) {
                toolbar.setNavigationIcon(R.drawable.icon_arrow_back_white);
                toolbar.setNavigationOnClickListener(v -> onBackPressed());

            } else {
                toolbar.setNavigationIcon(null);
            }
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setUpVideoToolbar() {
        toolbar = findViewById(R.id.details_toolbar);
        if (toolbar != null) {
            assert getSupportActionBar() != null;
            toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    public void setUpToolbar(boolean isBack, String text) {
        setUpToolbar(isBack);
        TextView textTitle = toolbar.findViewById(R.id.text_title_toolbar);
        textTitle.setText(text);
        textTitle.setVisibility(View.VISIBLE);
    }

    public void setUpToolbar(boolean isBack, int resTitle) {
        setUpToolbar(isBack, getString(resTitle));
    }

    public void setUpDrawerMenu(int idChecked) {
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(idChecked);
        View header = navigationView.getHeaderView(0);

        if (drawer != null) {
            toggle = new ActionBarDrawerToggle(
                    this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.icon_menu);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setUpUserDetailHeader(header);
    }

    private void setUpUserDetailHeader(View header) {

        ImageView userImg = header.findViewById(R.id.img_user);
        TextView userName = header.findViewById(R.id.name_user);
        TextView userEmial = header.findViewById(R.id.email_user);
        TextView userInitial = header.findViewById(R.id.img_user_initial);
        userName.setText(TeachGrowthApplication.getInstance().getUserDetail().getAttributes().getName());
        userEmial.setText(TeachGrowthApplication.getInstance().getUserDetail().getAttributes().getEmail());
        setUserImgOrInitial(userImg, userInitial);

    }

    public void setUserImgOrInitial(ImageView userImg, TextView userInitial) {

        if (TeachGrowthApplication.getInstance()
                .getUserDetail().getAttributes().getThumbUrl() != null) {

            userInitial.setVisibility(View.GONE);
            userImg.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(TeachGrowthApplication.getInstance()
                            .getUserDetail().getAttributes().getThumbUrl())
                    .into(userImg);
        } else {
            userImg.setVisibility(View.GONE);
            userInitial.setVisibility(View.VISIBLE);
            userInitial.setText(getInitialOfUser(TeachGrowthApplication.getInstance()
                    .getUserDetail().getAttributes().getName()));
        }
    }

    private String getInitialOfUser(String name) {
        StringBuilder initial = new StringBuilder();
        String[] arrayName = name.split(" ");

        if (arrayName.length > 1) {
            for (int i = 0; i < arrayName.length; i++) {
                if (i == 0 || i == arrayName.length - 1)
                    initial.append(arrayName[i].charAt(0));
            }
        } else {
            String firstName = arrayName[0];
            initial = new StringBuilder(String.valueOf(firstName.charAt(0)) +
                    String.valueOf(firstName.charAt(1)));
        }

        return initial.toString().toUpperCase();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle != null) {
            if (toggle.onOptionsItemSelected(item)) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void openActivity(Class<?> openActivity) {
        Intent intent = new Intent();
        intent.setClass(this, openActivity);
        startActivity(intent);
    }

    public void openActivity(Class<?> openActivity, String name, String value) {
        Intent intent = new Intent();
        intent.putExtra(name, value);
        intent.setClass(this, openActivity);
        startActivity(intent);
    }

    @Override
    public void openActivityNewTask(Class<?> openActivity) {
        Intent intent = new Intent();
        intent.setClass(this, openActivity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overrideTransition();
    }

    private void overrideTransition() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public MaterialDialog getBasicMaterialDialog() {
        return new MaterialDialog.Builder(this).build();
    }

    @Override
    public void showDialogFragment(DialogFragment dialogFragment) {
        dialogFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void finish(int resultCode) {
        setResult(resultCode);
        finish();
    }

    @Override
    public void finish(int resultCode, Bundle bundle) {
        getIntent().putExtras(bundle);
        setResult(resultCode, getIntent());
        finish();
    }

    @Override
    public void showSimpleDialog(String msg) {
        Utils.showSimpleDialog(this, msg, (dialog, which) -> {
        });
    }

    @Override
    public void showSimpleDialog(int title, int text, int textPositive, int textNegative, MaterialDialog.SingleButtonCallback callback) {
        Utils.showSimpleDialog(this, title, text, textPositive, textNegative, callback);
    }

    @Override
    public void showSimpleDialog(int text, int textPositive, int textNegative, MaterialDialog.SingleButtonCallback callback) {
        Utils.showSimpleDialog(this, text, textPositive, textNegative, callback);
    }

    @Override
    public void showSimpleDialog(int msg, MaterialDialog.SingleButtonCallback callback) {
        Utils.showSimpleDialog(this, msg, callback);
    }

    @Override
    public void showDialogProgress() {
        if (progressBar == null) initDialogProgress();
        AnimationsUtils.fadeIn(progressBar);
    }

    @Override
    public void hideDialogProgress() {
        try {
            if (progressBar == null) initDialogProgress();
            AnimationsUtils.fadeOut(progressBar);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showError(Throwable t, MaterialDialog.SingleButtonCallback callback) {
        Utils.showDialogError(this, t, callback);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.isChecked()) {
            drawer.closeDrawers();
            return false;
        }

        boolean handled = false;

        switch (item.getItemId()) {

            case R.id.drawer_video_library:
                openActivityNewTask(EvidenceListActivity.class);
                handled = true;
                break;

            case R.id.drawer_uploads:
                openActivityNewTask(EvidenceUploadActivity.class);
                handled = true;
                break;

            case R.id.drawer_settings:
                openActivityNewTask(SettingsActivity.class);
                handled = true;
                break;

            case R.id.drawer_logout:
                TeachGrowthApplication.getInstance().logoutUser();
                handled = true;
                break;

//            case R.id.drawer_feedback:
//                openUrlOnBrowser();
//                handled = true;
//                break;

//            case R.id.drawer_about_growth:
//                openUrlOnBrowser();
//                handled = true;
//                break;

            case R.id.drawer_about_kanttum:
                openUrlOnBrowser();
                handled = true;
                break;
        }

        item.setChecked(handled);
        drawer.closeDrawers();

        return handled;
    }

    private void openUrlOnBrowser() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(br.com.teachgrowth.androidapp.app.Constants.Urls.ABOUT_KANTTUM));
        startActivity(browserIntent);
    }
}
