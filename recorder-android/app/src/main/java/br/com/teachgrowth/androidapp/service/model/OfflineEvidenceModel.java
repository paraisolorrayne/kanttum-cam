package br.com.teachgrowth.androidapp.service.model;

import org.parceler.Parcel;

import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.service.model.response.AttributesVideoResponse;
import br.com.teachgrowth.androidapp.service.model.response.UploadResponse;

/**
 * Created by lucas on 21/03/18.
 */
@Parcel(Parcel.Serialization.BEAN)
public class OfflineEvidenceModel {

    public String timeDuration;
    private Long countPartOne;
    private Long timePartOne;
    private int id;
    private String videoTitle;
    private String videoDescription;
    private int videoDuration;
    private String videoPath;
    private String videoUrl;
    private String videoStatus;
    private int percentUploaded;
    private UploadResponse amazonInformations;
    private int transferObserverId;
    private String thumbUrl;

    public OfflineEvidenceModel() {
    }

    public OfflineEvidenceModel(AttributesVideoResponse attributesVideoResponse) {
        this.id = attributesVideoResponse.getId();
        this.videoUrl = attributesVideoResponse.getVideoResponse().getUrl();
        this.videoDuration = attributesVideoResponse.getVideoResponse().getDuration();
        this.videoTitle = attributesVideoResponse.getVideoResponse().getTitle();
        this.videoDescription = attributesVideoResponse.getVideoResponse().getDescription();
        this.thumbUrl = attributesVideoResponse.getVideoResponse().getThumbUrl();
        if (attributesVideoResponse.getVideoResponse().getAvailable()) {
            this.videoStatus = EvidenceStatusEnum.STATUS_FINISHED.getId();
        }else {
            this.videoStatus = EvidenceStatusEnum.STATUS_FINISHING.getId();
        }
    }

    public OfflineEvidenceModel(String videoPath, int videoDuration, String videoName, String videoDescription, String videoStatus) {
        this.videoPath = videoPath;
        this.videoDuration = videoDuration;
        this.videoTitle = videoName;
        this.videoDescription = videoDescription;
        this.videoStatus = videoStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoStatus() {
        return videoStatus;
    }

    public void setVideoStatus(String videoStatus) {
        this.videoStatus = videoStatus;
    }

    public UploadResponse getAmazonInformations() {
        return amazonInformations;
    }

    public void setAmazonInformations(UploadResponse amazonInformations) {
        this.amazonInformations = amazonInformations;
    }

    public int getPercentUploaded() {
        return percentUploaded;
    }

    public void setPercentUploaded(int percentUploaded) {
        this.percentUploaded = percentUploaded;
    }

    public int getTransferObserverId() {
        return transferObserverId;
    }

    public void setTransferObserverId(int transferObserverId) {
        this.transferObserverId = transferObserverId;
    }

    public Long getCountPartOne() {
        return countPartOne;
    }

    public void setCountPartOne(Long countPartOne) {
        this.countPartOne = countPartOne;
    }

    public Long getTimePartOne() {
        return timePartOne;
    }

    public void setTimePartOne(Long timePartOne) {
        this.timePartOne = timePartOne;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }
}
