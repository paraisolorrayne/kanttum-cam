package br.com.teachgrowth.androidapp.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingListener;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;

import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.CustomEditTextBinding;

@InverseBindingMethods({
        @InverseBindingMethod(type = EditTextCustom.class, attribute = "text"),
        @InverseBindingMethod(type = EditTextCustom.class, attribute = "error"),
})
public class EditTextCustom extends RelativeLayout {

    CustomEditTextBinding mBinding;
    String initialHint, initialText, msgError;
    int floatingLabelColor;
    int inputType;
    int imeOptions;
    int maxLength;
    int gravity;
    boolean isEnabled;

    private final List<OnTextChangedListener> mTextListeners = new ArrayList<>();
    private final List<OnErrorChangedListener> mErrorListeners = new ArrayList<>();

    private ViewModel viewModel;

    public EditTextCustom(Context context) {
        super(context);
        init(context);
    }

    public EditTextCustom(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs);
        init(context);
    }

    public EditTextCustom(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributes(context, attrs);
        init(context);
    }

    public void setText(String text) {
        viewModel.text.set(text);
        viewModel.getTextError().set("");
    }

    public String getText() {
        return viewModel.text.get();
    }

    public void setHint(String text) {
        viewModel.getHint().set(text);
    }

    public void setMask(TextWatcher textWatcher) {
        mBinding.edtField.addTextChangedListener(textWatcher);
    }

    public void setError(String msgError) {
        viewModel.getTextError().set(msgError);
        if (!TextUtils.isEmpty(msgError)) {
            mBinding.edtField.requestFocus();
        }
        viewModel.getErrorVisibility().set(TextUtils.isEmpty(msgError));
    }

    public void setInputType(int inputType) {
        viewModel.getInputType().set(inputType);
    }

    @BindingAdapter(value = {"onTextChange", "textAttrChanged"}, requireAll = false)
    public static void setTextListener(EditTextCustom view,
                                       final OnTextChangedListener listener,
                                       final InverseBindingListener textChange) {
        if (textChange == null) {
            view.setOnTextChangeListener(listener);
        } else {
            view.setOnTextChangeListener((view1, color) -> {
                if (listener != null) {
                    listener.onTextChange(view1, color);
                }
                textChange.onChange();
            });
        }
    }

    @BindingAdapter(value = {"onErrorChange", "errorAttrChanged"}, requireAll = false)
    public static void setErrorListener(EditTextCustom view,
                                        final OnErrorChangedListener listener,
                                        final InverseBindingListener textChange) {
        if (textChange == null) {
            view.setOnErrorChangeListener(listener);
        } else {
            view.setOnTextChangeListener((view1, color) -> {
                if (listener != null) {
                    listener.onErrorChange(view1, color);
                }
                textChange.onChange();
            });
        }
    }

    public void setOnErrorChangeListener(OnErrorChangedListener listener) {
        if (listener != null)
            mErrorListeners.add(listener);
        else mErrorListeners.clear();
    }

    public void setOnTextChangeListener(OnTextChangedListener listener) {
        if (listener != null)
            mTextListeners.add(listener);
        else mTextListeners.clear();
    }

    public interface OnTextChangedListener {
        void onTextChange(EditTextCustom editText, String newText);
    }

    public interface OnErrorChangedListener {
        void onErrorChange(EditTextCustom editText, String error);
    }

    private void init(Context context) {
        if (!isInEditMode()) {
            LayoutInflater inflater = LayoutInflater.from(context);
            mBinding = CustomEditTextBinding.inflate(inflater, this, true);
            viewModel = new ViewModel(initialText, initialHint, msgError, inputType);
            mBinding.setViewModel(viewModel);

            mBinding.edtField.setFocusFraction(1f);

            if (!TextUtils.isEmpty(initialHint)) {
                mBinding.edtField.setFloatingLabelText(initialHint);
                mBinding.edtField.setFloatingLabelTextColor(ContextCompat.getColor(context, floatingLabelColor));
            }

            if (inputType != -1) {
                mBinding.edtField.setInputType(inputType);
            }

            if (imeOptions != -1) {
                mBinding.edtField.setImeOptions(imeOptions);
            }

            if (maxLength != -1) {
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(maxLength);
                mBinding.edtField.setFilters(fArray);
            }

            if (gravity != -1) {
                mBinding.edtField.setGravity(gravity);
                mBinding.txtError.setGravity(gravity);
            }

            if (!isEnabled) {
                mBinding.edtField.setFocusableInTouchMode(false);
            }
            return;
        }
    }

    private void parseAttributes(Context context, AttributeSet attributeSet) {
        TypedArray attributes = context
                .getTheme()
                .obtainStyledAttributes(attributeSet, R.styleable.EditTextCustom, 0, 0);

        initialHint = attributes.getString(R.styleable.EditTextCustom_hint);
        initialText = attributes.getString(R.styleable.EditTextCustom_text);
        floatingLabelColor = attributes.getInteger(R.styleable.EditTextCustom_floatingLabelColor, R.color.green_blue);
        msgError = attributes.getString(R.styleable.EditTextCustom_error);
        inputType = attributes.getInt(R.styleable.EditTextCustom_inputType, EditorInfo.TYPE_CLASS_TEXT);
        imeOptions = attributes.getInt(R.styleable.EditTextCustom_imeOptions, EditorInfo.IME_ACTION_NEXT);
        maxLength = attributes.getInteger(R.styleable.EditTextCustom_maxLength, -1);
        gravity = attributes.getInteger(R.styleable.EditTextCustom_android_gravity, Gravity.LEFT);
        isEnabled = attributes.getBoolean(R.styleable.EditTextCustom_enable, true);
        attributes.recycle();
    }

    public MaterialEditText getEditText() {
        return mBinding.edtField;
    }

    public class ViewModel {

        private final ObservableField<String> text;
        private final ObservableField<String> mHintText;
        private final ObservableField<String> mTextError;
        private final ObservableField<Boolean> errorVisibility = new ObservableField<>(false);
        private ObservableInt mInputType;

        public ViewModel(String fieldText, String hintText, String errorText, int inputType) {
            text = new ObservableField<>(fieldText);
            mHintText = new ObservableField<>(hintText);
            mTextError = new ObservableField<>(errorText);
            mInputType = new ObservableInt(inputType);
            text.addOnPropertyChangedCallback(
                    new Observable.OnPropertyChangedCallback() {
                        @Override
                        public void onPropertyChanged(Observable observable, int i) {

                            for (OnTextChangedListener listener : mTextListeners)
                                listener.onTextChange(EditTextCustom.this, text.get());
                        }
                    });

            mTextError.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
                @Override
                public void onPropertyChanged(Observable observable, int i) {

                    for (OnErrorChangedListener listener : mErrorListeners) {
                        listener.onErrorChange(EditTextCustom.this, mTextError.get());
                    }

                }
            });
            errorVisibility.set(false);
        }

        public ObservableField<String> getText() {
            return text;
        }

        public ObservableField<String> getHint() {
            return mHintText;
        }

        public ObservableField<String> getTextError() {
            return mTextError;
        }

        public ObservableField<Boolean> getErrorVisibility() {
            return errorVisibility;
        }

        public ObservableInt getInputType() {
            return mInputType;
        }

    }
}
