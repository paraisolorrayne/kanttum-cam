package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableField;
import android.os.Handler;

import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.subjects.PublishSubject;

/**
 * Created by lucas on 05/03/18.
 */

public class RecoverPasswordSuccessViewModel extends BaseViewModel {

    public final ObservableField<String> msgSucces = new ObservableField<>();
    public final ObservableField<String> msgTimer = new ObservableField<>();
    public final PublishSubject<Integer> onTimer = PublishSubject.create();
    private int intTimer = 6;
    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            intTimer--;
            onTimer.onNext(intTimer);

            timerHandler.postDelayed(this, 1000);
        }
    };

    public RecoverPasswordSuccessViewModel(String emailMsg, CallbackBasicViewModel callback) {
        super(callback);
        msgSucces.set(emailMsg);
        timerHandler.postDelayed(timerRunnable, 0);
    }

    public Handler getTimerHandler() {
        return timerHandler;
    }

    public Runnable getTimerRunnable() {
        return timerRunnable;
    }
}
