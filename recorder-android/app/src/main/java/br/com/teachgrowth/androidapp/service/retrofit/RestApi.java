package br.com.teachgrowth.androidapp.service.retrofit;

import java.util.List;

import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.service.model.AttributesMetaResponse;
import br.com.teachgrowth.androidapp.service.model.IncludedCommentsModel;
import br.com.teachgrowth.androidapp.service.model.RequestRegisterDevice;
import br.com.teachgrowth.androidapp.service.model.request.AttributeLoginRequest;
import br.com.teachgrowth.androidapp.service.model.request.AttributeUploadRequest;
import br.com.teachgrowth.androidapp.service.model.request.AttributesCreateVideoRequest;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.request.DataModelList;
import br.com.teachgrowth.androidapp.service.model.request.ResetPasswordRequest;
import br.com.teachgrowth.androidapp.service.model.response.AttributesCommentsResponse;
import br.com.teachgrowth.androidapp.service.model.response.AttributesTokenResponse;
import br.com.teachgrowth.androidapp.service.model.response.AttributesVideoResponse;
import br.com.teachgrowth.androidapp.service.model.response.UploadResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserDetailResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserModel;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface RestApi {

    @Headers({"Content-Type: application/vnd.api+json"})
    @POST(Constants.Service.LOGIN)
    Observable<DataModel<UserModel, Object, Object>> login(@Body DataModel<AttributeLoginRequest, Object, Object> loginRequest);

    @Headers({"Content-Type: application/vnd.api+json"})
    @POST(Constants.Service.RESET_PASSWORD)
    Observable<DataModel<UserModel, Object, Object>> resetPassord(@Body DataModel<ResetPasswordRequest, Object, Object> loginRequest);

    @Headers({"Content-Type: application/vnd.api+json"})
    @POST(Constants.Service.UPLOAD)
    Observable<DataModel<UploadResponse, Object, Object>> uploads(@Body DataModel<AttributeUploadRequest, Object, Object> uploadRequestDataModel);

    @Headers({"Content-Type: application/vnd.api+json"})
    @POST(Constants.Service.CREATE_VIDEO)
    Observable<DataModel<AttributesVideoResponse, Object, Object>> createVideo(
            @Body DataModel<AttributesCreateVideoRequest, Object, Object>
                    attributesCreateVideoRequestDataModel);

    @Headers({"Content-Type: application/vnd.api+json"})
    @POST(Constants.Service.REGISTER_DEVICE)
    Observable<DataModel<AttributesTokenResponse, Object, Object>> registerDevice(
            @Body DataModel<RequestRegisterDevice, Object, Object> requestRegisterDevice);

    @Headers({"Content-Type: application/vnd.api+json"})
    @GET(Constants.Service.EVIDENCES_VIDEOS)
    Observable<DataModel<List<AttributesVideoResponse>, Object, Object>> getVideosEvidence();

    @Headers({"Content-Type: application/vnd.api+json"})
    @GET(Constants.Service.GET_USER_DETAIL)
    Observable<DataModel<UserDetailResponse, Object, Object>> getUser();


    @Headers({"Content-Type: application/vnd.api+json"})
    @DELETE(Constants.Service.DELETE_VIDEO)
    Observable<Response<AttributesMetaResponse>> deleteVideosEvidence(@Path("id") int videoId);

    @Headers({"Content-Type: application/vnd.api+json"})
    @DELETE(Constants.Service.DELETE_TOKEN_DIVICE)
    Observable<Response<AttributesMetaResponse>> deleteTokenDivice(@Path("id") int tokenId);

    @Headers({"Content-Type: application/vnd.api+json"})
    @GET(Constants.Service.GET_VIDEO)
    Observable<DataModel<AttributesVideoResponse, Object, AttributesMetaResponse.MetaResponse>> getVideo(
            @Path("id") int videoId);

    @Headers({"Content-Type: application/vnd.api+json"})
    @GET(Constants.Service.CREATE_VIDEO)
    Observable<DataModelList<AttributesVideoResponse, Object, AttributesMetaResponse.MetaResponse>> getVideoInformation(
            @Query("filter[file_name][eq]") String videoId);

    @Headers({"Content-Type: application/vnd.api+json"})
    @GET(Constants.Service.GET_VIDEO_COMMENTS)
    Observable<DataModel<List<AttributesCommentsResponse>,
            List<IncludedCommentsModel>, Object>> getVideoComments(
            @Path("institution_id") String instituitionId,
            @Path("evidence_id") String evidenceId, @Path("id") int videoId);

}
