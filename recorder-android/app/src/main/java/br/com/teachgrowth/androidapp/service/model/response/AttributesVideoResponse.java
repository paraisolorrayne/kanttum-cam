package br.com.teachgrowth.androidapp.service.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lucas on 12/04/18.
 */

public class AttributesVideoResponse {

    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("attributes")
    private VideoResponse videoResponse;
    @SerializedName("relationships")
    private Relationships relationships;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public VideoResponse getVideoResponse() {
        return videoResponse;
    }

    public void setVideoResponse(VideoResponse videoResponse) {
        this.videoResponse = videoResponse;
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

    public int getFeedbackAmount() {
        return getRelationships().getComments().getData().size();
    }

    public class VideoResponse {
        @SerializedName("title")
        private String title;
        @SerializedName("description")
        private String description;
        @SerializedName("duration")
        private int duration;
        @SerializedName("available")
        private boolean available;
        @SerializedName("played")
        private boolean played;
        @SerializedName("url")
        private String url;
        @SerializedName("created_at")
        private String created_at;
        @SerializedName("thumb_url")
        private String thumbUrl;

        public VideoResponse(String title, String description, int duration,
                             boolean available, boolean played, String url, String created_at, String thumbUrl) {
            this.title = title;
            this.description = description;
            this.duration = duration;
            this.available = available;
            this.played = played;
            this.url = url;
            this.created_at = created_at;
            this.thumbUrl = thumbUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public boolean getAvailable() {
            return available;
        }

        public void setAvailable(boolean available) {
            this.available = available;
        }

        public boolean getPlayed() {
            return played;
        }

        public void setPlayed(boolean played) {
            this.played = played;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getThumbUrl() {
            return thumbUrl;
        }

        public void setThumbUrl(String thumbUrl) {
            this.thumbUrl = thumbUrl;
        }
    }

    private class Relationships {
        @SerializedName("comments")
        Comments comments;

        public Comments getComments() {
            return comments;
        }

        public void setComments(Comments comments) {
            this.comments = comments;
        }

        private class Comments {
            @SerializedName("data")
            List<Data> data;

            public List<Data> getData() {
                return data;
            }

            public void setData(List<Data> data) {
                this.data = data;
            }

            private class Data {
                @SerializedName("id")
                private int id;
                @SerializedName("type")
                private String type;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }
            }
        }
    }
}
