package br.com.teachgrowth.androidapp.viewmodel;

import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;

/**
 * Created by lucas on 05/04/18.
 */

public class VideoFullScreenViewModel extends BaseViewModel {
    private String videoPath;
    private OfflineEvidenceModel evidenceModel;

    public VideoFullScreenViewModel(OfflineEvidenceModel evidenceModel, CallbackBasicViewModel callback) {
        super(callback);
        this.evidenceModel = evidenceModel;
    }

    public VideoFullScreenViewModel(String videoPath, CallbackBasicViewModel callback) {
        super(callback);
        this.videoPath = videoPath;
    }

    public String getVideoPath() {
        if (evidenceModel == null) {
            return videoPath;
        } else {
            if (evidenceModel.getVideoPath() == null) {
                return evidenceModel.getVideoUrl();
            }
            return evidenceModel.getVideoPath();
        }
    }
}
