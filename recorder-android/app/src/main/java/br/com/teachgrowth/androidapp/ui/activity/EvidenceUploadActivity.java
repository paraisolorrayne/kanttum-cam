package br.com.teachgrowth.androidapp.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.databinding.ActivityEvidenceUploadBinding;
import br.com.teachgrowth.androidapp.helper.RxJavaUtils;
import br.com.teachgrowth.androidapp.service.UploadVideoService;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.EvidenceUploadViewModel;
import rx.Subscription;

import static br.com.teachgrowth.androidapp.helper.Utils.isMyServiceRunning;
import static br.com.teachgrowth.androidapp.helper.Utils.thereIsConnection;

public class EvidenceUploadActivity extends BaseActivityViewModel<ActivityEvidenceUploadBinding, EvidenceUploadViewModel> implements EvidenceUploadViewModel.CustomCallback {

    Menu menu;
    private Subscription subscriptionEvidence;
    private Subscription allItemsAlreadySubject;
    private Subscription subscription;
    private String cicle = Constants.Cycle;
    private boolean isDialogVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_evidence_upload);
        subscribeSubjects();
        mViewModel = new EvidenceUploadViewModel(cicle, this);
        mBinding.setViewModel(mViewModel);

        setUpToolbar(false, R.string.title_activity_evidence_upload);
        setUpDrawerMenu(R.id.drawer_uploads);

        if (!isMyServiceRunning(UploadVideoService.class, this)) {
            Log.d("TESTETAREFA", "SERVIÇO CRIADO ONCREATE");
            startService(new Intent(getApplicationContext(), UploadVideoService.class));
        }

        int notificationId = getIntent().getIntExtra("notificationId", -1);
        if (notificationId != -1)
            NotificationManagerCompat.from(this).cancel("notification", notificationId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeSubjects();
        startDownloadIfDoesnt();
        mViewModel.clearSelectedList();
        mViewModel.setList();
    }

    private void startDownloadIfDoesnt() {
        if (isMyServiceRunning(UploadVideoService.class, this)) {
            Log.d("TESTETAREFA", "resume next");
            mViewModel.startDownloadIfDoesnt();
        } else {
            Log.d("TESTETAREFA", "SERVIÇO CRIADO VM EVIDENCES");
            this.startService(new Intent(this, UploadVideoService.class));
        }

    }

    private void subscribeSubjects() {
        RxJavaUtils.checkUnsubscribe(subscriptionEvidence);
        RxJavaUtils.checkUnsubscribe(allItemsAlreadySubject);
        RxJavaUtils.checkUnsubscribe(subscription);

        TeachGrowthApplication.getInstance().evidenceSubject.subscribe(evidenceModel -> {
            if (mViewModel != null) {
                mViewModel.updateProgressList(evidenceModel);
                runOnUiThread(() -> mViewModel.removeCompletedUploadFromList(evidenceModel));
            }
        });

        allItemsAlreadySubject = TeachGrowthApplication.getInstance().allItemsAlreadySubject.subscribe(isAlready -> {
            if (mViewModel != null)
                mViewModel.setUploadInProgress(isAlready);
            if (!isAlready && !thereIsConnection(this) && !isDialogVisible) {
                isDialogVisible = true;
                //Check for only wifi
                if (!TeachGrowthApplication.getInstance().isUserPreferenceSettingsAllow3g()) {
                    showSimpleDialog(R.string.waiting_wifi2, R.string.wifi_message, R.string.exit, R.string.settings, (dialog, which) -> {
                        if (which == DialogAction.NEGATIVE) {
                            openWifiSettings();
                            dialog.dismiss();
                        }
                        isDialogVisible = false;
                    });
                    //Check for every connection
                } else {
                    showSimpleDialog(R.string.waiting_connection, R.string.no_connection_message, R.string.exit, R.string.settings, ((dialog, which) -> {
                        if (which == DialogAction.NEGATIVE) {
                            openWifiSettings();
                            dialog.dismiss();
                        }
                    }));
                }

            }
        });

        subscription = TeachGrowthApplication.getInstance()
                .evidenceResumeSubject
                .subscribe(offlineEvidenceModel -> {
                    if (!thereIsConnection(this) && !isDialogVisible) {
                        isDialogVisible = true;
                        if (!TeachGrowthApplication.getInstance().isUserPreferenceSettingsAllow3g()) {
                            showSimpleDialog(R.string.waiting_wifi2, R.string.wifi_message, R.string.exit, R.string.settings, (dialog, which) -> {
                                if (which == DialogAction.NEGATIVE) {
                                    openWifiSettings();
                                    dialog.dismiss();
                                }
                            });
                        } else {
                            showSimpleDialog(R.string.waiting_connection, R.string.no_connection_message, R.string.exit, R.string.settings, ((dialog, which) -> {
                                if (which == DialogAction.NEGATIVE) {
                                    openWifiSettings();
                                    dialog.dismiss();
                                }
                            }));
                        }
                        isDialogVisible = false;
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish_step:
                showSimpleDialog(R.string.finish_step, R.string.finish_step_message, R.string.finish, R.string.cancel, (dialog, which) -> {
                    //Todo do request
                    Log.d("TESTETAREFA", "ON OPTIONS");
                });
                break;
            case R.id.next_step:

                break;
            case R.id.tutorial:
                TeachGrowthApplication.getInstance().logoutUser();
                break;

            case R.id.delete:
                showSimpleDialog(R.string.confirm_delete, R.string.del, R.string.cancel, (dialog, which) -> {
                    if (which == DialogAction.POSITIVE)
                        mViewModel.deleteSelectedItems();
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateDeleteToolbar(Integer count) {
        if (menu != null) menu.clear();
        TextView txtToolbar = toolbar.findViewById(R.id.text_title_toolbar);
        if (count != null && count > 0) {
            txtToolbar.setText(count + "");
            getMenuInflater().inflate(R.menu.evidence_list_delete_menu, menu);
            super.onCreateOptionsMenu(menu);

        } else {
            txtToolbar.setText(R.string.title_activity_evidence_list);
            super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewModel.hasItemsSelected())
            mViewModel.clearSelectedList();
        else
            super.onBackPressed();
    }

    private void openWifiSettings() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 0);
    }
}
