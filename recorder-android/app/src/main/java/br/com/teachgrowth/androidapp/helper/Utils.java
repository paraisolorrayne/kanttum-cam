package br.com.teachgrowth.androidapp.helper;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.TrackBox;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.googlecode.mp4parser.BasicContainer;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Mp4TrackImpl;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.util.Matrix;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.service.model.GaleryModel;
import br.com.teachgrowth.androidapp.service.model.response.ErrorResponse;
import br.com.teachgrowth.androidapp.ui.activity.RecoverPasswordActivity;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivity;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by gabrielaraujo on 14/12/2017.
 */

public class Utils {

    private static final int[] pesoCPF = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};

    public static Object jsonToObject(String obj, Class<?> classModel) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
//                .serializeNulls()
                .create();
        return gson.fromJson(obj, classModel);
    }

    public static Object jsonToObject(String obj, Type classModel) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
//                .serializeNulls()
                .create();
        return gson.fromJson(obj, classModel);
    }

    public static String objectToString(Object obj) {
        try {
            if (obj == null) {
                return "";
            }
            Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
            return gson.toJson(obj);
        } catch (Exception ex) {
            return "";
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static boolean isValidCPF(String cpf) {
        if (TextUtils.isEmpty(cpf) || (cpf == null) || (cpf.length() != 11)) return false;

        if (cpf.equalsIgnoreCase("11111111111") || cpf.equalsIgnoreCase("22222222222") || cpf.equalsIgnoreCase("33333333333")
                || cpf.equalsIgnoreCase("44444444444") || cpf.equalsIgnoreCase("55555555555") || cpf.equalsIgnoreCase("66666666666")
                || cpf.equalsIgnoreCase("77777777777") || cpf.equalsIgnoreCase("88888888888") || cpf.equalsIgnoreCase("99999999999") || cpf.equalsIgnoreCase("00000000000"))
            return false;

        Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
        Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
        return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
    }

    private static int calcularDigito(String str, int[] peso) {
        int soma = 0;
        for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
            digito = Integer.parseInt(str.substring(indice, indice + 1));
            soma += digito * peso[peso.length - str.length() + indice];
        }
        soma = 11 - soma % 11;
        return soma > 9 ? 0 : soma;
    }

    public static void showSimpleDialog(Context context, int resString, MaterialDialog.SingleButtonCallback callback) {
        showSimpleDialog(context, context.getString(resString), callback);
    }

    public static void showSimpleDialog(Context context, String text, MaterialDialog.SingleButtonCallback callback) {
        Typeface titleTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft_bold));
        Typeface contentTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft));
        try {
            new MaterialDialog.Builder(context)
                    .content(text)
                    .cancelable(false)
                    .typeface(titleTypeface, contentTypeface)
                    .positiveText(R.string.ok)
                    .onPositive(callback)
                    .show();
        } catch (Exception ex) {
        }
    }

    public static void showSimpleDialog(Context context, int title, int text, int textPositive, int textNegative, MaterialDialog.SingleButtonCallback callback) {
        Typeface titleTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft_bold));
        Typeface contentTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft));
        try {
            new MaterialDialog.Builder(context)
                    .title(title)
                    .content(text)
                    .cancelable(false)
                    .contentColorRes(R.color.black_54)
                    .titleColorRes(R.color.brownish_grey)
                    .typeface(titleTypeface, contentTypeface)
                    .positiveText(textPositive)
                    .negativeText(textNegative)
                    .onPositive(callback)
                    .onNegative(callback)
                    .show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showSimpleDialog(Context context, int text, int textPositive, int textNegative, MaterialDialog.SingleButtonCallback callback) {
        Typeface titleTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft_bold));
        Typeface contentTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft));
        try {
            new MaterialDialog.Builder(context)
                    .content(text)
                    .cancelable(false)
                    .contentColorRes(R.color.black_54)
                    .titleColorRes(R.color.brownish_grey)
                    .typeface(titleTypeface, contentTypeface)
                    .positiveText(textPositive)
                    .negativeText(textNegative)
                    .onPositive(callback)
                    .onNegative(callback)
                    .show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showDialogError(Context context, Throwable t, MaterialDialog.SingleButtonCallback callback) {
        Typeface titleTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft_bold));
        Typeface contentTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft));

        MaterialDialog.Builder dialog = new MaterialDialog.Builder(context);

        if (isThrowableInternetOff(t)) {
            dialog.negativeText(R.string.configurations)
                    .onNegative((dialog1, which) -> {
                        Intent callGPSSettingIntent = new Intent(Settings.ACTION_SETTINGS);
                        context.startActivity(callGPSSettingIntent);
                    });
        } else if (((HttpException) t).code() == 401) {
            callback = (dialog12, which) -> {
                TeachGrowthApplication.getInstance().logoutUser();
                dialog12.dismiss();
            };
        }
        try {
            ErrorResponse errorResponse = getMensagemThrowable(context, t);
            String msgError = errorResponse.getDetail();
            String title = errorResponse.getTitle();
            dialog
                    .title(title)
                    .content(msgError)
                    .cancelable(false)
                    .contentColorRes(R.color.black_54)
                    .titleColorRes(R.color.brownish_grey)
                    .typeface(titleTypeface, contentTypeface)
                    .positiveText(R.string.ok)
                    .onPositive(callback)
                    .show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showDialogError(Context context, int error) {
        Typeface titleTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft_bold));
        Typeface contentTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.proxima_nova_soft));
        try {
            new MaterialDialog.Builder(context)
                    .content(error)
                    .cancelable(false)
                    .contentColorRes(R.color.black_54)
                    .titleColorRes(R.color.brownish_grey)
                    .typeface(titleTypeface, contentTypeface)
                    .positiveText(R.string.ok)
                    .show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static ErrorResponse getMensagemThrowable(Context context, Throwable t) {

        if (t instanceof HttpException) {
            try {
                return parserError(((HttpException) t).response().errorBody().string());
            } catch (Exception ex) {
                return new ErrorResponse(context.getString(R.string.msg_error_generic));
            }
        } else if (isThrowableInternetOff(t)) {
            if (context instanceof RecoverPasswordActivity) {
                return new ErrorResponse(context.getString(R.string.title_erro_sem_internet), context.getString(R.string.msg_erro_sem_internet_recouver_password));
            } else {
                return new ErrorResponse(context.getString(R.string.title_erro_sem_internet), context.getString(R.string.msg_erro_sem_internet_login));
            }
        }

        return new ErrorResponse(context.getString(R.string.msg_error_generic));
    }

    private static boolean isThrowableInternetOff(Throwable t) {
        return t instanceof UnknownHostException || t instanceof IOException || t instanceof SocketTimeoutException;
    }

    private static ErrorResponse parserError(String error) throws Exception {
        Type listType = new TypeToken<ArrayList<ErrorResponse>>() {
        }.getType();
        return ((List<ErrorResponse>) jsonToObject((new JSONObject(error).get("errors")).toString(), listType)).get(0);
    }

    public static String getStringToInputStream(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                line = reader.readLine();
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

    public static void deleteCache(Context context) {
        File pathDad = context.getCacheDir();
        if (pathDad.isDirectory()) {
            for (String file : pathDad.list()) {
                File itemCurrent = new File(pathDad.getPath(), file);
                if (itemCurrent.isFile()) {
                    try {
                        deleteFile(pathDad.getPath() + "/" + file);
                    } catch (IOException e) {
                    }
                } else {
                    for (String file2 : itemCurrent.list()) {
                        try {
                            deleteFile(itemCurrent.getPath() + "/" + file2);
                        } catch (IOException e) {
                        }
                    }
                }
            }
        }
    }

    private static void deleteFile(String path) throws IOException {
        File file = new File(path);
        file.delete();
        if (file.exists()) {
            file.getCanonicalFile().delete();
            if (file.exists()) {
                TeachGrowthApplication.getInstance().getApplicationContext().deleteFile(file.getName());
            }
        }
    }

    public static void hideKeyboard(View view) {
        Context ctx = view.getContext();
        InputMethodManager imm = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void callNumber(Context context, int phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + context.getString(phone).trim()));
        context.startActivity(callIntent);
    }

    private static void verifyCreatePath() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Teachgrowth/");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private static String getPathAppFile() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/Teachgrowth/";
    }

    public static List<GaleryModel> getVideos() {

        List<GaleryModel> galeryModelList = new ArrayList<>();

        String[] parameters = {MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DATE_ADDED,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.DATE_TAKEN, MediaStore.Video.Media.DATA};

        Cursor videocursor = TeachGrowthApplication.getInstance().getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                parameters, null, null, MediaStore.Video.Media.DATE_ADDED + " DESC");

        for (int i = 0; i < videocursor.getCount(); i++) {
            videocursor.moveToPosition(i);
            GaleryModel galeryModel = new GaleryModel(
                    videocursor.getString(videocursor.getColumnIndex(MediaStore.Video.Thumbnails.DATA)),
                    videocursor.getString(videocursor.getColumnIndex(MediaStore.Video.Media.DURATION)));
            galeryModelList.add(galeryModel);
        }

        return galeryModelList;
    }

    public static int getDimensionPixelSize(Context context, int resDimen) {
        return context.getResources().getDimensionPixelSize(resDimen);
    }

    public static Size chooseVideoSize(Size[] choices) {
        for (Size size : choices) {
            if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
                return size;
            }
        }
        Log.e("error video", "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }

    public static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Log.e("error video", "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatMilliseconds(String milliseconds) {
//        try {
//            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"));
//            calendar.setTimeInMillis(Integer.parseInt(milliseconds));
//            Date date = calendar.getTime();
//            if (calendar.get(Calendar.HOUR) != 0) {
//                return new SimpleDateFormat("HH:mm:ss").format(date);
//            } else {
//                return new SimpleDateFormat("m:ss").format(date);
//            }
//        } catch (Exception e) {
//            return "";
//        }

        int seconds = (Integer.valueOf(milliseconds) / 1000) % 60;
        int minutes = ((Integer.valueOf(milliseconds) / (1000 * 60)) % 60);
        int hours = ((Integer.valueOf(milliseconds) / (1000 * 60 * 60)) % 24);

        return (hours > 0 ? formatTime(String.valueOf(hours)) + ":" : "") + formatTime(String.valueOf(minutes)) + ":" + formatTime(String.valueOf(seconds));

    }

    private static String formatTime(String time) {
        return time.length() == 1 ? "0" + time : time;
    }

    @SuppressLint("SimpleDateFormat")
    public static int timerToMilliseconds(String timer) {
        try {
            Date date;
            if (timer.split(":").length > 2) {
                return (int) new SimpleDateFormat("H:mm:ss").parse(timer).getTime() - 10800000;
            } else {
                return (int) new SimpleDateFormat("m:ss").parse(timer).getTime() - 10800000;
            }
        } catch (Exception e) {
            return 0;
        }

    }

    @SuppressLint("SimpleDateFormat")
    public static String parDate(String dateApi) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'H:mm").parse(dateApi);

            return new SimpleDateFormat("dd/MM/yyyy - H:mm").format(date);
        } catch (Exception e) {
            return dateApi;
        }
    }

    public static boolean thereIsConnection(Context context) {

        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connManager != null;

        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo m3G = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (TeachGrowthApplication.getInstance().isUserPreferenceSettingsAllow3g()) {
            return mWifi.isConnected() || m3G.isConnected();
        }
        return mWifi.isConnected();
    }

    public static void deleteVideoFileTempPath(Context context, String cicleName) {
        final File dir = context.getExternalFilesDir(null);
        String dirPath = null;
        if (dir != null) {
            dirPath = dir.getAbsolutePath() + "/" + cicleName;
        }
        File cycleDir = null;
        if (dirPath != null) {
            cycleDir = new File(dirPath);
        }
        if (cycleDir != null && cycleDir.exists()) {
            for (File file : cycleDir.listFiles()) {
                file.delete();
            }
        }
    }

    public static String getVideoFileTempPath(Context context, String cycleName, String videoLetter) {
        final File dir = context.getExternalFilesDir(cycleName);
        String dirPath = null;
        if (dir != null) {
            dirPath = dir.getAbsolutePath() + "/";
        }
        File cycleDir = null;
        if (dirPath != null) {
            cycleDir = new File(dirPath);
        }
        if (!cycleDir.exists()) {
            cycleDir.mkdir();
        }

        return dirPath + "/temp_" + videoLetter + Constants.FINAL_VIDEO_TYPE;
    }

    private static File[] getCycleVideos(Context context, String cycleName) {
        final File dir = context.getExternalFilesDir(null);
        String dirPath = null;
        if (dir != null) {
            dirPath = dir.getAbsolutePath() + "/" + cycleName;
        }
        File cycleDir = null;
        if (dirPath != null) {
            cycleDir = new File(dirPath);
        }
        if (cycleDir != null && cycleDir.exists()) {
            return cycleDir.listFiles();
        }

        return null;
    }

    public static String merge(Context context, String cycleName, int orientation) {
        File resultFile;
        final File dir = context.getExternalFilesDir(cycleName);
        String dirPath = null;
        if (dir != null) {
            dirPath = dir.getAbsolutePath() + "/";
        }
//        File[] cycleDir = getCycleVideos(context, cycleName);
        File cycleDir = null;
        if (dirPath != null) {
            cycleDir = new File(dirPath);
        }

        try {
            ArrayList<String> arrFilePaths = new ArrayList<>();

            if (cycleDir != null) {
                for (File file : cycleDir.listFiles()) {
                    if (file.getAbsolutePath().contains("temp"))
                        arrFilePaths.add(file.getAbsolutePath());
                }
            }

            String videoPath = dirPath + generateVideoName("video_");
            resultFile = new File(videoPath);

            if (arrFilePaths.size() > 0) {

                Movie[] inMovies = new Movie[arrFilePaths.size()];
                for (int i = 0; i < arrFilePaths.size(); i++) {
//                    inMovies[i] = MovieCreator.build(
//                            );

                    IsoFile isoFile = new IsoFile(arrFilePaths.get(i));
                    Movie m = new Movie();

                    List<TrackBox> trackBoxes = isoFile.getMovieBox().getBoxes(
                            TrackBox.class);

                    for (TrackBox trackBox : trackBoxes) {
                        trackBox.getTrackHeaderBox().setMatrix(getMatrixOrientation(orientation));
                        m.addTrack(new Mp4TrackImpl("teste" + i, trackBox));
                    }

                    inMovies[i] = m;
                }

                List<Track> videoTracks = new LinkedList<>();
                List<Track> audioTracks = new LinkedList<>();

                for (Movie m : inMovies) {
                    for (Track t : m.getTracks()) {
                        if (t.getHandler().equals("soun")) {
                            audioTracks.add(t);
                        }
                        if (t.getHandler().equals("vide")) {
                            videoTracks.add(t);
                        }
                    }
                }

                Movie result = new Movie();

                if (audioTracks.size() > 0)
                    result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));

                if (videoTracks.size() > 0)
                    result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));

                BasicContainer out = (BasicContainer) new DefaultMp4Builder().build(result);

                FileChannel fc = new RandomAccessFile(resultFile.getAbsolutePath(), "rw").getChannel();
                out.writeContainer(fc);
                fc.close();
                notifyNewFileOnGallery(context, resultFile);

                for (String strFiles : arrFilePaths) {
                    if (strFiles.contains("temp"))
                        new File(strFiles).delete();
                }

            }

            return moveVideoToGallery(resultFile.getPath(), context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @SuppressLint("SimpleDateFormat")
    public static String generateVideoName(String prefix) {
        String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        return prefix + fileName + Constants.FINAL_VIDEO_TYPE;
    }

    private static String moveVideoToGallery(String videoPath, Context context) {
        try {
            StringBuilder newVideoPath = new StringBuilder();
            newVideoPath
                    .append(Constants.GALLERY_VIDEO_PATH)
                    .append(generateVideoName("video_"));

            File from = new File(videoPath);
            File to = new File(newVideoPath.toString());
            from.renameTo(to);
            notifyNewFileOnGallery(context, to);

            return newVideoPath.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private static void notifyNewFileOnGallery(Context context, File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(file);
            scanIntent.setData(contentUri);
            context.sendBroadcast(scanIntent);
        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file:///" + Environment.getExternalStorageDirectory()));
            context.sendBroadcast(intent);
        }
    }

    public static Matrix getMatrixOrientation(int orientation) {
        switch (orientation) {
            case 90:
                return Matrix.ROTATE_180;
            case 180:
                return Matrix.ROTATE_270;
            case 270:
                return Matrix.ROTATE_0;
            default:
                return Matrix.ROTATE_90;
        }
    }

    public static void expand(final View v, int duration) {
        final boolean expand = false;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((BaseActivity) v.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = 1000;

        int prevHeight = v.getHeight();

        if (expand) {
            int measureSpecParams = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);
            v.measure(measureSpecParams, measureSpecParams);
            height = v.getMeasuredHeight();
        }

        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, height);
        valueAnimator.addUpdateListener(animation -> {
            v.getLayoutParams().height = (int) animation.getAnimatedValue();
            v.requestLayout();
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (expand) {
                    v.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!expand) {
                    v.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static int getXPositionFromTimeVideo(Activity context, int positionTime, int sizeVideo) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        return positionTime * width / sizeVideo;
    }

    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    static public boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static int getDrawableCommentKind(String kind) {
        switch (kind) {
            case "negative":
                return R.drawable.ic_marks_negative;
            case "positive":
                return R.drawable.ic_marks_positive;
            default:
                return R.drawable.ic_marks_alert;
        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

}
