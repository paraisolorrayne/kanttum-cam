package br.com.teachgrowth.androidapp.service.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucas on 24/04/18.
 */

public class AttributesCommentsResponse {

    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("attributes")
    private CommentsModel commentsModel;
    @SerializedName("relationships")
    private Relationships  relationships;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CommentsModel getCommentsModel() {
        return commentsModel;
    }

    public void setCommentsModel(CommentsModel commentsModel) {
        this.commentsModel = commentsModel;
    }

    public String getText() {
        return commentsModel.getText();
    }

    public void setText(String text) {
        commentsModel.setText(text);
    }

    public String getKind() {
        return commentsModel.getKind();
    }

    public void setKind(String kind) {
        commentsModel.setKind(kind);
    }

    public int getCommented_at() {
        return commentsModel.getCommented_at();
    }

    public void setCommented_at(int commented_at) {
        this.commentsModel.setCommented_at(commented_at);
    }

    public String getCreatedAt() {
        return commentsModel.getCreatedAt();
    }

    public void setCreatedAt(String createdAt) {
        this.commentsModel.setCreatedAt(createdAt);
    }

    public String getUpdatedAt() {
        return commentsModel.getUpdatedAt();
    }

    public void setUpdatedAt(String updatedAt) {
        this.commentsModel.setUpdatedAt(updatedAt);
    }

    public int getAuthorId() {
        return commentsModel.getAuthorId();
    }

    public void setAuthorId(int authorId) {
        this.commentsModel.setAuthorId(authorId);
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

    public List<Integer> getListCriteriaIdsForComments() {
        List<Integer> auxCriterias = new ArrayList<>();
        for (int i = 0; i < getRelationships().getCriteria().getData().size(); i++) {
            auxCriterias.add(getRelationships().getCriteria().getDataId(i));
        }
        return auxCriterias;
    }

    private class CommentsModel {

        @SerializedName("text")
        private String text;
        @SerializedName("kind")
        private String kind;
        @SerializedName("author_id")
        private int authorId;
        @SerializedName("commented_at")
        private int commented_at;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public int getAuthorId() {
            return authorId;
        }

        public void setAuthorId(int authorId) {
            this.authorId = authorId;
        }

        public int getCommented_at() {
            return commented_at;
        }

        public void setCommented_at(int commented_at) {
            this.commented_at = commented_at;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }

    private class Relationships {
        @SerializedName("relationships")
        private Relationships relationships;

        @SerializedName("criteria")
        private Criteria criteria;

        public Relationships getRelationships() {
            return relationships;
        }

        public void setRelationships(Relationships relationships) {
            this.relationships = relationships;
        }

        public int getDataId() {
            return relationships.getDataId();
        }

        public void setDataId(int id) {
            this.relationships.setDataId(id);
        }

        public String getDataType() {
            return relationships.getDataType();
        }

        public void setDataType(String type) {
            this.relationships.setDataType(type);
        }

        public Criteria getCriteria() {
            return criteria;
        }

        public void setCriteria(Criteria criteria) {
            this.criteria = criteria;
        }

        private class Criteria {

            @SerializedName("data")
            private List<Data> data;

            public List<Data> getData() {
                return data;
            }

            public void setData(List<Data> data) {
                this.data = data;
            }

            public int getDataId(int position) {
                return data.get(position).getId();
            }

            public String getDataType(int position) {
                return data.get(position).getType();
            }

            private class Data {
                @SerializedName("id")
                private int id;
                @SerializedName("type")
                private String type;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }
            }
        }
    }

}
