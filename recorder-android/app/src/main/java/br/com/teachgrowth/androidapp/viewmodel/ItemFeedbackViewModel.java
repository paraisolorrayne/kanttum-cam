package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;

import com.genius.groupie.GroupAdapter;

import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.ui.adapter.FeedBackListItemLabel;
import br.com.teachgrowth.androidapp.ui.interfaces.OnClickComment;

/**
 * Created by mateus on 12/04/18.
 */

public class ItemFeedbackViewModel {
    public final GroupAdapter groupAdapter = new GroupAdapter();
    public ObservableField<String> srcImage = new ObservableField<>();
    public ObservableField<String> timeFeedback = new ObservableField<>();
    public ObservableInt statusImg = new ObservableInt(R.drawable.icon_play_green);
    public ObservableInt colorStatus = new ObservableInt(R.color.green_blue);
    String status;
    private OnClickComment onClickComment;


    public ItemFeedbackViewModel(String srcImage, String status,
                                 String timeFeedback, OnClickComment onClickComment, List<String> criterias) {
        this.srcImage.set(srcImage);
        this.status = status;
        this.timeFeedback.set(timeFeedback);
        this.onClickComment = onClickComment;

        if (status.equals("positive")) {
            colorStatus.set(R.color.green_blue);
            statusImg.set(R.drawable.icon_play_green);
        } else if (status.equals("negative")) {
            colorStatus.set(R.color.tomato);
            statusImg.set(R.drawable.icon_play_red);
        } else {
            colorStatus.set(R.color.orangey_yellow);
            statusImg.set(R.drawable.icon_play_yellow);
        }

        for (String s : criterias) {
            groupAdapter.add(new FeedBackListItemLabel(s, colorStatus.get()));
        }
    }

    public void onClick(View view) {
        onClickComment.onClick(Utils.timerToMilliseconds(timeFeedback.get()));
    }
}
