package br.com.teachgrowth.androidapp.viewmodel.base;

import android.animation.ValueAnimator;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.os.Bundle;
import android.view.animation.AccelerateInterpolator;

import com.afollestad.materialdialogs.MaterialDialog;

import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;

/**
 * Created by gabrielaraujo on 18/11/2017.
 */

public class BaseViewModel implements ViewModel {

    public final ObservableField<String> titleTextToolbar = new ObservableField<>("");
    protected CallbackBasicViewModel callback;

    public BaseViewModel(CallbackBasicViewModel callback) {
        this.callback = callback;
    }

    protected void showProgress() {
        if (callback != null) callback.showDialogProgress();
    }

    protected void hideProgress() {
        if (callback != null) callback.hideDialogProgress();
    }

    protected void showError(Throwable t, MaterialDialog.SingleButtonCallback callback2) {
       hideProgress();
        if (callback != null) callback.showError(t, callback2);
    }

    protected void openActivity(Class<?> open) {
        if (callback != null) callback.openActivity(open);
    }

    protected void openActivity(Class<?> open, String name, String value) {
        if (callback != null) callback.openActivity(open, name, value);
    }

    protected void openActivityNewTask(Class<?> open) {
        if (callback != null) callback.openActivityNewTask(open);
    }

    protected void finish(int resultCode) {
        if (callback != null) callback.finish(resultCode);
    }

    protected void finish(int resultCode, Bundle bundle) {
        if (callback != null) callback.finish(resultCode, bundle);
    }

    protected void animatedTransitionFadeIn(ObservableFloat alphaValue) {
        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.setDuration(1000);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.addUpdateListener(
                valueAnimator -> alphaValue.set((Float) valueAnimator.getAnimatedValue())
        );
        animator.start();
    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }
}
