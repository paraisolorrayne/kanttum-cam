package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableBoolean;
import android.widget.CompoundButton;

import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;

public class SettingsViewModel extends BaseViewModel {
    public ObservableBoolean isAllow = new ObservableBoolean(false);

    public SettingsViewModel(CallbackBasicViewModel callback) {
        super(callback);
        isAllow.set(TeachGrowthApplication.getInstance().isUserPreferenceSettingsAllow3g());
    }

    public void Allow3gForDownload(CompoundButton compoundButton, boolean isAllow) {
        TeachGrowthApplication.getInstance()
                .setUserPreferenceSettingsAllow3g(isAllow);
    }
}
