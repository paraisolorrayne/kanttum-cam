package br.com.teachgrowth.androidapp.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ActivityRecoverPasswordBinding;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.RecoverPasswordViewModel;

public class RecoverPasswordActivity extends BaseActivityViewModel<ActivityRecoverPasswordBinding, RecoverPasswordViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_recover_password);
        mViewModel = new RecoverPasswordViewModel(this);
        mBinding.setViewModel(mViewModel);
        mViewModel.onSuccess.subscribe(email -> {
            RecoverPasswordSuccessActivity.open(this, email);
            finish(0);
        });
    }

    public void onBack(View view) {
        onBackPressed();
    }

}
