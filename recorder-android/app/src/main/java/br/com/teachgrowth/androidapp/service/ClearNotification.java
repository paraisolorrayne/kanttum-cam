package br.com.teachgrowth.androidapp.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import br.com.teachgrowth.androidapp.helper.NotificationService;

/**
 * Created by mateus on 11/04/18.
 */

public class ClearNotification extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        new NotificationService(context)
                .clear(intent.getIntExtra("notificationId",-1));
    }
}
