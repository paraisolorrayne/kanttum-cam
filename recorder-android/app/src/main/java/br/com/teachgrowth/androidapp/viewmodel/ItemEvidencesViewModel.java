package br.com.teachgrowth.androidapp.viewmodel;

import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.service.UploadVideoService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;

import static br.com.teachgrowth.androidapp.helper.Utils.isMyServiceRunning;

/**
 * Created by lucas on 22/03/18.
 */

public class ItemEvidencesViewModel extends BaseViewModel {

    public final ObservableField<String> pathImagem = new ObservableField<>();
    public final ObservableInt statusColor = new ObservableInt(R.color.warm_grey);
    public final ObservableInt drawableStatus = new ObservableInt();
    public final ObservableField<String> statusText = new ObservableField<>();
    public final ObservableInt progressNumber = new ObservableInt(0);
    public final ObservableField<String> progressTime = new ObservableField<>();
    public final ObservableBoolean showProgress = new ObservableBoolean(false);
    public final ObservableBoolean showFeedbackAmount = new ObservableBoolean(false);
    public final ObservableField<String> feedbackAmount = new ObservableField();
    public final ObservableField<String> videoSize = new ObservableField<>();
    public final ObservableField<String> videoTitle = new ObservableField<>();
    public final ObservableField<String> videoDescription = new ObservableField<>();
    public final ObservableField<Boolean> isSelected = new ObservableField<>(false);
    public final ObservableField<Boolean> isSelectedPossibility = new ObservableField<>(false);
    public final ObservableBoolean isEvidenceListActivity = new ObservableBoolean();

    private OfflineEvidenceModel evidenceModel;
    private String cicle;

    public ItemEvidencesViewModel(String cicle, OfflineEvidenceModel evidenceModel, CallbackBasicViewModel callback, boolean isEvidenceListActivity) {
        super(callback);
        this.cicle = cicle;
        this.evidenceModel = evidenceModel;
        init(isEvidenceListActivity);
    }

    private void init(boolean isEvidenceListActivity) {
        if (evidenceModel.getThumbUrl() != null)
            pathImagem.set(evidenceModel.getThumbUrl());
        else if (evidenceModel.getVideoPath() == null)
            pathImagem.set(evidenceModel.getVideoUrl());
        else
            pathImagem.set(evidenceModel.getVideoPath());

        videoSize.set(Utils.formatMilliseconds(String.valueOf(evidenceModel.getVideoDuration())));
        videoTitle.set(evidenceModel.getVideoTitle());
        videoDescription.set(evidenceModel.getVideoDescription());
        EvidenceStatusEnum statusEnum = EvidenceStatusEnum.getClientStatusEnumFromId(evidenceModel.getVideoStatus());
        statusColor.set(statusEnum.getResColor());
        drawableStatus.set(statusEnum.getResDrawable());
        showProgress.set(statusEnum.getId().equals(EvidenceStatusEnum.STATUS_DOWNLOADING.getId()));
        statusText.set(callback.getString(statusEnum.getText()));
        progressTime.set(evidenceModel.timeDuration);
        this.isEvidenceListActivity.set(isEvidenceListActivity);
    }

    public void changeStatus(OfflineEvidenceModel evidenceModel) {
        this.evidenceModel = evidenceModel;

        EvidenceStatusEnum statusEnum = EvidenceStatusEnum.getClientStatusEnumFromId(evidenceModel.getVideoStatus());
        showProgress.set(statusEnum.getId().equals(EvidenceStatusEnum.STATUS_DOWNLOADING.getId()));
        progressNumber.set(evidenceModel.getPercentUploaded());
        statusColor.set(statusEnum.getResColor());
        statusColor.notifyChange();
        drawableStatus.set(statusEnum.getResDrawable());
        statusText.set(callback.getString(statusEnum.getText()));
        progressTime.set(evidenceModel.timeDuration);
    }

    public void statusDownloadClick(View v) {
        Log.d("TESTETAREFA", "CLICK STATUS DOWNLOAD");
        if (isMyServiceRunning(UploadVideoService.class, v.getContext())) {
            Log.d("TESTETAREFA", "resume next");
            TeachGrowthApplication.getInstance().evidenceResumeSubject.onNext(evidenceModel);
        } else {
            Log.d("TESTETAREFA", "SERVIÇO CRIADO VM EVIDENCES");
            v.getContext().startService(new Intent(v.getContext(), UploadVideoService.class));
        }
    }
}
