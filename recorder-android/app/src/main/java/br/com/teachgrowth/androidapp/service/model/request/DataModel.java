package br.com.teachgrowth.androidapp.service.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 02/03/18.
 */

public class DataModel<T, E, I> extends Object {

    @SerializedName("data")
    T data;

    @SerializedName("included")
    E include;

    @SerializedName("meta")
    I meta;

    public DataModel(T data) {
        this.data = data;
    }

    public DataModel(T data, E include) {
        this.data = data;
        this.include = include;
    }

    public DataModel(T data, E include, I meta) {
        this.data = data;
        this.include = include;
        this.meta = meta;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public E getInclude() {
        return include;
    }

    public void setInclude(E include) {
        this.include = include;
    }

    public I getMeta() {
        return meta;
    }

    public void setMeta(I meta) {
        this.meta = meta;
    }
}
