package br.com.teachgrowth.androidapp.viewmodel;

import android.content.Context;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.os.Handler;
import android.view.View;

import java.text.DecimalFormat;
import java.util.Objects;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.subjects.PublishSubject;

/**
 * Created by lucas on 13/03/18.
 */

public class CameraVideoViewModel extends BaseViewModel {
    //dialog observables
    public final ObservableField<String> dialogTitle = new ObservableField<>();
    public final ObservableField<String> dialogContent = new ObservableField<>();
    public final ObservableField<String> dialogConfirm = new ObservableField<>();
    public final ObservableBoolean showDialog = new ObservableBoolean(false);

    public final ObservableField<String> timerField = new ObservableField<>();
    public final ObservableField<String> recordVideoTimer = new ObservableField<>();
    public final ObservableBoolean isShowTimer = new ObservableBoolean(false);
    public final ObservableBoolean showLandscapeViews = new ObservableBoolean(false);
    public final ObservableBoolean showLandscapeViewsRevert = new ObservableBoolean(false);
    public final ObservableBoolean showPortraitViews = new ObservableBoolean(true);
    public final ObservableBoolean mIsRecordingVideo = new ObservableBoolean(false);
    public final ObservableBoolean showBtnsControls = new ObservableBoolean(false);
    public final ObservableInt showProgressBar = new ObservableInt(View.INVISIBLE);
    public final ObservableInt orientation = new ObservableInt(0);
    public final PublishSubject<Integer> onTimer = PublishSubject.create();
    public final PublishSubject<Integer> algumacoisaqueaindasei = PublishSubject.create();
    public int videoOrientation = 0;

    private long recordTimer = 0;
    private int intTimer = 6;
    private Handler timerHandler = new Handler();

    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            intTimer--;
            timerField.set(String.valueOf(intTimer));
            isShowTimer.set(intTimer > 0);
            timerHandler.postDelayed(this, 1000);
            if (intTimer < 0) {
                timerHandler.removeCallbacks(timerRunnable);
                onTimer.onNext(intTimer);
                startRecordVideoTimer();
            }
        }
    };

    private Runnable recordVideoTimerRunnable = new Runnable() {
        @Override
        public void run() {
            recordTimer += 1000;
            recordVideoTimer.set(setClock(recordTimer));
            timerHandler.postDelayed(this, 1000);
        }
    };

    private String setClock(long time) {

        long elapsedSeconds = time / 1000;
        long secondsDisplay = elapsedSeconds % 60;
        long elapsedMinutes = elapsedSeconds / 60;
        long minutesDisplay = elapsedMinutes % 60;
        long elapsedHour = elapsedMinutes / 60;
        long hourDisplay = elapsedHour % 60;

        DecimalFormat df = new DecimalFormat("##00");

        if (time > 3600000) {
            return df.format(hourDisplay) + ":" + df.format(minutesDisplay) + ":" + df.format(secondsDisplay);
        } else {
            return df.format(minutesDisplay) + ":" + df.format(secondsDisplay);
        }
    }

    public CameraVideoViewModel(CallbackBasicViewModel callback) {
        super(callback);
        mIsRecordingVideo.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                showBtnsControls.set(!mIsRecordingVideo.get());
            }
        });
        isShowTimer.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (isShowTimer.get()) showBtnsControls.set(false);
            }
        });
    }

    public void startTimer() {
        intTimer = 6;
        timerHandler.postDelayed(timerRunnable, 0);
    }

    public void startRecordVideoTimer() {
        timerHandler.postDelayed(recordVideoTimerRunnable, 0);
    }

    public Handler getTimerHandler() {
        return timerHandler;
    }

    public Runnable getTimerRunnable() {
        return timerRunnable;
    }

    public Runnable getRecordVideoTimerRunnable() {
        return recordVideoTimerRunnable;
    }

    public void clickBack() {
        dialogTitle.set(callback.getString(R.string.exit_the_recording));
        dialogContent.set(callback.getString(R.string.when_exiting_the_video));
        dialogConfirm.set(callback.getString(R.string.exit));
        showDialog.set(true);
    }

    public void clickExclude(View view) {
        dialogTitle.set(callback.getString(R.string.exclude_the_recording));
        dialogContent.set(callback.getString(R.string.the_video_will_be_permanently));
        dialogConfirm.set(callback.getString(R.string.exclude));
        showDialog.set(true);
    }

    public void showDialogTimeRecord(long time) {
        dialogTitle.set(callback.getString(R.string.continue_recording));
        dialogContent.set(callback.getString(R.string.without_enough_space, time));
        dialogConfirm.set(callback.getString(R.string.continue_dialog));
        showDialog.set(true);
    }


    public void dialogOnClick(View view) {
        if (view.getId() == R.id.btn_confirm) {
            if (Objects.requireNonNull(dialogConfirm.get()).equals(callback.getString(R.string.exit))) callback.finish(0);
            if (Objects.requireNonNull(dialogConfirm.get()).equals(callback.getString(R.string.continue_dialog))) {
                startTimer();
            } else {
                deleteVideo(view.getContext());
            }
        }
        showDialog.set(false);
    }

    public void deleteVideo(Context context) {
        Utils.deleteVideoFileTempPath(context, Constants.Cycle);
        showBtnsControls.set(false);
        recordVideoTimer.set("");
        recordTimer = 0;
    }

}
