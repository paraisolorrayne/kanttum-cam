package br.com.teachgrowth.androidapp.ui.interfaces;

public interface OnPlayerIsPreparedListener {
    void isPrepared();
}
