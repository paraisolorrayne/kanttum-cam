package br.com.teachgrowth.androidapp.ui.adapter;

import android.view.View;

import com.genius.groupie.Item;

import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ItemFeedbackBinding;
import br.com.teachgrowth.androidapp.ui.interfaces.OnClickComment;
import br.com.teachgrowth.androidapp.viewmodel.ItemFeedbackViewModel;

/**
 * Created by mateus on 12/04/18.
 */

public class FeedBackListItem extends Item<ItemFeedbackBinding> {

    private final List<String> criterias;
    private String srcImage;
    private String status;
    private String feedback;
    private String timeFeedback;
    private OnClickComment onClickComment;

    public FeedBackListItem(String feedback, String srcImage,
                            String status, String timeFeedback, OnClickComment onClickComment, List<String> criterias) {
        this.feedback = feedback;
        this.srcImage = srcImage;
        this.status = status;
        this.timeFeedback = timeFeedback;
        this.onClickComment = onClickComment;
        this.criterias = criterias;
    }

    @Override
    public int getLayout() {
        return R.layout.item_feedback;
    }

    @Override
    public void bind(ItemFeedbackBinding viewBinding, int position) {
        viewBinding.setViewModel(new ItemFeedbackViewModel(srcImage, status, timeFeedback, onClickComment, criterias));
        viewBinding.expandTextView.setText(feedback);

        viewBinding.expandTextView.setOnExpandStateChangeListener((textView, isExpanded) -> {
            if (isExpanded)
                viewBinding.txtReadMore.setVisibility(View.VISIBLE);
            else
                viewBinding.txtReadMore.setVisibility(View.GONE);
        });

        viewBinding.txtReadMore.setOnClickListener(view -> viewBinding.expandTextView.expand());
        viewBinding.expandTextView.setText(feedback);
        viewBinding.txtReadMore.setOnClickListener(view -> viewBinding.expandTextView.expand());
    }
}
