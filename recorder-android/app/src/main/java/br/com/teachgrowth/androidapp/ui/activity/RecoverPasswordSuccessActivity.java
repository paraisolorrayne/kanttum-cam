package br.com.teachgrowth.androidapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.databinding.ActivityRecoverPasswordSuccessBinding;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.RecoverPasswordSuccessViewModel;

public class RecoverPasswordSuccessActivity extends BaseActivityViewModel<ActivityRecoverPasswordSuccessBinding, RecoverPasswordSuccessViewModel> {

    public static void open(Context context, String email) {
        Intent intent = new Intent(context, RecoverPasswordSuccessActivity.class);
        intent.putExtra(Constants.Extra.EMAIL, email);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_recover_password_success);
        mViewModel = new RecoverPasswordSuccessViewModel(getString(R.string.confirmation_recover_message, getIntent().getStringExtra(Constants.Extra.EMAIL)), this);
        mBinding.setViewModel(mViewModel);
        mViewModel.onTimer.subscribe(timer -> {

            if (timer == 0) {
                finish();
            }
            mViewModel.msgTimer.set(getString(R.string.timer_recover_password, timer));
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mViewModel.getTimerHandler().removeCallbacks(mViewModel.getTimerRunnable());
    }

    public void onBack(View view) {
        onBackPressed();
    }
}
