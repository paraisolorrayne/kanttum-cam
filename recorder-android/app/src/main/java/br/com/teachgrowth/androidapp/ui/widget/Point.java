package br.com.teachgrowth.androidapp.ui.widget;

/**
 * Created by lucas on 25/04/18.
 */

public class Point {
    private int videoTimer;
    private int resDrawable;

    public Point(int videoTimer, int resDrawable) {
        this.videoTimer = videoTimer;
        this.resDrawable = resDrawable;
    }

    int getX() {
        return videoTimer;
    }

    public void setX(int x) {
        this.videoTimer = x;
    }

    int getResDrawable() {
        return resDrawable;
    }

    public void setResDrawable(int resDrawable) {
        this.resDrawable = resDrawable;
    }
}
