package br.com.teachgrowth.androidapp.ui.interfaces;

/**
 * Created by lucas on 27/04/18.
 */

public interface OnClickComment {
    void onClick(int commentTime);
}
