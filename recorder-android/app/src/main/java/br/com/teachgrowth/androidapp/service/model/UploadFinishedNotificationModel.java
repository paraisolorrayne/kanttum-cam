package br.com.teachgrowth.androidapp.service.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 23/04/18.
 */

public class UploadFinishedNotificationModel {

    @SerializedName("growth_process_id")
    private int growthProcessId;

    @SerializedName("type")
    private String type;

    @SerializedName("medium_id")
    private int medium_id;

    @SerializedName("institution_id")
    private int institution_id;

    public int getGrowthProcessId() {
        return growthProcessId;
    }

    public void setGrowthProcessId(int growthProcessId) {
        this.growthProcessId = growthProcessId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMedium_id() {
        return medium_id;
    }

    public void setMedium_id(int medium_id) {
        this.medium_id = medium_id;
    }

    public int getInstitution_id() {
        return institution_id;
    }

    public void setInstitution_id(int institution_id) {
        this.institution_id = institution_id;
    }
}
