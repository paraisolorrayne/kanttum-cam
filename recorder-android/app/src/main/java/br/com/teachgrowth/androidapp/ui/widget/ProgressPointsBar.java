package br.com.teachgrowth.androidapp.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.helper.Utils;

/**
 * Created by lucas on 25/04/18.
 */

public class ProgressPointsBar extends View {
    private List<Point> points = new ArrayList<>();
    private int videoSize;

    public ProgressPointsBar(Context context) {
        super(context);
    }

    public ProgressPointsBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void addPoint(Point point) {
        points.add(point);
        //this will request a call to draw(canvas) method
        invalidate();
    }

    public void initPoints(List<Point> points, int videoSize) {
        this.points = new ArrayList<>(points);
        this.videoSize = videoSize;
        invalidate();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        for (Point p : points) {
            drawPoint(canvas, p);
        }
    }

    private void drawPoint(Canvas canvas, Point p) {

        Resources res = getResources();
        Bitmap bitmap = BitmapFactory.decodeResource(res, p.getResDrawable());
        int initialX = Utils.getXPositionFromTimeVideo((Activity) getContext(), p.getX(), videoSize);
        int dimen = Utils.getDimensionPixelSize(getContext(), R.dimen.margin_10);
        int margin = Utils.getDimensionPixelSize(getContext(), R.dimen.margin_2);
        Rect rectangle = new Rect(initialX, margin, initialX + dimen, dimen + margin);
        canvas.drawBitmap(bitmap, null, rectangle, null);
    }


}