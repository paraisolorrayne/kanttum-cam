package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import br.com.teachgrowth.androidapp.R;

/**
 * Created by mateus on 12/04/18.
 */

public class ItemFeedbackLabelViewModel {
    public ObservableInt colorStatus = new ObservableInt(R.color.green_blue);
    public ObservableField<String> label = new ObservableField<>();

    public ItemFeedbackLabelViewModel(String label, int color) {
        this.label.set(label);
        this.colorStatus.set(color);
    }
}
