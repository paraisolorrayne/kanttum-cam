package br.com.teachgrowth.androidapp.service.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 27/03/18.
 */

public class AttributeUploadRequest {

    @SerializedName("attributes")
    UploadModel uploadModel;

    public AttributeUploadRequest(String extension) {
        this.uploadModel = new UploadModel(extension);
    }

    public UploadModel getUploadModel() {
        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }

    public class UploadModel {

        @SerializedName("file_name")
        private String extension;

        public UploadModel(String extension) {
            this.extension = extension;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

    }
}
