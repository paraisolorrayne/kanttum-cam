package br.com.teachgrowth.androidapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.response.UserDetailResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserModel;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivity;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SplashActivity extends BaseActivity {

    private static final int TIME_SPLASH = 2000;
    private BaseActivity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Context appContext = getApplicationContext();
//        AWSConfiguration awsConfig = new AWSConfiguration(appContext);
//        IdentityManager identityManager = new IdentityManager(appContext, awsConfig);
//        IdentityManager.setDefaultIdentityManager(identityManager);
//        identityManager.doStartupAuth(this, new StartupAuthResultHandler() {
//            @Override
//            public void onComplete(StartupAuthResult startupAuthResult) {
//                Log.v("user_amazon", String.valueOf(startupAuthResult.getIdentityManager()));
//                // User identity is ready as unauthenticated user or previously signed-in user.
//            }
//        });

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, pendingDynamicLinkData -> {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink;
                        String dynamicLinkUserToken = null;
                        UserModel userModel = TeachGrowthApplication.getInstance().getUserModel();

                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            dynamicLinkUserToken = deepLink.getQueryParameter("token");
                        }

                        if (dynamicLinkUserToken != null) {
                            if (userModel != null) {
                                if (dynamicLinkUserToken.equals(userModel.getAccess_token())) {
                                    openEvidenceListActivitySSO();
                                } else {
                                    userModel = new UserModel();
                                    userModel.setAccess_token(dynamicLinkUserToken);

                                    TeachGrowthApplication.getInstance().logoutUserSSO(userModel);

                                    callApiUserDetail();
                                }
                            } else {
                                userModel = new UserModel();
                                userModel.setAccess_token(dynamicLinkUserToken);

                                TeachGrowthApplication.getInstance().setUserModel(userModel);
                                callApiUserDetail();
                            }
                        } else {
                            openDefaultActivity();
                        }
                    })
                    .addOnFailureListener(this, e -> {
                        openDefaultActivity();
                        finish();
                    });
        }, TIME_SPLASH);
    }

    private void openDefaultActivity() {
        if (TeachGrowthApplication.getInstance().getUserModel() != null) {
            openActivity(EvidenceListActivity.class);
        } else {
            openActivity(LoginActivity.class);
        }

        finish();
    }

    private void callApiUserDetail() {
        RetrofitBase.getInterfaceRetrofit()
                .getUser()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess,
                        this::handlerErrors,
                        () -> {
                        });
    }

    private void onSuccess(DataModel<UserDetailResponse, Object, Object> response) {
        if (response != null)
            TeachGrowthApplication.getInstance().setUserDetail(response.getData());
        openEvidenceListActivitySSO();
    }

    private void openEvidenceListActivitySSO() {
        Intent intent = new Intent(activity, EvidenceListActivity.class);
        intent.putExtra(Constants.Extra.LOGIN_FROM_SSO, true);

        activity.startActivity(intent);
        finish();
    }

    private void handlerErrors(Throwable t) {
        showError(t, (dialog, which) -> {
        });
    }
}
