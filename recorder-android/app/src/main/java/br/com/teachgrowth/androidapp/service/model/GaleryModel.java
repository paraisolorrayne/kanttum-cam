package br.com.teachgrowth.androidapp.service.model;

/**
 * Created by lucas on 06/03/18.
 */

public class GaleryModel {
    private String videoPath;
    private String videoTime;

    public GaleryModel(String videoPath, String videoTime) {
        this.videoPath = videoPath;
        this.videoTime = videoTime;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(String videoTime) {
        this.videoTime = videoTime;
    }
}
