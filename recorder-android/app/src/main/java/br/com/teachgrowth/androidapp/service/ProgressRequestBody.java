package br.com.teachgrowth.androidapp.service;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

/**
 * Created by lucas on 28/03/18.
 */

public class ProgressRequestBody extends RequestBody {
    private File mFile;
    private String mPath;
    private UploadCallbacks mListener;
    private OfflineEvidenceModel evidenceModel;

    private static final int DEFAULT_BUFFER_SIZE = 2048;

    public interface UploadCallbacks {
        void onProgressUpdate(OfflineEvidenceModel evidenceModel, int percentage);

        void onError(OfflineEvidenceModel evidenceModel);

        void onFinish(OfflineEvidenceModel evidenceModel);
    }

    public ProgressRequestBody(final OfflineEvidenceModel evidenceModel, final UploadCallbacks listener) {
        mFile = new File(evidenceModel.getVideoPath());
        this.evidenceModel = evidenceModel;
        mListener = listener;
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse("video/mp4");
    }

    @Override
    public long contentLength() {
        return mFile.length();
    }

    @Override
    public void writeTo(@NonNull BufferedSink sink) throws IOException {
        long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];

        try (FileInputStream in = new FileInputStream(mFile)) {
            long uploaded = 0;
            int lastPercentValue = 0;
            int read;
            Handler handler = new Handler(Looper.getMainLooper());
            while ((read = in.read(buffer)) != -1) {
                int percentValue = (int) (100 * uploaded / fileLength);
                // update progress on UI thread
                if (lastPercentValue != percentValue) {
                    lastPercentValue = percentValue;
                    handler.post(new ProgressUpdater(uploaded, fileLength));
                }

                uploaded += read;
                sink.write(buffer, 0, read);
            }
        }
    }

    private class ProgressUpdater implements Runnable {
        private long mUploaded;
        private long mTotal;

        ProgressUpdater(long uploaded, long total) {
            mUploaded = uploaded;
            mTotal = total;
        }

        @Override
        public void run() {
            mListener.onProgressUpdate(evidenceModel, (int) (100 * mUploaded / mTotal));
        }
    }
}