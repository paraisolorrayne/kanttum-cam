package br.com.teachgrowth.androidapp.service.model.response;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import br.com.teachgrowth.androidapp.service.model.UploadResponseAttributes;

/**
 * Created by lucas on 27/03/18.
 */

@Parcel(Parcel.Serialization.BEAN)
public class UploadResponse {

    @SerializedName("id")
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("attributes")
    private UploadResponseAttributes attributes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UploadResponseAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(UploadResponseAttributes attributes) {
        this.attributes = attributes;
    }

}
