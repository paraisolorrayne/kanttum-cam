package br.com.teachgrowth.androidapp.viewmodel;

import android.app.Activity;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.service.model.GaleryModel;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.activity.EvidencesDetailEditableActivity;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;

/**
 * Created by lucas on 06/03/18.
 */

public class ItemGaleryViewModel extends BaseViewModel {

    public final ObservableField<String> pathImagem = new ObservableField<>();
    public final ObservableField<String> videoSize = new ObservableField<>();
    public ObservableBoolean obsMarginViewVisible;
    public String cicle;


    private GaleryModel galeryModel;

    public ItemGaleryViewModel(String cicle, GaleryModel galeryModel, boolean marginViewVisible, CallbackBasicViewModel callback) {
        super(callback);
        this.cicle = cicle;
        this.galeryModel = galeryModel;
        obsMarginViewVisible = new ObservableBoolean(marginViewVisible);
        init();
    }

    private void init() {
        pathImagem.set(galeryModel.getVideoPath());
        videoSize.set(Utils.formatMilliseconds(galeryModel.getVideoTime()));
    }

    public void onClick(View view) {
        if (verifyVideos(galeryModel.getVideoPath())) {
            EvidencesDetailEditableActivity.openActivity((Activity) view.getContext(), cicle, galeryModel.getVideoPath());
        } else {
            callback.showSimpleDialog(R.string.same_video_error, null);
        }
    }

    public boolean verifyVideos(String videoPath) {
        List<OfflineEvidenceModel> evidenceModels = TeachGrowthApplication.getInstance().getEvidences(cicle);
        for (OfflineEvidenceModel evidenceModel : evidenceModels) {
            if (evidenceModel.getVideoPath() != null && evidenceModel.getVideoPath().equals(videoPath)) {
                return false;
            }
        }
        List<OfflineEvidenceModel> evidenceUploadModels = TeachGrowthApplication.getInstance().getUploadEvidences(cicle);
        for (OfflineEvidenceModel evidenceModel : evidenceUploadModels) {
            if (evidenceModel.getVideoPath() != null && evidenceModel.getVideoPath().equals(videoPath)) {
                return false;
            }
        }
        return true;
    }

}
