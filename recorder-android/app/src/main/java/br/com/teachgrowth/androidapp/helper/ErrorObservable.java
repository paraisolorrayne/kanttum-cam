package br.com.teachgrowth.androidapp.helper;

import android.databinding.BaseObservable;

/**
 * Created by gabrielaraujo on 19/11/2017.
 */

public class ErrorObservable extends BaseObservable {

    private String error;
    private Integer errorString;

    public ErrorObservable() {
    }

    public ErrorObservable(String error) {
        this.error = error;
    }

    public ErrorObservable(int errorString) {
        this.errorString = errorString;
    }

    public void set(String error) {
        this.error = error;
        notifyChange();
    }

    public String get() {
        return error;
    }

    public void set(int error) {
        this.errorString = error;
        notifyChange();
    }

    public int getIntError() {
        return errorString;
    }

    public void clear() {
        this.error = null;
        this.errorString = null;
        notifyChange();
    }

    public boolean hasError() {
        return error != null || errorString != null;
    }

}
