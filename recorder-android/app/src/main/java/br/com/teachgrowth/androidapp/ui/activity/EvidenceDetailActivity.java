package br.com.teachgrowth.androidapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import org.parceler.Parcels;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.databinding.ActivityEvidenceDetailBinding;
import br.com.teachgrowth.androidapp.helper.NotificationService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.ui.interfaces.OnPlayerIsPreparedListener;
import br.com.teachgrowth.androidapp.ui.widget.VideoPlayer;
import br.com.teachgrowth.androidapp.viewmodel.EvidencesDetailViewModel;

public class EvidenceDetailActivity
        extends BaseActivityViewModel<ActivityEvidenceDetailBinding, EvidencesDetailViewModel> {

    public static void openActivity(Activity activity, OfflineEvidenceModel evidenceModel){
        Intent intent = new Intent(activity, EvidenceDetailActivity.class);
        intent.putExtra(Constants.Extra.BUNDLE_EVIDENCE, Parcels.wrap(evidenceModel));
        activity.startActivity(intent);
    }

    OnPlayerIsPreparedListener onPlayerIsPreparedListener = () -> {
        // Restore state members from saved instance
        mBinding.videoPlayer.setCurrentTimer(TeachGrowthApplication.getInstance().getVideoCurrentPlaying());
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_evidence_detail);
        mViewModel =
                new EvidencesDetailViewModel(Parcels.unwrap(getIntent()
                        .getParcelableExtra(Constants.Extra.BUNDLE_EVIDENCE)), this);

        mBinding.setViewModel(mViewModel);
        new NotificationService(this).clear(mViewModel.getEvidenceModel().getTransferObserverId());
        initLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.videoPlayer.init(mViewModel.getVideoPath(),
                getFullScreenCallback(), mViewModel.playerIsPrepared);
        mBinding.videoPlayer.setOnPlayerIsPreparedListener(onPlayerIsPreparedListener);
    }

    void initLayout() {
        getWindow().setStatusBarColor(getResources().getColor(R.color.black_two));
        setUpVideoToolbar();
    }

    private VideoPlayer.FullScreenCallback getFullScreenCallback() {
        return () -> VideoFullScreenActivity.openActivity(EvidenceDetailActivity.this, mViewModel.getEvidenceModel());
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBinding.videoPlayer.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBinding.videoPlayer.onDestroy();
    }
}
