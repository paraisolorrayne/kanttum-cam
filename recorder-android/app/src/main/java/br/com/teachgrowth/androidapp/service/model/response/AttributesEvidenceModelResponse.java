package br.com.teachgrowth.androidapp.service.model.response;

/**
 * Created by lucas on 17/04/18.
 */

public class AttributesEvidenceModelResponse {
    private int id;
    private String type;

    private class EvidenceModel {
        private String status;
        private String stage_number;
        private String initiated_at;
        private String finished_at;
        private String created_at;
        private String updated_at;

    }
}
