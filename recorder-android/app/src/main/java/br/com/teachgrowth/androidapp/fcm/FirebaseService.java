package br.com.teachgrowth.androidapp.fcm;

import android.text.TextUtils;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.Objects;

import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;


/**
 * Created by lucas on 11/01/2018. Updated by Bruno Myrrha on 21/08/2018. Adapted to new FirebaseService implementation.
 */

public class FirebaseService extends FirebaseMessagingService {
    private static TeachGrowthApplication app = TeachGrowthApplication.getInstance();

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        saveToken(s);
    }

    public void registerDevice() {
        if (TextUtils.isEmpty(app.getDeviceToken())) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(
                    task -> {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        saveToken(Objects.requireNonNull(task.getResult()).getToken());
                    }
            );
        }
    }

    private static void saveToken(String token) {
        if (!TextUtils.isEmpty(token) && app.getUserModel() != null) {
            app.setDeviceToken(token);
            app.sendDeviceTokenToApi();
        }
    }
}