package br.com.teachgrowth.androidapp.ui.adapter;

import android.view.View;

import com.genius.groupie.Item;

import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ItemEvidenceBinding;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceDetailActivity;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivity;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.EvidenceListViewModel;
import br.com.teachgrowth.androidapp.viewmodel.ItemEvidencesViewModel;

/**
 * Created by lucas on 22/03/18.
 */

public class EvidenceListItem extends Item<ItemEvidenceBinding> {

    private EvidenceListViewModel.AdapterController adapterController;
    private CallbackBasicViewModel callback;
    private OfflineEvidenceModel evidenceModel;
    private ItemEvidencesViewModel itemEvidencesViewModel;
    private String cicle;
    private List<Integer> selectedPositions = new ArrayList<>();
    private boolean selectedPossibility = false;

    public EvidenceListItem(String cicle, OfflineEvidenceModel evidenceModel, CallbackBasicViewModel callback, EvidenceListViewModel.AdapterController adapterController) {
        this.callback = callback;
        this.cicle = cicle;
        this.evidenceModel = evidenceModel;
        this.adapterController = adapterController;
    }

    @Override
    public int getLayout() {
        return R.layout.item_evidence;
    }

    @Override
    public void bind(ItemEvidenceBinding viewBinding, int position) {
        itemEvidencesViewModel = new ItemEvidencesViewModel(cicle, evidenceModel, callback, true);
        if (selectedPositions.contains(position))
            itemEvidencesViewModel.isSelected.set(true);
        itemEvidencesViewModel.isSelectedPossibility.set(selectedPossibility);
        viewBinding.setViewModel(itemEvidencesViewModel);
        //viewBinding.partialStatusUploading.setViewModel(itemEvidencesViewModel);
        viewBinding.imgVideo.setClipToOutline(true);

        viewBinding.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!itemEvidencesViewModel.isSelectedPossibility.get())
                    EvidenceDetailActivity.openActivity((BaseActivity) view.getContext(), evidenceModel);
                else {
                    if (!itemEvidencesViewModel.isSelected.get())
                        selectedPositions.add(position);
                    else
                        selectedPositions.remove(selectedPositions.indexOf(position));
                    adapterController.onSelectedItem(position, itemEvidencesViewModel.isSelected.get());
                }
            }
        });
    }

    public void updateProgress(OfflineEvidenceModel evidenceModel) {
        if (itemEvidencesViewModel != null) {
            itemEvidencesViewModel.changeStatus(evidenceModel);
        }
    }

    public void updateStateSelected(boolean b) {
        itemEvidencesViewModel.isSelected.set(b);
    }

    public void updateStateSelectedPossibility(boolean b) {
        if (itemEvidencesViewModel != null)
            itemEvidencesViewModel.isSelectedPossibility.set(b);
        selectedPossibility = b;
        if (!selectedPossibility)
            selectedPositions.clear();
    }
}
