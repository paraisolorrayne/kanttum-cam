package br.com.teachgrowth.androidapp.helper;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import br.com.teachgrowth.androidapp.R;

public class NotificationService {

    private static NotificationCompat.Builder notificationBuilder;
    private Context context;
    private Integer notificationId;

    public NotificationService(Context context) {
        this.context = context;
    }

    private static NotificationCompat.Builder mBuilder(Context context) {
        String notificationChannelId = "upload";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mNotificationChannel = new NotificationChannel(
                    notificationChannelId,
                    "Recorder_Upload",
                    NotificationManager.IMPORTANCE_MIN);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mNotificationChannel);
            }
        }

        Bitmap teachGrowthLargeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.teach_growth);
        return new NotificationCompat.Builder(context, notificationChannelId)
                .setSmallIcon(R.drawable.ic_teachgrowth_statusbar)
                .setLargeIcon(teachGrowthLargeIcon)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setColor(context.getResources().getColor(R.color.colorPrimary))
                .setAutoCancel(true);
    }

    public NotificationService configure(String title, String content) {
        notificationBuilder = mBuilder(context);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(content);
        if (content.length() > 14) {
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        }
        return this;
    }

    public void configureId(int notificationId) {
        this.notificationId = notificationId;
    }

    public void configureProgressBar() {
        notificationBuilder.setProgress(100, 0, false);
    }

    public void updateProgress(int progress) {
        if (progress < 100) {
            notificationBuilder.setContentText("O upload está em andamento - " + progress + "% concluido.");
            notificationBuilder.setProgress(100, progress, false);
            show();
        } else {
            notificationBuilder.setContentText("Upload completado.");
            notificationBuilder.setProgress(0, 0, false);
            show();
        }
    }

    public void addIntent(Intent intent) {
        notificationBuilder.setContentIntent(
                PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        );
    }

    public void addAction(String text, Intent[] intents) {
        PendingIntent pendingIntent = PendingIntent.getActivities(context, notificationId, intents, PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder.addAction(new NotificationCompat.Action.Builder(R.drawable.bg_transparent, text, pendingIntent).build());
    }


    public void addAction(String text, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, intent, PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder.addAction(new NotificationCompat.Action.Builder(R.drawable.bg_transparent, text, pendingIntent).build());
    }

    public void clear(int id) {
        NotificationManagerCompat.from(context).cancel(id);
    }

    public void show() {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId == null ? 0 : notificationId, notificationBuilder.build());
    }

    public void alertUser() {
        notificationBuilder.setVibrate(new long[]{
                150, 300, 150, 600
        });
        try {
            RingtoneManager
                    .getRingtone(
                            context,
                            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                    ).play();
        } catch (Exception e) {
            Toast.makeText(context, "Erro ao emitir som de notificação.", Toast.LENGTH_SHORT).show();
            Log.e("RINGTONE", e.getMessage());
        }
    }
}