package br.com.teachgrowth.androidapp.service.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 28/02/18.
 */

public class AttributeLoginRequest {

    @SerializedName("attributes")
    private LoginRequest loginRequest;

    public AttributeLoginRequest(String email, String senha) {
        this.loginRequest = new LoginRequest(email, senha);
    }

    public class LoginRequest {

        @SerializedName("email")
        private String email;
        @SerializedName("password")
        private String password;

        public LoginRequest(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }
}
