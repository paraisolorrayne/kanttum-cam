package br.com.teachgrowth.androidapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;

import org.parceler.Parcels;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.databinding.ActivityVideoFullScreenBinding;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.ui.interfaces.OnPlayerIsPreparedListener;
import br.com.teachgrowth.androidapp.viewmodel.VideoFullScreenViewModel;

public class VideoFullScreenActivity extends BaseActivityViewModel<ActivityVideoFullScreenBinding, VideoFullScreenViewModel> {

    public static void openActivity(Activity activity, OfflineEvidenceModel evidenceModel) {
        Intent intent = new Intent(activity, VideoFullScreenActivity.class);
        intent.putExtra(Constants.Extra.BUNDLE_EVIDENCE, Parcels.wrap(evidenceModel));
        activity.startActivity(intent);
    }

    public static void openActivity(Activity activity, String videoPath) {
        Intent intent = new Intent(activity, VideoFullScreenActivity.class);
        intent.putExtra(Constants.Extra.VIDEO_PATH, videoPath);
        activity.startActivity(intent);
    }

    OnPlayerIsPreparedListener onPlayerIsPreparedListener = () -> {
        // Restore state members from saved instance
        mBinding.videoPlayer.setCurrentTimer(TeachGrowthApplication.getInstance().getVideoCurrentPlaying());
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_full_screen);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ViewGroup.LayoutParams params = mBinding.videoPlayer.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        mBinding.videoPlayer.setLayoutParams(params);

        String videoPath = getIntent().getStringExtra(Constants.Extra.VIDEO_PATH);
        if (videoPath == null) {
            mViewModel = new VideoFullScreenViewModel((OfflineEvidenceModel) Parcels.unwrap(getIntent()
                    .getParcelableExtra(Constants.Extra.BUNDLE_EVIDENCE)), this);
        } else {
            mViewModel = new VideoFullScreenViewModel(videoPath, this);
        }

        mBinding.setViewModel(mViewModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.videoPlayer.init(mViewModel.getVideoPath(),
                this::finish);

        mBinding.videoPlayer.setOnPlayerIsPreparedListener(onPlayerIsPreparedListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBinding.videoPlayer.onPause();
        Log.i("current_time", "onStop - FULL - t: " + TeachGrowthApplication.getInstance().getVideoCurrentPlaying());
    }
}
