package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableFloat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.genius.groupie.GroupAdapter;

import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.fcm.FirebaseService;
import br.com.teachgrowth.androidapp.service.UploadVideoService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.activity.GalleryActivity;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivity;
import br.com.teachgrowth.androidapp.ui.adapter.EvidenceUploadItem;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EvidenceUploadViewModel extends BaseViewModel {
    public final GroupAdapter groupAdapter = new GroupAdapter();
    public ObservableBoolean listIsEmpty = new ObservableBoolean(false);
    public ObservableFloat alpha = new ObservableFloat();
    CustomCallback callback;
    boolean firstRun = true;
    private String cicle;
    private FirebaseService service = new FirebaseService();
    private List<OfflineEvidenceModel> evidenceModels;
    private List<Integer> itemsSelected = new ArrayList<Integer>();
    AdapterController adapterController = new AdapterController() {
        @Override
        public void onSelectedItem(int position, boolean isSelected, boolean isLongClick) {
            Integer aux = searchIfItemHasSelected(position);
            if (isLongClick || itemsSelected.size() > 0) {
                if (aux != null) {
                    if (!isSelected)
                        itemsSelected.add(position);
                    else
                        itemsSelected.remove(aux);
                } else
                    itemsSelected.add(position);

                ((EvidenceUploadItem) groupAdapter.getItem(position)).updateStateSelected(!isSelected);
            }

            callback.updateDeleteToolbar(itemsSelected.size());
            updateListItemsState();
        }

    };
    private ObservableBoolean allItemsAlready = new ObservableBoolean(false);

    public EvidenceUploadViewModel(String cicle, CustomCallback callback) {
        super(callback);
        this.callback = callback;
        this.cicle = cicle;
        init();
        if (!TeachGrowthApplication.getInstance().isTokenSaved()) service.registerDevice();
        allItemsAlready.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {

            }
        });
        alpha.set(0.0f);
    }

    private void init() {
        setList();
    }

    public void addEvidence(View view) {
        GalleryActivity.openGalleryActivity((BaseActivity) view.getContext(), cicle);
    }

    public void setList() {
        evidenceModels = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        groupAdapter.clear();
        for (OfflineEvidenceModel evidenceModel : evidenceModels) {
            Log.d("setlist", "entrou");
            groupAdapter.add(new EvidenceUploadItem(cicle, evidenceModel, callback, adapterController));
        }
        listIsEmpty.set(evidenceModels.size() == 0);
        animatedTransitionFadeIn(alpha);
        UploadVideoService.checkIfAllItemsAlready();
    }

    public void needHelp(View view) {

    }

    public void updateProgressList(OfflineEvidenceModel evidenceModel) {
        if (evidenceModels != null) {
            for (int i = 0; i < evidenceModels.size(); i++) {
                if (!TextUtils.isEmpty(evidenceModels.get(i).getVideoPath()) && evidenceModels.get(i).getVideoPath().equals(evidenceModel.getVideoPath())) {
                    evidenceModels.get(i).setVideoStatus(evidenceModel.getVideoStatus());
                    EvidenceUploadItem itemEvidencesViewModel = (EvidenceUploadItem) groupAdapter.getItem(i);
                    evidenceModels.get(i).setTransferObserverId(evidenceModel.getTransferObserverId());
                    if (itemEvidencesViewModel != null) {
                        itemEvidencesViewModel.updateProgress(evidenceModel);
                    }
                    break;
                }
            }
        }
        UploadVideoService.checkIfAllItemsAlready();
    }

    public void removeCompletedUploadFromList(OfflineEvidenceModel evidenceModel) {
        if (evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHED.getId())) {
            evidenceModels.remove(evidenceModel);

            TeachGrowthApplication.getInstance().removeUploadEvidence(evidenceModel, cicle);

            updateListSate();
        }
    }

    private void updateListItemsState() {
        if (itemsSelected.size() > 0)
            for (int i = 0; i < groupAdapter.getItemCount(); i++)
                ((EvidenceUploadItem) groupAdapter.getItem(i)).updateStateSelectedPossibility(true);
        else
            for (int i = 0; i < groupAdapter.getItemCount(); i++)
                ((EvidenceUploadItem) groupAdapter.getItem(i)).updateStateSelectedPossibility(false);
    }

    private Integer searchIfItemHasSelected(int position) {
        for (int i : itemsSelected) {
            if (i == position)
                return position;
        }
        return null;
    }

    public void clearSelectedList() {
        for (int i : itemsSelected) {
            ((EvidenceUploadItem) groupAdapter.getItem(i)).updateStateSelected(false);
        }
        itemsSelected.clear();
        updateListItemsState();
        if (!firstRun)
            callback.updateDeleteToolbar(0);
        else firstRun = false;
    }

    public boolean hasItemsSelected() {
        return itemsSelected.size() > 0;
    }

    public void deleteSelectedItems() {
        callback.showDialogProgress();
        ArrayList<Integer> aux = new ArrayList<>();
        aux.addAll(itemsSelected);
        for (int i : aux) {
            if (!evidenceModels.get(i).getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHED.getId()) && !evidenceModels.get(i).getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHING.getId())) {
                TeachGrowthApplication.getInstance().deleteEvidence.onNext(evidenceModels.get(i));
                removeItems(i, false);
            }
        }

        deleteEvidenceOnLine();
    }

    private void deleteEvidenceOnLine() {
        if (itemsSelected.size() > 0) {
            RetrofitBase.getInterfaceRetrofit()
                    .deleteVideosEvidence(evidenceModels.get(itemsSelected.get(itemsSelected.size() - 1)).getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        if (response.code() == 200) {
                            removeItems(response.body().getMetaResponse().getId(), true);
                            deleteEvidenceOnLine();
                        } else {
                            handlerErrorsDelete(null, evidenceModels.get(itemsSelected.get(itemsSelected.size() - 1)).getVideoTitle());
                        }
                    }, throwable -> {
                        handlerErrorsDelete(throwable, evidenceModels.get(itemsSelected.get(itemsSelected.size() - 1)).getVideoTitle());
                    });
        }
    }

    private void handlerErrorsDelete(Throwable throwable, String evidenceName) {
        if (throwable != null)
            throwable.printStackTrace();
        itemsSelected.clear();
        updateListSate();
        callback.showSimpleDialog("Ocorreu um erro ao excluir a evidencia: " + evidenceName);
        callback.hideDialogProgress();
    }

    private void removeItems(int i, boolean isVideoId) {
        if (isVideoId)
            i = findIndexEvidenceById(i);

        itemsSelected.remove(i);
        TeachGrowthApplication.getInstance().removeUploadEvidence(evidenceModels.get(i), cicle);

        if (itemsSelected.size() == 0)
            updateListSate();
    }

    private void updateListSate() {
        evidenceModels = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        listIsEmpty.set(evidenceModels.size() == 0);
        animatedTransitionFadeIn(alpha);
        callback.updateDeleteToolbar(0);
        setList();
        callback.hideDialogProgress();
    }

    private int findIndexEvidenceById(int id) {
        for (int i : itemsSelected)
            if (evidenceModels.get(i).getId() == id)
                return i;

        return 0;
    }

    public void setUploadInProgress(Boolean allItemsAlready) {
        this.allItemsAlready.set(allItemsAlready);
    }

    public void startDownloadIfDoesnt() {
        evidenceModels = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        for (OfflineEvidenceModel evidenceModel : evidenceModels) {
            TeachGrowthApplication.getInstance().evidenceResumeSubject.onNext(evidenceModel);
        }
    }

    public interface AdapterController {
        void onSelectedItem(int position, boolean isSelected, boolean isLongClick);
    }

    public interface CustomCallback extends CallbackBasicViewModel {
        void updateDeleteToolbar(Integer count);
    }
}
