package br.com.teachgrowth.androidapp.ui.activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.databinding.ActivityEvidenceListBinding;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.EvidenceListViewModel;

public class EvidenceListActivity extends BaseActivityViewModel<ActivityEvidenceListBinding, EvidenceListViewModel> implements EvidenceListViewModel.CustomCallback {

    private String cycle = Constants.Cycle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_evidence_list);
        mViewModel = new EvidenceListViewModel(cycle, this);

        mBinding.setViewModel(mViewModel);

        setUpToolbar(false, R.string.title_activity_evidence_list);
        setUpDrawerMenu(R.id.drawer_video_library);
        //UploadVideoService.prepairEvidencesDowload(this);

        int notificationId = getIntent().getIntExtra("notificationId", -1);
        if (notificationId != -1)
            NotificationManagerCompat.from(this).cancel("notification", notificationId);

        if(getIntent().getBooleanExtra(Constants.Extra.LOGIN_FROM_SSO,false)) {
            mViewModel.addEvidence(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewModel.clearSelectedList();
    }

    @Override
    public void onBackPressed() {
        if (mViewModel.hasItemsSelected())
            mViewModel.clearSelectedList();
        else
            super.onBackPressed();
    }
}
