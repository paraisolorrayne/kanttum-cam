package br.com.teachgrowth.androidapp.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ActivitySettingsBinding;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.SettingsViewModel;

public class SettingsActivity extends BaseActivityViewModel<ActivitySettingsBinding, SettingsViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        mViewModel = new SettingsViewModel(this);
        mBinding.setViewModel(mViewModel);

        setUpToolbar(false, R.string.title_activity_settings);
        setUpDrawerMenu(R.id.drawer_settings);

    }
}
