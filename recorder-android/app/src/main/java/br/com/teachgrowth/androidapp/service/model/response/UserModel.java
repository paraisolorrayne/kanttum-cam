package br.com.teachgrowth.androidapp.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 27/02/18.
 */

public class UserModel {

    @SerializedName("id")
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("attributes")
    private Attributes attributes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getAccess_token() {
        return attributes.getAccess_token();
    }

    public void setAccess_token(String access_token) {
        if (attributes == null) {
            attributes = new Attributes();
        }

        attributes.setAccess_token(access_token);
    }

    public String getToken_type() {
        return attributes.getToken_type();
    }

    public void setToken_type(String token_type) {
        attributes.setToken_type(token_type);
    }
    private class Attributes {

        @SerializedName("access_token")
        private String access_token;

        @SerializedName("token_type")
        private String token_type;

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

    }
}
