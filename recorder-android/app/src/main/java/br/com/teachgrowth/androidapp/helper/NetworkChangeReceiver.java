package br.com.teachgrowth.androidapp.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import br.com.teachgrowth.androidapp.service.UploadVideoService;

/**
 * Created by lucas on 30/01/18.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TESTETAREFA", "SERVIÇO CRIADO NETWORK");
        context.startService(new Intent(context.getApplicationContext(), UploadVideoService.class));
    }

}