package br.com.teachgrowth.androidapp.ui.interfaces;

import android.app.DialogFragment;
import android.os.Bundle;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by gabrielaraujo on 18/11/2017.
 */

public interface CallbackBasicViewModel {

    void showDialogProgress();

    void hideDialogProgress();

    void showError(Throwable t, MaterialDialog.SingleButtonCallback callback);

    void openActivity(Class<?> openActivity);

    void openActivity(Class<?> openActivity, String name, String value);

    void openActivityNewTask(Class<?> openActivity);

    MaterialDialog getBasicMaterialDialog();

    void showDialogFragment(DialogFragment dialogFragment);

    void finish(int resultCode);

    void finish(int resultCode, Bundle bundle);

    void showSimpleDialog(String msg);

    void showSimpleDialog(int title, int text, int textPositive, int textNegative, MaterialDialog.SingleButtonCallback callback);

    void showSimpleDialog(int text, int textPositive, int textNegative, MaterialDialog.SingleButtonCallback callback);

    void showSimpleDialog(int msg, MaterialDialog.SingleButtonCallback callback);

    String getString(int resString);

    String getString(int resString, Object... formatArgs);

}
