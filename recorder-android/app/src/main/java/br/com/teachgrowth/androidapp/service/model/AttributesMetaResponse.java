package br.com.teachgrowth.androidapp.service.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mateus on 20/04/18.
 */

public class AttributesMetaResponse {
    @SerializedName("meta")
    private AttributesMetaResponse.MetaResponse metaResponse;

    public MetaResponse getMetaResponse() {
        return metaResponse;
    }

    public void setMetaResponse(MetaResponse metaResponse) {
        this.metaResponse = metaResponse;
    }

    public class MetaResponse {

        Integer id;

        Comments comments;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Comments getComments() {
            return comments;
        }

        public void setComments(Comments comments) {
            this.comments = comments;
        }

        public class Comments {
            Boolean available;

            public Boolean getAvailable() {
                return available;
            }

            public void setAvailable(Boolean available) {
                this.available = available;
            }
        }
    }
}
