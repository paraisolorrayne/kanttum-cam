package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import br.com.teachgrowth.androidapp.BuildConfig;
import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.helper.ErrorObservable;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.request.ResetPasswordRequest;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by lucas on 05/03/18.
 */

public class RecoverPasswordViewModel extends BaseViewModel {

    public final ObservableField<String> email = new ObservableField<>();

    public final ErrorObservable errorEmail = new ErrorObservable();

    public final PublishSubject<String> onSuccess = PublishSubject.create();

    public RecoverPasswordViewModel(CallbackBasicViewModel callback) {
        super(callback);
        if (BuildConfig.DEBUG) {
            email.set("user_h@kanttum.com");
        }
    }

    public void clickRecover(View v) {
        if (verifyItens()) {
            callApi();
        }
    }

    private boolean verifyItens() {
        if (TextUtils.isEmpty(email.get())) {
            errorEmail.set(callback.getString(R.string.msg_error_email));
            return false;
        }

        return true;
    }

    private void callApi() {
        showProgress();
        RetrofitBase.getInterfaceRetrofit()
                .resetPassord(getRequest())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userModel -> {
                            onSuccess.onNext(email.get());
                            hideProgress();
                        }, this::handlerErrors,
                        () -> hideProgress());
    }

    private DataModel<ResetPasswordRequest, Object, Object> getRequest() {
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(email.get());
        return new DataModel(resetPasswordRequest);
    }

    private void handlerErrors(Throwable t) {
        hideProgress();
        showError(t, (dialog, which) -> {
        });
    }
}
