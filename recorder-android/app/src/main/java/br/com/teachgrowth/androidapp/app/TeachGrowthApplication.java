package br.com.teachgrowth.androidapp.app;

import android.content.Intent;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.google.firebase.FirebaseApp;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.helper.PreferencesManager;
import br.com.teachgrowth.androidapp.helper.PrefsHelper;
import br.com.teachgrowth.androidapp.service.UploadVideoService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.model.RequestRegisterDevice;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.response.AttributesTokenResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserDetailResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserModel;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.activity.LoginActivity;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx_activity_result.RxActivityResult;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static br.com.teachgrowth.androidapp.app.Constants.TYPE_TOKEN;

/**
 * Created by lucas on 23/02/18.
 */

public class TeachGrowthApplication extends MultiDexApplication {

    public static PreferencesManager prefs;
    private static TeachGrowthApplication instance;
    public final PublishSubject<OfflineEvidenceModel> evidenceSubject = PublishSubject.create();
    public final PublishSubject<OfflineEvidenceModel> evidenceResumeSubject = PublishSubject.create();
    public final PublishSubject<Boolean> allItemsAlreadySubject = PublishSubject.create();
    public final PublishSubject<OfflineEvidenceModel> deleteEvidence = PublishSubject.create();
    public PrefsHelper prefsHelper;
    private UserModel userModel;
    private static boolean videoPlaying = false;
    private static int videoCurrentPlaying = 0;

    public static TeachGrowthApplication getInstance() {
        return instance;
    }

    public static boolean isVideoPlaying() {
        return videoPlaying;
    }

    public static void setVideoPlaying(boolean videoIsPlaying) {
        TeachGrowthApplication.videoPlaying = videoIsPlaying;
    }

    public int getVideoCurrentPlaying() {
        return videoCurrentPlaying;
    }

    public void setVideoCurrentPlaying(int currentPlaying) {
        videoCurrentPlaying = currentPlaying;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        init();
        Hawk.init(this);
        FirebaseApp.initializeApp(this);
        initializeApplication();
    }

    private void initializeApplication() {
        AWSConfiguration awsConfiguration = new AWSConfiguration(getApplicationContext());
        // If IdentityManager is not created, create it
        if (IdentityManager.getDefaultIdentityManager() == null) {
            IdentityManager identityManager =
                    new IdentityManager(getApplicationContext(), awsConfiguration);
            IdentityManager.setDefaultIdentityManager(identityManager);
        }
    }

    public void logoutUser() {
        deletedeTokenDivice();
        userModel = null;
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        for (TransferObserver transferObserver : UploadVideoService.getObservers()) {
            UploadVideoService.getTransferUtility().deleteTransferRecord(transferObserver.getId());
        }
    }

    public void logoutUserSSO(UserModel userModel) {
        setUserModel(userModel);

        deletedeTokenDivice();

        for (TransferObserver transferObserver : UploadVideoService.getObservers()) {
            UploadVideoService.getTransferUtility().deleteTransferRecord(transferObserver.getId());
        }
    }

    private void deletedeTokenDivice() {

        if (prefsHelper.getTokenResponse() != null) {
            AttributesTokenResponse token = prefsHelper.getTokenResponse();

            RetrofitBase.getInterfaceRetrofit()
                    .deleteTokenDivice(Integer.parseInt(token.getId()))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
        prefs.clear();
    }

    public String getDeviceToken() {
        return prefs.getString(Constants.SharedPreferences.DEVICE_TOKEN);
    }

    public void setDeviceToken(String token) {
        if (!TextUtils.isEmpty(token)) {
            prefs.setValue(Constants.SharedPreferences.DEVICE_TOKEN, token);
        }
    }

    public void sendDeviceTokenToApi() {
        Log.e("Notification", instance.getDeviceToken());

        String token = instance.getDeviceToken();
        String device_type = Constants.DEVICE_TYPE;
        RetrofitBase.getInterfaceRetrofit()
                .registerDevice(new DataModel<>(new RequestRegisterDevice(token, device_type)))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSucces -> {
                            Log.e("Notification Success", onSucces.getData().getTokenResponse().getToken());
                            tokenSuccess(onSucces);
                        },
                        onError -> {
                            Log.e("Notification", onError.getLocalizedMessage());
                        });
    }

    public void tokenSuccess(DataModel<AttributesTokenResponse, Object, Object> onSucces) {
        prefsHelper.saveTokenResponse(onSucces.getData());
        prefsHelper.tokenSuccess();
    }

    public boolean isTokenSaved() {
        return prefsHelper.isTokenSaved();
    }

    public void saveEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        prefsHelper.saveEvidence(evidenceModel, cicle);
    }

    public void updateEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        prefsHelper.updateEvidence(evidenceModel, cicle);
    }

    public void removeEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        prefsHelper.removeEvidence(evidenceModel, cicle);
    }

    public List<OfflineEvidenceModel> getEvidences(String cicle) {
        return prefsHelper.getEvidences(cicle);
    }

    public void saveUploadEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        prefsHelper.saveUploadEvidence(evidenceModel, cicle);
    }

    public void updateUploadEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        prefsHelper.updateUploadEvidence(evidenceModel, cicle);
    }

    public void removeUploadEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        prefsHelper.removeUploadEvidence(evidenceModel, cicle);
    }

    public List<OfflineEvidenceModel> getUploadEvidences(String cicle) {
        return prefsHelper.getUploadEvidences(cicle);
    }

    public void updateEvidence(List<OfflineEvidenceModel> evidenceModels) {
        prefs.setValue(String.format(Constants.SharedPreferences.EVIDENCES, Constants.Cycle), "");
        for (OfflineEvidenceModel evidenceModel : evidenceModels) {
            saveEvidence(evidenceModel, Constants.Cycle);
        }
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public UserDetailResponse getUserDetail() {
        return prefsHelper.getUserDetail();
    }

    public boolean isUserPreferenceSettingsAllow3g() {
        return prefsHelper.getUserPreferenceSettings();
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
        prefsHelper.saveUser(userModel);
    }

    public void setUserDetail(UserDetailResponse userDetail) {
        prefsHelper.saveUserDetail(userDetail);
    }

    public void setUserPreferenceSettingsAllow3g(boolean userPreferenceSettingsAllow3g) {
        prefsHelper.saveUserPreferenceSettings(userPreferenceSettingsAllow3g);
    }

    public String getToken() {
        try {
            return TYPE_TOKEN + userModel.getAccess_token();
        } catch (Exception e) {
            return "";
        }
    }

    private void init() {
        RxActivityResult.register(this);
        initPrefs();
        initUser();
        configFonts();
    }

    private void initUser() {
        userModel = prefsHelper.getUser();
    }

    private void initPrefs() {
        PreferencesManager.initializeInstance(this);
        prefs = PreferencesManager.getInstance();
        prefsHelper = new PrefsHelper();
    }

    private void configFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.proxima_nova_soft_medium))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
