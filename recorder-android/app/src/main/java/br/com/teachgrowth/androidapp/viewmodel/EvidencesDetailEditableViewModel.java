package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableField;
import android.text.TextUtils;
import android.util.Log;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.helper.ErrorObservable;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceUploadActivity;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;

/**
 * Created by lucas on 19/03/18.
 */

public class EvidencesDetailEditableViewModel extends BaseViewModel {
    public final ObservableField<String> videoTitle = new ObservableField<>();
    public final ObservableField<String> description = new ObservableField<>();

    public final ErrorObservable errorDescription = new ErrorObservable();
    public final ErrorObservable errorVideoName = new ErrorObservable();
    private String cicle;
    private String videoPath;
    private OfflineEvidenceModel evidenceModel;

    public EvidencesDetailEditableViewModel(String cicle, String videoPath, CallbackBasicViewModel callback) {
        super(callback);
        this.cicle = cicle;
        this.videoPath = videoPath;
    }

    public EvidencesDetailEditableViewModel(String cicle,
                                            OfflineEvidenceModel evidenceModel, CallbackBasicViewModel callback) {
        super(callback);
        this.cicle = cicle;
        this.evidenceModel = evidenceModel;
        videoTitle.set(evidenceModel.getVideoTitle());
        description.set(evidenceModel.getVideoDescription());
    }

    public void saveEvidence(int videoDuration) {
        Log.d("entrouSave", "entrou");
        if (verifyItens()) {
            if (evidenceModel == null) {
                Log.d("entrouSave", "entrou2");
                TeachGrowthApplication.getInstance().saveUploadEvidence(getEvidence(videoPath, videoDuration), cicle);
            } else {
                Log.d("entrouSave", "entrou3");
                evidenceModel.setVideoTitle(videoTitle.get());
                evidenceModel.setVideoDescription(description.get());
                TeachGrowthApplication.getInstance().updateUploadEvidence(evidenceModel, cicle);
            }
            callback.openActivityNewTask(EvidenceUploadActivity.class);
        }
    }

    private OfflineEvidenceModel getEvidence(String videoPath, int videoDuration) {
        return new OfflineEvidenceModel(videoPath,
                videoDuration,
                videoTitle.get(),
                description.get(),
                EvidenceStatusEnum.STATUS_NOT_STARTED.getId());
    }

    private boolean verifyItens() {
        if (TextUtils.isEmpty(videoTitle.get())) {
            callback.showSimpleDialog(callback.getString(R.string.msg_error_name_video));
            return false;
        }
        return true;
    }

    public String getVideoPath() {
        if (evidenceModel != null) {
            return evidenceModel.getVideoPath();
        } else {
            return videoPath;
        }
    }

    public OfflineEvidenceModel getEvidenceModel() {
        return evidenceModel;
    }
}
