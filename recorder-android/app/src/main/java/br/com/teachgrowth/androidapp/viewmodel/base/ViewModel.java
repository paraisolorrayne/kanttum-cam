package br.com.teachgrowth.androidapp.viewmodel.base;

/**
 * Created by gabrielaraujo on 18/11/2017.
 */

public interface ViewModel {
    void resume();
    void destroy();
}
