package br.com.teachgrowth.androidapp.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.parceler.Parcels;

import java.util.List;

import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.helper.NotificationService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.model.UploadFinishedNotificationModel;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceDetailActivity;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceListActivity;

/**
 * Created by lucas on 11/01/2018. Updated by Bruno Myrrha on 22/08/18
 */

public class FirebaseMessaging extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("Notification", "remoteMessage " + remoteMessage.getData().get(Constants.Notifications.MESSAGE));
        Intent intent = new Intent(getApplicationContext(), EvidenceListActivity.class);
        intent.putExtra(Constants.Extra.NOTIFICATION, true);

        UploadFinishedNotificationModel uploadFinishedNotificationModel = new UploadFinishedNotificationModel();
        uploadFinishedNotificationModel.setMedium_id(Integer.parseInt(remoteMessage.getData().get(Constants.Firebase.EVIDENCE_ID)));
        uploadFinishedNotificationModel.setType(remoteMessage.getData().get(Constants.Firebase.TYPE));

        NotificationService notification = new NotificationService(getApplicationContext())
                .configure(
                        remoteMessage.getData().get(Constants.Notifications.NOTIFICATION_TITLE),
                        remoteMessage.getNotification().getBody()
                );

        if (uploadFinishedNotificationModel.getType().equals("upload")) {
            OfflineEvidenceModel evidence = TeachGrowthApplication.getInstance().prefsHelper.getEvidenceUploadById(uploadFinishedNotificationModel.getMedium_id(), Constants.Cycle);
            if (evidence != null) {
                new NotificationService(TeachGrowthApplication.getInstance()).clear(evidence.getTransferObserverId());

                notification.configureId(evidence.getTransferObserverId());
                Intent Intent = new Intent(getApplicationContext(), EvidenceDetailActivity.class);
                Intent.putExtra(Constants.Extra.BUNDLE_EVIDENCE, Parcels.wrap(evidence));
                Intent backIntent = new Intent(getApplicationContext(), EvidenceListActivity.class);

                Intent clearIntent = new Intent("CLEAR_NOTIFICATION");
                clearIntent.putExtra("notificationId", evidence.getTransferObserverId());

                notification.addAction("VER EVIDÊNCIA", new Intent[]{backIntent, Intent});
                notification.addAction("SAIR", clearIntent);
            }
            updateEvidence(uploadFinishedNotificationModel.getMedium_id());
        }
        notification.show();
    }

    private void updateEvidence(int evidenceId) {
        List<OfflineEvidenceModel> evidenceModels = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        for (OfflineEvidenceModel evidenceModel : evidenceModels) {
            if (evidenceModel.getId() == evidenceId) {
                Log.d("FINAL", "removeu");
                evidenceModel.setVideoStatus(EvidenceStatusEnum.STATUS_FINISHED.getId());
                TeachGrowthApplication.getInstance().updateEvidence(evidenceModel, Constants.Cycle);
                TeachGrowthApplication.getInstance().removeUploadEvidence(evidenceModel, Constants.Cycle);

                TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidenceModel);
            }
        }
    }
}
