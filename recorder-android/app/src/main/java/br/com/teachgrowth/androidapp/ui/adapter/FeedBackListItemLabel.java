package br.com.teachgrowth.androidapp.ui.adapter;

import com.genius.groupie.Item;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.databinding.ItemFeedbackLabelBinding;
import br.com.teachgrowth.androidapp.viewmodel.ItemFeedbackLabelViewModel;

/**
 * Created by mateus on 12/04/18.
 */

public class FeedBackListItemLabel extends Item<ItemFeedbackLabelBinding> {

    private String label;
    private int colorStatus;

    public FeedBackListItemLabel(String label, int color) {
        this.label = label;
        this.colorStatus = color;
    }

    @Override
    public int getLayout() {
        return R.layout.item_feedback_label;
    }

    @Override
    public void bind(ItemFeedbackLabelBinding viewBinding, int position) {
        viewBinding.setViewModel(new ItemFeedbackLabelViewModel(label, colorStatus));
    }
}
