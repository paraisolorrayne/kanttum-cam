package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.text.TextUtils;
import android.view.View;

import br.com.teachgrowth.androidapp.BuildConfig;
import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.helper.ErrorObservable;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.service.model.request.AttributeLoginRequest;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.response.UserDetailResponse;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.activity.EvidenceListActivity;
import br.com.teachgrowth.androidapp.ui.activity.RecoverPasswordActivity;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucas on 27/02/18.
 */

public class LoginViewModel extends BaseViewModel {

    public final ObservableField<String> email = new ObservableField<>();
    public final ObservableField<String> senha = new ObservableField<>();
    public final ObservableInt inputType = new ObservableInt(129);
    public final ObservableInt resDrawablePassword = new ObservableInt(R.drawable.icon_eye_deactive);
    public final ObservableBoolean enableBtnShowPassword = new ObservableBoolean(false);

    public final ErrorObservable errorEmail = new ErrorObservable();
    public final ErrorObservable errorSenha = new ErrorObservable();

    public LoginViewModel(CallbackBasicViewModel callback) {
        super(callback);
        senha.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (TextUtils.isEmpty(senha.get())) {
                    resDrawablePassword.set(R.drawable.icon_eye_deactive);
                    enableBtnShowPassword.set(false);
                    inputType.set(129);
                } else {
                    resDrawablePassword.set(R.drawable.icon_eye_normal);
                    enableBtnShowPassword.set(true);
                }
            }
        });
        if (BuildConfig.DEBUG) {
            email.set("fabio@kanttum.com");
            senha.set("123qweasd");
        }
    }

    public void clickLogin(View v) {
        Utils.hideKeyboard(v);
        if (verifyItens()) {
            callApi();
        }
    }

    public void viewPassword(View v) {
        inputType.set(inputType.get() == 129 ? 1 : 129);
    }

    public void clickEsqueciSenha(View view) {
        openActivity(RecoverPasswordActivity.class);
    }

    private boolean verifyItens() {
        if (TextUtils.isEmpty(email.get())) {
            errorEmail.set(callback.getString(R.string.msg_error_email));
            return false;
        }

        if (TextUtils.isEmpty(senha.get())) {
            errorSenha.set(callback.getString(R.string.msg_error_senha));
            return false;
        }
        return true;
    }

    private void callApi() {
        showProgress();
        RetrofitBase.getInterfaceRetrofit()
                .login(getRequest())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userModel -> {
                            TeachGrowthApplication.getInstance().setUserModel(userModel.getData());
                            callApiUserDetail();
                        }, this::handlerErrors,
                        () -> hideProgress());
    }

    private DataModel<AttributeLoginRequest, Object, Object> getRequest() {
        AttributeLoginRequest loginRequest =
                new AttributeLoginRequest(email.get(), senha.get());
        return new DataModel(loginRequest);
    }

    private void callApiUserDetail() {
        RetrofitBase.getInterfaceRetrofit()
                .getUser()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess,
                        this::handlerErrors,
                        () -> {
                        });
    }

    private void onSuccess(DataModel<UserDetailResponse, Object, Object> response) {
        if (response != null)
            TeachGrowthApplication.getInstance().setUserDetail(response.getData());
        hideProgress();
        callback.openActivityNewTask(EvidenceListActivity.class);
    }

    private void handlerErrors(Throwable t) {
        hideProgress();
        showError(t, (dialog, which) -> {
        });
    }
}
