package br.com.teachgrowth.androidapp.app;


import android.os.Environment;

public class Constants {
    public static final String BUCKET_REGION = "us-east-1";
    public static final String BUCKET = "dev-kanttum-backend-transcoder-in";
    public static final String COGNITO_POOL_ID = "us-east-1:0edc702c-9895-4487-b918-b43eb774ce0a";
    static final String DEVICE_TYPE = "android";
    public static String Cycle = "cycle";
    static String TYPE_TOKEN = "Bearer ";
    public static final String FINAL_VIDEO_TYPE = ".mp4";
    public static final String GALLERY_VIDEO_PATH =
            Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/";

    public class Extra {
        public static final String EMAIL = "email";
        public static final String VIDEO_PATH = "video_path";
        public static final String BUNDLE_EVIDENCE = "evidence";
        public static final String NOTIFICATION = "notification";
        public static final String BUNDLE_CYCLE = "cycle";
        public static final String LOGIN_FROM_SSO = "longin_from_sso";
    }

    public class Service {
        public static final String LOGIN = "auth/login";
        public static final String RESET_PASSWORD = "auth/reset_password";
        public static final String UPLOAD = "uploads";
        public static final String REGISTER_DEVICE = "devices";
        public static final String EVIDENCES_VIDEOS = "videos";
        public static final String CREATE_VIDEO = "videos";
        public static final String DELETE_VIDEO = "videos/{id}";
        public static final String DELETE_TOKEN_DIVICE = "devices/{id}";
        public static final String GET_VIDEO_COMMENTS = "institutions/{institution_id}/evidences/{evidence_id}/videos/{id}/comments?include=author,criteria";
        public static final String GET_VIDEO = "videos/{id}";
        public static final String GET_USER_DETAIL = "user";
    }

    public class SharedPreferences {
        static final String DEVICE_TOKEN = "device_token";
        public static final String TOKEN_RESPONSE = "token_response";
        public static final String PATH = "teachgrowth";
        public static final String USER = "user";
        public static final String USER_DETAIL = "user_Detail";
        public static final String USER_PREFENCE_SETTINGS = "User_preference_settings";
        public static final String EVIDENCES = "evidences_%s";
        public static final String UPLOAD_EVIDENCES = "Upload_evidences_%s";
        public static final String TOKEN_SAVED = "token_saved";
    }

    public class Notifications {
        public static final String NOTIFICATION_TITLE = "title";
        public static final String MESSAGE = "message";
    }

    public class Urls {
        public static final String FEDDBACK = "http://kanttum.com/feedback";
        public static final String ABOUT_GROWTH = "http://kanttum.com/teachgrowth";
        public static final String ABOUT_KANTTUM = "http://kanttum.com";
    }

    public class Firebase {
        public static final String TYPE = "type";
        public static final String EVIDENCE_ID = "medium_id";
    }

}
