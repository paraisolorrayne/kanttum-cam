package br.com.teachgrowth.androidapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.DialogAction;

import org.parceler.Parcels;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.databinding.ActivityEvidencesDetailEditableBinding;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.ui.interfaces.OnPlayerIsPreparedListener;
import br.com.teachgrowth.androidapp.ui.widget.VideoPlayer;
import br.com.teachgrowth.androidapp.viewmodel.EvidencesDetailEditableViewModel;

public class EvidencesDetailEditableActivity
        extends BaseActivityViewModel<ActivityEvidencesDetailEditableBinding, EvidencesDetailEditableViewModel> {

    public static void openActivity(Activity activity, String cycle, String path) {
        Intent intent = new Intent(activity, EvidencesDetailEditableActivity.class);
        intent.putExtra(Constants.Extra.BUNDLE_CYCLE, cycle);
        intent.putExtra(Constants.Extra.VIDEO_PATH, path);
        activity.startActivity(intent);
    }

    public static void openActivity(Activity activity, String cicle, OfflineEvidenceModel evidenceModel) {
        Intent intent = new Intent(activity, EvidencesDetailEditableActivity.class);
        intent.putExtra(Constants.Extra.BUNDLE_CYCLE, cicle);
        intent.putExtra(Constants.Extra.BUNDLE_EVIDENCE, Parcels.wrap(evidenceModel));
        activity.startActivity(intent);
    }

    OnPlayerIsPreparedListener onPlayerIsPreparedListener = () -> {
        // Restore state members from saved instance
        mBinding.videoPlayer.setCurrentTimer(TeachGrowthApplication.getInstance().getVideoCurrentPlaying());
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_evidences_detail_editable);
        setUpToolbar(true, R.string.upload);

        if (getIntent().getParcelableExtra(Constants.Extra.BUNDLE_EVIDENCE) != null) {
            mViewModel =
                    new EvidencesDetailEditableViewModel(
                            getIntent().getStringExtra(Constants.Extra.BUNDLE_CYCLE),
                            (OfflineEvidenceModel) Parcels.unwrap(getIntent()
                                    .getParcelableExtra(Constants.Extra.BUNDLE_EVIDENCE)), this);
        } else {
            mViewModel = new EvidencesDetailEditableViewModel(
                    getIntent().getStringExtra(Constants.Extra.BUNDLE_CYCLE),
                    getIntent().getStringExtra(Constants.Extra.VIDEO_PATH),
                    this);
        }
        mBinding.setViewModel(mViewModel);
        mBinding.videoPlayer.init(mViewModel.getVideoPath(),
                getFullScreenCallback());
        mBinding.videoPlayer.setOnPlayerIsPreparedListener(onPlayerIsPreparedListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBinding.videoPlayer.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBinding.videoPlayer.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.evidence_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_send:
                if (mBinding.videoPlayer.getDuration() > 0) {
                    mViewModel.saveEvidence(mBinding.videoPlayer.getDuration());
                } else {
                    showSimpleDialog("Espaço insuficiente para enviar Evidência.");
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private VideoPlayer.FullScreenCallback getFullScreenCallback() {
        return () -> {
            if (mViewModel.getEvidenceModel() != null) {
                VideoFullScreenActivity.openActivity(EvidencesDetailEditableActivity.this, mViewModel.getEvidenceModel());
            } else {
                VideoFullScreenActivity.openActivity(EvidencesDetailEditableActivity.this, mViewModel.getVideoPath());
            }
        };
    }

    @Override
    public void onBackPressed() {
        showSimpleDialog(R.string.confirm_exit, R.string.cancel, R.string.exit, (dialog, which) -> {
            if (which == DialogAction.NEGATIVE)
                finish();
        });
    }
}
