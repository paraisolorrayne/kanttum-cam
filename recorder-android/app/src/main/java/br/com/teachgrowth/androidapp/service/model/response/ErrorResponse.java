package br.com.teachgrowth.androidapp.service.model.response;

public class ErrorResponse {

    private String status;
    private String title;
    private String detail;

    public ErrorResponse(String detail) {
        this.detail = detail;
    }

    public ErrorResponse(String title, String detail) {
        this.title = title;
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
