package br.com.teachgrowth.androidapp.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;

import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.databinding.ActivityGaleryBinding;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivity;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.GaleryViewModel;

public class GalleryActivity extends
        BaseActivityViewModel<ActivityGaleryBinding, GaleryViewModel> implements SurfaceHolder.Callback, Handler.Callback {

    private static final int MSG_CAMERA_OPENED = 1;
    private static final int MSG_SURFACE_READY = 2;
    private static final int REQUEST_VIDEO_CAPTURE = 10;
    private final Handler mHandler = new Handler(this);
    SurfaceHolder mSurfaceHolder;
    CameraManager mCameraManager;
    String[] mCameraIDsList;
    CameraDevice.StateCallback mCameraStateCB;
    CameraDevice mCameraDevice;
    CameraCaptureSession mCaptureSession;
    boolean mSurfaceCreated = true;
    boolean mIsCameraConfigured = false;
    private Surface mCameraSurface = null;

    public static void openGalleryActivity(BaseActivity activity, String cicle) {
        new RxPermissions(activity)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.CAMERA)
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        activity.openActivity(GalleryActivity.class, Constants.Extra.BUNDLE_CYCLE, cicle);
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_galery);
        mViewModel = new GaleryViewModel(getIntent().getStringExtra(Constants.Extra.BUNDLE_CYCLE), this);
        mBinding.setViewModel(mViewModel);

        this.mSurfaceHolder = mBinding.surfaceView.getHolder();
        this.mSurfaceHolder.addCallback(this);
        this.mCameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);

        try {
            mCameraIDsList = mCameraManager.getCameraIdList();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        mCameraStateCB = new CameraDevice.StateCallback() {
            @Override
            public void onOpened(CameraDevice camera) {
                mCameraDevice = camera;
            }

            @Override
            public void onDisconnected(CameraDevice camera) {
            }

            @Override
            public void onError(CameraDevice camera, int error) {
            }
        };
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
//            Uri videoUri = intent.getData();
//        }
//    }

    @Override
    protected void onStart() {
        super.onStart();

        mViewModel.setList();

        checkIfAdapterListHasChanged();

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCameraManager.openCamera(mCameraIDsList[0], mCameraStateCB, new Handler());
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopCamera();
    }

    private void checkIfAdapterListHasChanged() {
        if (mViewModel != null) {
            if (mViewModel.groupAdapter.getItemCount() == Utils.getVideos().size()) {
                new Handler().postDelayed(this::checkIfAdapterListHasChanged, 10 * 1000);
            } else {
                mViewModel.setList();
            }
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_CAMERA_OPENED:
            case MSG_SURFACE_READY:
                if (mSurfaceCreated && (mCameraDevice != null)
                        && !mIsCameraConfigured) {
                    configureCamera();
                }
                break;
        }

        return true;
    }

    private void configureCamera() {
        // prepare list of surfaces to be used in capture requests
        List<Surface> sfl = new ArrayList<Surface>();

        sfl.add(mCameraSurface); // surface for viewfinder preview

        // configure camera with all the surfaces to be ever used
        try {
            mCameraDevice.createCaptureSession(sfl,
                    new CaptureSessionListener(), null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        mIsCameraConfigured = true;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCameraSurface = holder.getSurface();
        mHandler.sendEmptyMessage(MSG_CAMERA_OPENED);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mCameraSurface = holder.getSurface();
        mSurfaceCreated = true;
        mHandler.sendEmptyMessage(MSG_SURFACE_READY);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mSurfaceCreated = false;
    }

    public void onBack(View view) {
        onBackPressed();
    }

    private class CaptureSessionListener extends
            CameraCaptureSession.StateCallback {
        @Override
        public void onConfigureFailed(final CameraCaptureSession session) {
        }

        @Override
        public void onConfigured(final CameraCaptureSession session) {
            mCaptureSession = session;
            try {
                CaptureRequest.Builder previewRequestBuilder = mCameraDevice
                        .createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                previewRequestBuilder.addTarget(mCameraSurface);
                mCaptureSession.setRepeatingRequest(previewRequestBuilder.build(),
                        null, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }


    public void onClick(View view) {
        if (view.getId() == R.id.surfaceView) {
            try {
                new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        try {
                            CameraDevice c = (CameraDevice) objects[0];
                            c.close();
                            openActivity(CameraVideoActivity.class, Constants.Extra.BUNDLE_CYCLE, mViewModel.getCicle());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;

                    }
                }.execute(mCameraDevice);

                mIsCameraConfigured = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        takeVideoIntent.putExtra(MediaStore.Video.Thumbnails.HEIGHT, 640);
        takeVideoIntent.putExtra(MediaStore.Video.Thumbnails.WIDTH, 480);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    public void onBackPressed() {
        stopCamera();
        super.onBackPressed();
    }

    public void stopCamera() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    CameraDevice c = (CameraDevice) objects[0];
                    c.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;

            }
        }.execute(mCameraDevice);

        mIsCameraConfigured = false;
    }
}
