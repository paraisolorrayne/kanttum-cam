package br.com.teachgrowth.androidapp.enums;

import br.com.teachgrowth.androidapp.R;

/**
 * Created by lucas on 28/02/18.
 */

public enum EvidenceStatusEnum {

    STATUS_NOT_STARTED("status_not_started", R.string.status_not_started, R.color.warm_grey, 0),
    STATUS_PAUSED("status_paused", R.string.status_paused, R.color.warm_grey, R.drawable.ic_portable_wifi_off),
    STATUS_DOWNLOADING("status_downloading", R.string.status_downloading, R.color.warm_grey, 0),
    STATUS_FAILED_AMAZON("status_failed_amazon", R.string.status_failed, R.color.tomato, R.drawable.ic_refresh),
    STATUS_FAILED_START_UPLOAD("status_failed_start_upload", R.string.status_failed, R.color.tomato, R.drawable.ic_refresh),
    STATUS_FAILED_CREATE_VIDEO("status_failed_create_video", R.string.status_failed, R.color.tomato, R.drawable.ic_refresh),
    STATUS_FINISHING("status_finishing", R.string.status_finishing, R.color.warm_grey, 0),
    STATUS_FINISHED("status_finished", R.string.status_finished, R.color.mid_green, R.drawable.ic_check);

    private String id;
    private int text;
    private int resColor;
    private int resDrawable;

    EvidenceStatusEnum(final String id, final int text, int resColor, int resDrawable) {
        this.text = text;
        this.id = id;
        this.resColor = resColor;
        this.resDrawable = resDrawable;
    }

    public String getId() {
        return id;
    }

    public int getText() {
        return text;
    }

    public int getResColor() {
        return resColor;
    }

    public int getResDrawable() {
        return resDrawable;
    }

    public static EvidenceStatusEnum getClientStatusEnumFromId(String id) {
        for (EvidenceStatusEnum e : values()) {
            if (e.id.equalsIgnoreCase(id)) return e;
        }
        return STATUS_PAUSED;
    }
}
