package br.com.teachgrowth.androidapp.ui.activity.base;

import android.databinding.ViewDataBinding;
import br.com.teachgrowth.androidapp.viewmodel.base.ViewModel;

/**
 * Created by gabrielaraujo on 18/11/2017.
 */

public class BaseActivityViewModel<T extends ViewDataBinding, V extends ViewModel> extends BaseActivity {

    protected T mBinding;
    protected V mViewModel;

    @Override
    protected void onDestroy() {
        if (mViewModel != null){
            mViewModel.destroy();
            mViewModel = null;
        }
        super.onDestroy();
    }
}
