/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.teachgrowth.androidapp.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.databinding.ActivityCameraVideoBinding;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.ui.activity.base.BaseActivityViewModel;
import br.com.teachgrowth.androidapp.viewmodel.CameraVideoViewModel;

import static br.com.teachgrowth.androidapp.helper.Utils.merge;

public class CameraVideoActivity extends BaseActivityViewModel<ActivityCameraVideoBinding, CameraVideoViewModel> implements SensorEventListener {

    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    private Boolean shouldItAutomaticallySaveTheVideo = true;
    private Size mPreviewSize;
    private MediaRecorder mMediaRecorder;
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private Integer mSensorOrientation;
    private CaptureRequest.Builder mPreviewBuilder;
    private CameraDevice mCameraDevice;
    private CameraCaptureSession mPreviewSession;
    private int camPosition = 0;
    private int contVideo = 0;
    private String cycleName = Constants.Cycle;
    private Context mContext;
    private SensorManager mSensorManager;
    protected PowerManager.WakeLock mWakeLock;

    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            startPreview();
            mCameraOpenCloseLock.release();
            if (null != mBinding.texture) {
                configureTransform(mBinding.texture.getWidth(), mBinding.texture.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            showBtnPlay();
            mCameraDevice = null;
//            finish();
        }

    };

    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

    };

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_camera_video);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mViewModel = new CameraVideoViewModel(this);
        mBinding.setViewModel(mViewModel);
        mBinding.partialFooterPortrait.setViewModel(mViewModel);
        mBinding.partialHeaderPortrait.setViewModel(mViewModel);
        mBinding.partialHeaderLandscape.setViewModel(mViewModel);
        mBinding.partialHeaderLandscapeRevert.setViewModel(mViewModel);
        mBinding.dialogCameraVideo.setViewModel(mViewModel);

        mViewModel.onTimer.subscribe(timer -> {
            startRecordingVideo();
        });

        //keep screen on
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm != null) {
            mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
        }
        mWakeLock.acquire(60 * 60 * 1000L /*60 minutes*/);

        //Sensor Android
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }


    @Override
    public void onResume() {
        super.onResume();
        mContext = this;
        shouldItAutomaticallySaveTheVideo = true;

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        mViewModel.getTimerHandler().removeCallbacks(mViewModel.getRecordVideoTimerRunnable());
        startBackgroundThread();
        if (mBinding.texture.isAvailable()) {
            openCamera(mBinding.texture.getWidth(), mBinding.texture.getHeight());
        } else {
            mBinding.texture.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void saveTheVideoIfItIsAppropriate() {
        if (shouldItAutomaticallySaveTheVideo && mViewModel.mIsRecordingVideo.get()) {
            AtomicReference<String> path = new AtomicReference<>();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    if (camPosition == 1) mViewModel.videoOrientation += 180;
                    path.set(merge(mContext, cycleName, mViewModel.videoOrientation));
                    return null;
                }
            }.execute();

            finish();
        }
    }

    public void clickBack(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (contVideo > 0 || mViewModel.mIsRecordingVideo.get()) {
            mViewModel.clickBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPause() {
        mSensorManager.unregisterListener(this);
        stopBackgroundThread();
        closeCamera();
        saveTheVideoIfItIsAppropriate();
        mViewModel.mIsRecordingVideo.set(false);
        super.onPause();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_play: {
                if (mViewModel.mIsRecordingVideo.get()) {
                    closeCamera();
                    stopBackgroundThread();
                    mViewModel.mIsRecordingVideo.set(false);
                    onResume();
                    contVideo++;
                } else {
                    if (!isMoreThan30MinutesToRecorder()) {
                        mViewModel.showDialogTimeRecord(minutesRemainingInMemory());
                    } else {
                        mViewModel.startTimer();
                    }
                }
                break;
            }
            case R.id.texture:
                if (mViewModel.mIsRecordingVideo.get()) {
                    closeCamera();
                    stopBackgroundThread();
                    mViewModel.mIsRecordingVideo.set(false);
                    onResume();
                    contVideo++;
                }
                break;
            case R.id.btn_cancel:
                showBtnPlay();
                break;
            case R.id.btn_save:
                finishCameraActivity();
                break;
            case R.id.btn_flip_cam:
                camPosition = camPosition == 0 ? 1 : 0;
                closeCamera();
                stopBackgroundThread();
                mViewModel.mIsRecordingVideo.set(false);
                onResume();
                break;
        }
    }

    private boolean isMoreThan30MinutesToRecorder() {
        return (minutesRemainingInMemory() > 30);
    }

    private long minutesRemainingInMemory() {
        return ((getAvailableInternalMemorySize() / ((sizeOfFile()) / 8)) / 60);
    }

    public long getAvailableInternalMemorySize() {
        File path = mContext.getExternalFilesDir(Constants.GALLERY_VIDEO_PATH);
        StatFs stat = null;
        if (path != null) {
            stat = new StatFs(path.getPath());
        }
        long blockSize = 0;
        if (stat != null) {
            blockSize = stat.getBlockSizeLong();
        }
        long availableBlocks = 0;
        if (stat != null) {
            availableBlocks = stat.getAvailableBlocksLong();
        }
        return availableBlocks * blockSize;
    }

    private long sizeOfFile() {
        int quality = CamcorderProfile.QUALITY_480P;
        if (CamcorderProfile.hasProfile(CamcorderProfile.QUALITY_2160P))
            quality = CamcorderProfile.QUALITY_2160P;
        CamcorderProfile profile = CamcorderProfile.get(quality);
        return profile.videoBitRate + profile.audioBitRate;
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        showBtnPlay();

        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void openCamera(int width, int height) {
        //todo request permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (isFinishing()) {
            return;
        }
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            String cameraId = null;
            if (manager != null) {
                cameraId = manager.getCameraIdList()[camPosition];
            }

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = null;
            if (cameraId != null) {
                characteristics = manager.getCameraCharacteristics(cameraId);
            }
            StreamConfigurationMap map = null;
            if (characteristics != null) {
                map = characteristics
                        .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            }
            if (map == null) {
                throw new RuntimeException("Cannot get available preview/video sizes");
            }
            Size mVideoSize = Utils.chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            mPreviewSize = Utils.chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    width, height, mVideoSize);

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mBinding.texture.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                mBinding.texture.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }
            configureTransform(width, height);
            mMediaRecorder = new MediaRecorder();
            manager.openCamera(cameraId, mStateCallback, null);
        } catch (CameraAccessException e) {
            Toast.makeText(this, "Não pode acessar a camera.", Toast.LENGTH_SHORT).show();
            finish();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            Utils.showSimpleDialog(this,
                    R.string.ocorreu_um_erro,
                    (dialog, which) -> finish());

        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.");
        }
    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            closePreviewSession();
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mMediaRecorder) {
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.");
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    private void startPreview() {
        if (null == mCameraDevice || !mBinding.texture.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            SurfaceTexture texture = mBinding.texture.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            Surface previewSurface = new Surface(texture);
            mPreviewBuilder.addTarget(previewSurface);

            mCameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Toast.makeText(CameraVideoActivity.this, "Falha na configuração", Toast.LENGTH_SHORT).show();
                        }
                    }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void updatePreview() {
        if (null == mCameraDevice) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
            HandlerThread thread = new HandlerThread("CameraPreview");
            thread.start();
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == mBinding.texture || null == mPreviewSize) {
            return;
        }
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mBinding.texture.setTransform(matrix);
    }

    private void setUpMediaRecorder() throws IOException {
        String mNextVideoAbsolutePath = Utils.getVideoFileTempPath(this, cycleName, Utils.generateVideoName("video_"));
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        //CURRENT VIDEO SIZE: 18mb/min
        int bitrateForRecord = 8000000;
        mMediaRecorder.setVideoEncodingBitRate(bitrateForRecord);
        mMediaRecorder.setVideoFrameRate(30);
//        mMediaRecorder.setAudioEncodingBitRate(30);
        mMediaRecorder.setVideoSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
        mMediaRecorder.setAudioChannels(1);
        mMediaRecorder.setAudioSamplingRate(44100);
        mMediaRecorder.setAudioEncodingBitRate(96000);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        switch (mSensorOrientation) {
            case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                break;
            case SENSOR_ORIENTATION_INVERSE_DEGREES:
                mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                break;
        }
        mMediaRecorder.prepare();

    }

    private void startRecordingVideo() {
        if (null == mCameraDevice || !mBinding.texture.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            setUpMediaRecorder();
            SurfaceTexture texture = mBinding.texture.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            List<Surface> surfaces = new ArrayList<>();

            // Set up Surface for the camera preview
            Surface previewSurface = new Surface(texture);
            surfaces.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);

            // Set up Surface for the MediaRecorder
            Surface recorderSurface = mMediaRecorder.getSurface();
            surfaces.add(recorderSurface);
            mPreviewBuilder.addTarget(recorderSurface);

            // Start a capture session
            // Once the session starts, we can update the UI and start recording
            mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {

                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    mPreviewSession = cameraCaptureSession;
                    updatePreview();
                    runOnUiThread(() -> {
                        showBtnStop();
                        mViewModel.mIsRecordingVideo.set(true);
                        mMediaRecorder.start();
                    });
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    showBtnPlay();
                    Toast.makeText(CameraVideoActivity.this, "Falha no processo", Toast.LENGTH_SHORT).show();
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    private void closePreviewSession() {
        if (mPreviewSession != null) {
            mPreviewSession.close();
            mPreviewSession = null;
        }
    }

    private void showBtnPlay() {
        mBinding.partialFooterPortrait.btnPlay.setBackground(ContextCompat.getDrawable(this, R.drawable.bt_record));
    }

    private void showBtnStop() {
        mBinding.partialFooterPortrait.btnPlay.setBackground(ContextCompat.getDrawable(this, R.drawable.bt_record_pause));
    }

    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];

        if (x < 5 && x > -5 && y > 5) {
            if (mViewModel.orientation.get() != 0) {
                mViewModel.orientation.set(0);
                mViewModel.showLandscapeViews.set(false);
                mViewModel.showLandscapeViewsRevert.set(false);
                mViewModel.showPortraitViews.set(true);
            }
        } else if (x < -5 && y < 5 && y > -5) {
            if (mViewModel.orientation.get() != 90) {
                mViewModel.orientation.set(90);
                mViewModel.showLandscapeViews.set(true);
                mViewModel.showLandscapeViewsRevert.set(false);
                mViewModel.showPortraitViews.set(false);
            }
        } else if (x < 5 && x > -5 && y < -5) {
            if (mViewModel.orientation.get() != 180) {
//                mViewModel.orientation.set(180);
                mViewModel.orientation.set(0);
                mViewModel.showLandscapeViews.set(false);
                mViewModel.showLandscapeViewsRevert.set(false);
                mViewModel.showPortraitViews.set(true);
            }
        } else if (x > 5 && y < 5 && y > -5) {
            if (mViewModel.orientation.get() != 270) {
                mViewModel.orientation.set(270);
                mViewModel.showLandscapeViews.set(false);
                mViewModel.showLandscapeViewsRevert.set(true);
                mViewModel.showPortraitViews.set(false);
            }
        }
        if (contVideo == 0 && !mViewModel.mIsRecordingVideo.get()) {
            mViewModel.videoOrientation = mViewModel.orientation.get();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @SuppressLint("StaticFieldLeak")
    private void finishCameraActivity() {
        shouldItAutomaticallySaveTheVideo = false;
        Handler handler = new Handler();
        AtomicReference<String> path = new AtomicReference<>();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                mBinding.getViewModel().showProgressBar.set(View.VISIBLE);
                if (camPosition == 1) mViewModel.videoOrientation += 180;

                path.set(merge(mContext, cycleName, mViewModel.videoOrientation));

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                handler.postDelayed(() -> {

                    mBinding.getViewModel().showProgressBar.set(View.INVISIBLE);
                    if (new File(path.get()).exists()) {
                        EvidencesDetailEditableActivity.openActivity((Activity) mContext, cycleName, path.get());
                        finish();
                    } else {
                        showBtnPlay();
                        mViewModel.deleteVideo(mContext);
                    }
                }, 50);
            }
        }.execute();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Utils.showSimpleDialog(this,
                R.string.error_memory,
                (dialog, which) -> finish());

    }
}
