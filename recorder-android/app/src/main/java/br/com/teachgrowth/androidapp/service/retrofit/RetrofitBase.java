package br.com.teachgrowth.androidapp.service.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import br.com.teachgrowth.androidapp.BuildConfig;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitBase {

    private static RestApi interfaceRetrofit;

    public static RestApi getInterfaceRetrofit() {
        if (interfaceRetrofit == null) new RetrofitBase();
        return interfaceRetrofit;
    }

    private RetrofitBase() {
        initRetrofit();
    }

    private void initRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(getHeaderInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logInterceptor);
        }

        GsonBuilder gsonBuilder = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);

        Gson gson = gsonBuilder.create();
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gson);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(builder.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .build();

        interfaceRetrofit = retrofit.create(RestApi.class);
    }

    private static Interceptor getHeaderInterceptor() {
        return chain -> {
            Request original = chain.request();

            Request.Builder requestBuilder = original.newBuilder();

            if (!original.url().toString().contains("transcoder-kanttum-in.s3.amazonaws.com")) {
                requestBuilder.addHeader("Accept", "application/vnd.growth.v1");

                if (TeachGrowthApplication.getInstance().getUserModel() != null) {
                    String token = TeachGrowthApplication.getInstance().getToken();
                    requestBuilder.addHeader("Authorization", token);
                }
            }

            requestBuilder.method(original.method(), original.body());
            return chain.proceed(requestBuilder.build());
        };
    }
}
