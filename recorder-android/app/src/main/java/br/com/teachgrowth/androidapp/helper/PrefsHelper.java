package br.com.teachgrowth.androidapp.helper;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.orhanobut.hawk.Hawk;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.model.response.AttributesTokenResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserDetailResponse;
import br.com.teachgrowth.androidapp.service.model.response.UserModel;

/**
 * Created by gabrielaraujo on 18/12/2017.
 */

public class PrefsHelper {

    public void tokenSuccess() {
        TeachGrowthApplication.prefs.setValue(Constants.SharedPreferences.TOKEN_SAVED, true);
    }

    public boolean isTokenSaved() {
        return TeachGrowthApplication.prefs.getBoolean(Constants.SharedPreferences.TOKEN_SAVED, false);
    }

    public void saveTokenResponse(AttributesTokenResponse tokenResponse) {
        if (tokenResponse == null) return;
        String json = Utils.objectToString(tokenResponse);
        TeachGrowthApplication.prefs.setValue(Constants.SharedPreferences.TOKEN_RESPONSE, json);
    }

    public void saveUser(UserModel userModel) {
        if (userModel == null) return;
        String json = Utils.objectToString(userModel);
        TeachGrowthApplication.prefs.setValue(Constants.SharedPreferences.USER, json);
    }

    public void saveUserDetail(UserDetailResponse userDetail) {
        if (userDetail == null) return;
        String json = Utils.objectToString(userDetail);
        TeachGrowthApplication.prefs.setValue(Constants.SharedPreferences.USER_DETAIL, json);
    }

    public void saveUserPreferenceSettings(boolean userDetail) {
        TeachGrowthApplication.prefs.setValue(Constants.SharedPreferences.USER_PREFENCE_SETTINGS, userDetail);
    }

    public boolean getUserPreferenceSettings() {
        return TeachGrowthApplication.prefs.getBoolean(Constants.SharedPreferences.USER_PREFENCE_SETTINGS);
    }

    public AttributesTokenResponse getTokenResponse() {
        String json = TeachGrowthApplication.prefs.getString(Constants.SharedPreferences.TOKEN_RESPONSE);
        if (TextUtils.isEmpty(json)) {
            return null;
        }
        return (AttributesTokenResponse) Utils.jsonToObject(json, AttributesTokenResponse.class);
    }

    public UserModel getUser() {
        String json = TeachGrowthApplication.prefs.getString(Constants.SharedPreferences.USER);
        if (TextUtils.isEmpty(json)) {
            return null;
        }
        return (UserModel) Utils.jsonToObject(json, UserModel.class);
    }

    public UserDetailResponse getUserDetail() {
        String json = TeachGrowthApplication.prefs.getString(Constants.SharedPreferences.USER_DETAIL);
        if (TextUtils.isEmpty(json)) {
            return null;
        }
        return (UserDetailResponse) Utils.jsonToObject(json, UserDetailResponse.class);
    }

    public void saveEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        List<OfflineEvidenceModel> evidences = getEvidences(cicle);
        evidences.add(evidenceModel);
        TeachGrowthApplication.prefs.setValue(
                String.format(Constants.SharedPreferences.EVIDENCES, cicle), Utils.objectToString(evidences));
    }

    public void updateEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        List<OfflineEvidenceModel> evidences = getEvidences(cicle);
        for (int i=0; i<evidences.size(); i++) {
            if (evidences.get(i).getId() != 0) {
                if (evidences.get(i).getId() == evidenceModel.getId()) {
                    evidences.set(i, evidenceModel);
                }
            } else {
                if (evidences.get(i).getVideoPath().equals(evidenceModel.getVideoPath())) {
                    evidences.set(i, evidenceModel);
                }
            }
        }
        TeachGrowthApplication.prefs.setValue(
                String.format(Constants.SharedPreferences.EVIDENCES, cicle), Utils.objectToString(evidences));
    }

    public void removeEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        List<OfflineEvidenceModel> evidences = getEvidences(cicle);
        for (int i=0; i<evidences.size(); i++) {
            if (evidences.get(i).getVideoPath() != null && evidences.get(i).getVideoPath().equals(evidenceModel.getVideoPath()))
                evidences.remove(i);
            else if (evidences.get(i).getId() == evidenceModel.getId())
                evidences.remove(i);
        }
        TeachGrowthApplication.prefs.setValue(
                String.format(Constants.SharedPreferences.EVIDENCES, cicle), Utils.objectToString(evidences));
    }

    public List<OfflineEvidenceModel> getEvidences(String cicle) {
        try {
            Type listType = new TypeToken<List<OfflineEvidenceModel>>() {
            }.getType();
            String json = TeachGrowthApplication.prefs.getString(
                    String.format(Constants.SharedPreferences.EVIDENCES, cicle));


            if (!TextUtils.isEmpty(json)) {
                List<OfflineEvidenceModel> list =  (List<OfflineEvidenceModel>) Utils.jsonToObject(json, listType);
                return list == null ? new ArrayList<>() : list;
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public OfflineEvidenceModel getEvidenceUploadById(int id, String cicle) {
        List<OfflineEvidenceModel> list = getUploadEvidences(cicle);
        for (OfflineEvidenceModel e : list) {
            if (e.getId() == id)
                return e;
        }
        return null;
    }

    public OfflineEvidenceModel getEvidenceById(int id, String cicle) {
        List<OfflineEvidenceModel> list = getEvidences(cicle);
        for (OfflineEvidenceModel e : list) {
            if (e.getId() == id)
                return e;
        }
        return null;
    }

    public void saveUploadEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        List<OfflineEvidenceModel> evidences = getUploadEvidences(cicle);
        evidences.add(evidenceModel);
        TeachGrowthApplication.prefs.setValue(
                String.format(Constants.SharedPreferences.UPLOAD_EVIDENCES, cicle), Utils.objectToString(evidences));
    }

    public void updateUploadEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        List<OfflineEvidenceModel> evidences = getUploadEvidences(cicle);
        for (int i=0; i<evidences.size(); i++) {
            if (evidences.get(i).getId() != 0) {
                if (evidences.get(i).getId() == evidenceModel.getId()) {
                    evidences.set(i, evidenceModel);
                }
            } else {
                if (evidences.get(i).getVideoPath().equals(evidenceModel.getVideoPath())) {
                    evidences.set(i, evidenceModel);
                }
            }
        }
        TeachGrowthApplication.prefs.setValue(
                String.format(Constants.SharedPreferences.UPLOAD_EVIDENCES, cicle), Utils.objectToString(evidences));
    }

    public void removeUploadEvidence(OfflineEvidenceModel evidenceModel, String cicle) {
        List<OfflineEvidenceModel> evidences = getUploadEvidences(cicle);
        for (int i=0; i<evidences.size(); i++) {
            if (evidences.get(i).getVideoPath() != null && evidences.get(i).getVideoPath().equals(evidenceModel.getVideoPath()))
                evidences.remove(i);
        }
        TeachGrowthApplication.prefs.setValue(
                String.format(Constants.SharedPreferences.UPLOAD_EVIDENCES, cicle), Utils.objectToString(evidences));
    }

    public List<OfflineEvidenceModel> getUploadEvidences(String cicle) {
        try {
            Type listType = new TypeToken<List<OfflineEvidenceModel>>() {
            }.getType();
            String json = TeachGrowthApplication.prefs.getString(
                    String.format(Constants.SharedPreferences.UPLOAD_EVIDENCES, cicle));


            if (!TextUtils.isEmpty(json)) {
                List<OfflineEvidenceModel> list =  (List<OfflineEvidenceModel>) Utils.jsonToObject(json, listType);
                return list == null ? new ArrayList<>() : list;
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public OfflineEvidenceModel getUploadEvidenceById(int id, String cicle) {
        List<OfflineEvidenceModel> list = getUploadEvidences(cicle);
        for (OfflineEvidenceModel e : list) {
            if (e.getId() == id)
                return e;
        }
        return null;
    }

}
