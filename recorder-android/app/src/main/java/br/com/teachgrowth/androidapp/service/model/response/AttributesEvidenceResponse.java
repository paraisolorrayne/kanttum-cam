package br.com.teachgrowth.androidapp.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 19/04/18.
 */

public class AttributesEvidenceResponse {

    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("attributes")
    private Evidence evidence;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStep() {
        return this.evidence.getStep();
    }

    public void setStep(String step) {
        this.evidence.setStep(step);
    }

    public int getStageNumber() {
        return this.evidence.getStageNumber();
    }

    public void setStageNumber(int stageNumber) {
        this.evidence.setStageNumber(stageNumber);
    }

    public String getInitiatedAt() {
        return this.evidence.getInitiatedAt();
    }

    public void setInitiatedAt(String initiatedAt) {
        this.evidence.setInitiatedAt(initiatedAt);
    }

    public String getFinishOn() {
        return this.evidence.getFinishOn();
    }

    public void setFinishOn(String finishOn) {
        this.evidence.setFinishOn(finishOn);
    }

    public String getFinishedAt() {
        return this.evidence.getFinishedAt();
    }

    public void setFinishedAt(String finishedAt) {
        this.evidence.setFinishedAt(finishedAt);
    }

    public String getCreatedAt() {
        return this.evidence.getCreatedAt();
    }

    public void setCreatedAt(String createdAt) {
        this.evidence.setCreatedAt(createdAt);
    }

    private class Evidence {

        @SerializedName("step")
        private String step;
        @SerializedName("stage_number")
        private int stageNumber;
        @SerializedName("initiated_at")
        private String initiatedAt;
        @SerializedName("finish_on")
        private String finishOn;
        @SerializedName("finished_at")
        private String finishedAt;
        @SerializedName("created_at")
        private String createdAt;

        public Evidence(String step, int stageNumber, String initiatedAt,
                        String finishOn, String finishedAt, String createdAt) {
            this.step = step;
            this.stageNumber = stageNumber;
            this.initiatedAt = initiatedAt;
            this.finishOn = finishOn;
            this.finishedAt = finishedAt;
            this.createdAt = createdAt;
        }

        public String getStep() {
            return step;
        }

        public void setStep(String step) {
            this.step = step;
        }

        public int getStageNumber() {
            return stageNumber;
        }

        public void setStageNumber(int stageNumber) {
            this.stageNumber = stageNumber;
        }

        public String getInitiatedAt() {
            return initiatedAt;
        }

        public void setInitiatedAt(String initiatedAt) {
            this.initiatedAt = initiatedAt;
        }

        public String getFinishOn() {
            return finishOn;
        }

        public void setFinishOn(String finishOn) {
            this.finishOn = finishOn;
        }

        public String getFinishedAt() {
            return finishedAt;
        }

        public void setFinishedAt(String finishedAt) {
            this.finishedAt = finishedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }
    }
}
