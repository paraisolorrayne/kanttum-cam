package br.com.teachgrowth.androidapp.service.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 12/04/18.
 */

public class AttributesCreateVideoRequest {

    @SerializedName("attributes")
    private AttributesCreateVideoRequest.VideoRequest videoRequest;

    public AttributesCreateVideoRequest(String title, String description,
                                        String fileName, int duration, String kanttumUploadId) {
        this.videoRequest = new AttributesCreateVideoRequest.VideoRequest(title,
                description,
                fileName,
                duration);
    }

    public class VideoRequest {
        @SerializedName("title")
        private String title;
        @SerializedName("description")
        private String description;
        @SerializedName("file_name")
        private String fileName;
        @SerializedName("duration")
        private int duration;

        public VideoRequest(String title, String description,
                            String fileName, int duration) {
            this.title = title;
            this.description = description;
            this.fileName = fileName;
            this.duration = duration;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }
    }
}
