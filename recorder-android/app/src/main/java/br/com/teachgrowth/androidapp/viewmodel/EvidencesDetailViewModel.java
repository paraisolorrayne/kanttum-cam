package br.com.teachgrowth.androidapp.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.Menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.helper.Utils;
import br.com.teachgrowth.androidapp.service.model.AttributesMetaResponse;
import br.com.teachgrowth.androidapp.service.model.IncludedCommentsModel;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.model.response.AttributesCommentsResponse;
import br.com.teachgrowth.androidapp.service.model.response.AttributesVideoResponse;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import br.com.teachgrowth.androidapp.ui.interfaces.CallbackBasicViewModel;
import br.com.teachgrowth.androidapp.ui.widget.Point;
import br.com.teachgrowth.androidapp.viewmodel.base.BaseViewModel;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucas on 03/04/18.
 */

public class EvidencesDetailViewModel extends BaseViewModel {
    public final ObservableField<String> videoTitle = new ObservableField<>();
    public final ObservableField<String> description = new ObservableField<>();
    public final ObservableField<String> videoDate = new ObservableField<>();
    public final ObservableField<List<Point>> pointsObservable = new ObservableField<>();
    public final ObservableField<Integer> timerVideo = new ObservableField<>(0);
    public final ObservableInt amountFeedback = new ObservableInt(0);
    public final ObservableBoolean playerIsPrepared = new ObservableBoolean(false);

    Menu menu;
    DataModel<List<AttributesCommentsResponse>,
            List<IncludedCommentsModel>, Object> lastResponse;
    String filterType;
    private List<Point> points = new ArrayList<>();
    private OfflineEvidenceModel evidenceModel;

    public EvidencesDetailViewModel(OfflineEvidenceModel evidenceModel, CallbackBasicViewModel callback) {
        super(callback);
        this.evidenceModel = evidenceModel;
        callApi();
        videoTitle.set(evidenceModel.getVideoTitle());
        description.set(evidenceModel.getVideoDescription());
    }

    public List<Point> getPoints() {
        return points;
    }

    private void callApi() {
        if (evidenceModel.getId() != 0) {
            Log.d("PATHSERVICE", String.valueOf(evidenceModel.getId()));
            RetrofitBase.getInterfaceRetrofit()
                    .getVideo(evidenceModel.getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onSuccessGetVideo,
                            this::handlerErrors,
                            () -> {
                            });
        }
    }

    private void onSuccessGetVideo(DataModel<AttributesVideoResponse, Object,
            AttributesMetaResponse.MetaResponse> responseModel) {

        AttributesVideoResponse.VideoResponse videoResponse = responseModel.getData().getVideoResponse();

        videoTitle.set(videoResponse.getTitle());
        description.set(videoResponse.getDescription());
        videoDate.set(String.format(callback.getString(R.string.video_date),
                Utils.parDate(videoResponse.getCreated_at()).replace("-", "às")));
    }

    private HashMap<Integer, IncludedCommentsModel> createHashAuthors(List<IncludedCommentsModel> includedCommentsModels) {
        HashMap<Integer, IncludedCommentsModel> authors = new HashMap<>();
        for (IncludedCommentsModel comment : includedCommentsModels) {
            if (comment.getType().equals("users")) {
                authors.put(comment.getId(), comment);
            }
        }
        return authors;
    }

    private HashMap<Integer, IncludedCommentsModel> createHashCriterias(List<IncludedCommentsModel> includedCommentsModels) {
        HashMap<Integer, IncludedCommentsModel> authors = new HashMap<>();
        for (IncludedCommentsModel comment : includedCommentsModels) {
            if (comment.getType().equals("criteria")) {
                authors.put(comment.getId(), comment);
            }
        }
        return authors;
    }

    private void handlerErrors(Throwable t) {
        showError(t, (dialog, which) -> {
        });
    }

    public String getVideoPath() {
        if (evidenceModel.getVideoPath() == null) {
            return evidenceModel.getVideoUrl();
        }
        return evidenceModel.getVideoPath();
    }

    public OfflineEvidenceModel getEvidenceModel() {
        return evidenceModel;
    }
}
