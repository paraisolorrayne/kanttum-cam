package br.com.teachgrowth.androidapp.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.JobIntentService;
import android.util.Log;
import android.util.Pair;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.app.Constants;
import br.com.teachgrowth.androidapp.app.TeachGrowthApplication;
import br.com.teachgrowth.androidapp.enums.EvidenceStatusEnum;
import br.com.teachgrowth.androidapp.helper.NotificationService;
import br.com.teachgrowth.androidapp.service.model.OfflineEvidenceModel;
import br.com.teachgrowth.androidapp.service.model.request.AttributeUploadRequest;
import br.com.teachgrowth.androidapp.service.model.request.AttributesCreateVideoRequest;
import br.com.teachgrowth.androidapp.service.model.request.DataModel;
import br.com.teachgrowth.androidapp.service.retrofit.RetrofitBase;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static br.com.teachgrowth.androidapp.helper.Utils.isMyServiceRunning;
import static br.com.teachgrowth.androidapp.helper.Utils.round;
import static br.com.teachgrowth.androidapp.helper.Utils.thereIsConnection;

/**
 * Created by lucas on 28/03/18.
 */
public class UploadVideoService extends JobIntentService {

    private static List<OfflineEvidenceModel> listEvidence;
    private static CognitoCachingCredentialsProvider sCredProvider;
    private static AmazonS3Client sS3Client;
    private static List<TransferObserver> observers;
    private static TransferUtility transferUtility;
    private static Context mContext;
    private static NotificationService progressNotification;
    private static Map<Integer, Pair<Boolean, Handler>> idHandlerMap = new HashMap<>();

    public UploadVideoService() {
        super();
    }

    public static List<TransferObserver> getObservers() {
        return observers == null ? new ArrayList<>() : observers;
    }

    public static void setObservers(List<TransferObserver> observers) {
        UploadVideoService.observers = observers;
    }

    public static TransferUtility getTransferUtility() {
        return transferUtility;
    }

    public static void prepairEvidencesDowload(Context context) {
        try {
            initTransfer(context);
            if (thereIsConnection(context)) {
                listEvidence = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
                Log.d("TESTETAREFA", "SIZE LISTEVIDENCE = " + (listEvidence != null ? listEvidence.size() : "null"));
                for (OfflineEvidenceModel evidence : listEvidence) {
                    if (evidence.getVideoStatus().equalsIgnoreCase(EvidenceStatusEnum.STATUS_NOT_STARTED.getId()) ||
                            evidence.getVideoStatus().equalsIgnoreCase(EvidenceStatusEnum.STATUS_FAILED_START_UPLOAD.getId())) {
                        evidence.setVideoStatus(EvidenceStatusEnum.STATUS_DOWNLOADING.getId());
                        TeachGrowthApplication.getInstance().updateUploadEvidence(evidence, Constants.Cycle);
                        startuploadVideo(evidence);
                    } else if (evidence.getVideoStatus().equalsIgnoreCase(EvidenceStatusEnum.STATUS_FAILED_CREATE_VIDEO.getId())) {
                        createVideoKanttum(evidence, getRequestCreateVideo(evidence));
                    }
                }
                resumeAllUploads();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    //create upload kanttum
    private static void startuploadVideo(OfflineEvidenceModel evidenceModel) {
        Log.v("TESTETAREFA", "Start evidence: " + evidenceModel.getVideoTitle());
        Log.v("upload_log", "Start evidence: " + evidenceModel.getVideoTitle());
        RetrofitBase.getInterfaceRetrofit()
                .uploads(getRequest("video.mp4"))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uploadResponseDataModel -> {
                    Log.v("upload_log", "Start Success evidence: " + evidenceModel.getVideoTitle());
                    evidenceModel.setAmazonInformations(uploadResponseDataModel.getData());
                    sendVideoToAmazon(evidenceModel);
                }, throwable -> {
                    Log.v("upload_log", "Start Error evidence: " + evidenceModel.getVideoTitle());
                    handlerErrors(evidenceModel);
                });
    }

    private static void sendVideoToAmazon(OfflineEvidenceModel evidence) {
        if (!checkIfEvidenceIsAlreadyInObserver(evidence)) {
            Log.d("TESTETAREFA", "sendToAmazon");
            Log.v("upload_log", "sendVideoToAmazon evidence: " + evidence.getVideoTitle());
            TransferObserver uploadObserver = transferUtility.upload(
                    Constants.BUCKET,
                    evidence.getAmazonInformations().getAttributes().getFileName(),
                    new File(evidence.getVideoPath()));

            uploadObserver.setTransferListener(getDownloadListener(evidence));

            observers.add(uploadObserver);
            evidence.setTransferObserverId(uploadObserver.getId());
            TeachGrowthApplication.getInstance().updateUploadEvidence(evidence, Constants.Cycle);


        }
        resumeAllUploads();
    }

    private static boolean checkIfEvidenceIsAlreadyInObserver(OfflineEvidenceModel evidenceObserver) {
        List<OfflineEvidenceModel> listEvidence = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        for (OfflineEvidenceModel evidenceModel : listEvidence) {
            if (evidenceModel.getVideoPath().equals(evidenceObserver.getVideoPath())) {
                if (evidenceModel.getTransferObserverId() > 0) {
                    if (evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FAILED_START_UPLOAD.getId())
                            || evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_NOT_STARTED.getId())
                            || evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_PAUSED.getId())) {
                        excludeUpload(evidenceModel);
                        return false;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    //create video kanttum
    private static void createVideoKanttum(OfflineEvidenceModel evidenceModel,
                                           DataModel<AttributesCreateVideoRequest, Object, Object> attributesCreateVideoRequestDataModel) {

        Log.v("upload_log", "createVideoKanttum evidence: " + evidenceModel.getVideoTitle());

        evidenceModel.setVideoStatus(EvidenceStatusEnum.STATUS_FINISHING.getId());
        TeachGrowthApplication.getInstance().updateUploadEvidence(evidenceModel, Constants.Cycle);
        TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidenceModel);
        RetrofitBase.getInterfaceRetrofit()
                .createVideo(attributesCreateVideoRequestDataModel)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(attributesVideoResponse -> {
                    Log.v("upload_log", "createVideoKanttum Success evidence: " + evidenceModel.getVideoTitle());

                    evidenceModel.setId(attributesVideoResponse.getData().getId());

                    checkVideoAvailability(evidenceModel);
                }, throwable -> {
                    Log.v("upload_log", "createVideoKanttum Success evidence: " + evidenceModel.getVideoTitle());
                    handlerErrors(evidenceModel);
                });
    }

    private static void resumeAllUploads() {
        listEvidence = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        for (TransferObserver transferObserver : observers) {
            if (transferObserver.getState() != TransferState.COMPLETED) {
                transferUtility.resume(transferObserver.getId());
                Log.d("TESTETAREFA", "resume SIZE LISTEVIDENCE = " + (listEvidence != null ? listEvidence.size() : "null"));
                for (OfflineEvidenceModel evidenceModel : listEvidence) {
                    if (evidenceModel.getVideoPath() != null && evidenceModel.getVideoPath().equals(transferObserver.getAbsoluteFilePath())) {
                        Log.v("upload_log", "reusme evidence: " + evidenceModel.getVideoTitle());
                        transferObserver.setTransferListener(getDownloadListener(evidenceModel));
                    }
                }
            }
        }
    }

    private static void pauseAllUploads() {
        transferUtility.pauseAllWithType(TransferType.UPLOAD);
    }

    //methods prepair s3 Amazon
    private static void initTransfer(Context context) {
        if (transferUtility == null) {
            transferUtility =
                    TransferUtility.builder()
                            .s3Client(getS3Client(context))
                            .context(context)
                            .build();
        }

        if (observers == null) {
            observers = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        }
    }

    private static CognitoCachingCredentialsProvider getCredProvider(Context context) {
        if (sCredProvider == null) {
            sCredProvider = new CognitoCachingCredentialsProvider(
                    context.getApplicationContext(),
                    Constants.COGNITO_POOL_ID,
                    Regions.US_EAST_1);
        }
        return sCredProvider;
    }

    private static AmazonS3Client getS3Client(Context context) {
        if (sS3Client == null) {
            sS3Client = new AmazonS3Client(getCredProvider(context.getApplicationContext()));
            sS3Client.setRegion(Region.getRegion(Regions.fromName(Constants.BUCKET_REGION)));
        }
        return sS3Client;
    }

    private static TransferListener getDownloadListener(OfflineEvidenceModel evidence) {
        return new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (thereIsConnection(mContext)) {
                    Log.v("upload_log", "sendVideoToAmazon evidence: " + evidence.getVideoTitle() + "status: " + state.name());
                    if (TransferState.COMPLETED == state) {
                        createNotification(evidence, TeachGrowthApplication.getInstance().getString(R.string.status_finishing));
                        createVideoKanttum(evidence, getRequestCreateVideo(evidence));
                    } else if (TransferState.WAITING_FOR_NETWORK == state) {
                        evidence.setVideoStatus(EvidenceStatusEnum.STATUS_PAUSED.getId());
                        TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidence);
                        TeachGrowthApplication.getInstance().updateUploadEvidence(evidence, Constants.Cycle);
                    } else if (TransferState.IN_PROGRESS == state) {
                        evidence.setVideoStatus(EvidenceStatusEnum.STATUS_DOWNLOADING.getId());
                        TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidence);
                        TeachGrowthApplication.getInstance().updateUploadEvidence(evidence, Constants.Cycle);
                    }
                } else {
                    evidence.setVideoStatus(EvidenceStatusEnum.STATUS_PAUSED.getId());
                    TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidence);
                    TeachGrowthApplication.getInstance().updateUploadEvidence(evidence, Constants.Cycle);
                }
                Log.v("upload", "id: " + id + " status: " + state.name());
            }

            @Override
            public void onProgressChanged(
                    int id, long bytesCurrent, long bytesTotal) {
                try {
                    if (thereIsConnection(mContext)) {
                        int percentage = (int) (100 * bytesCurrent / bytesTotal);
                        Log.d("upload_log:", "Filesize: " + bytesTotal / 1000000 + " mb.");
                        evidence.setPercentUploaded(percentage);
                        evidence.timeDuration = getTimeToFinish(evidence, bytesCurrent, bytesTotal);
                        TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidence);
                        if (progressNotification == null) {
                            createProgressNotification(evidence, "O upload está em andamento.");
                        } else {
                            updateProgressNotification(percentage);
                        }
//                        createNotification(evidence, "O upload está em andamento - " + percentage + "% concluido");
                        Log.v("upload_log", "onProgress observable: " + id + " evidence: " + evidence.getVideoTitle() + " percent:" + percentage);
                        if (percentage == 100) {
                            Log.v("upload_log", "100% progress evidence: " + evidence.getVideoTitle());
                        }
                    } else {
//                        new NotificationUtils_DEPRECATED(TeachGrowthApplication.getInstance()).clearNotification(evidence.getTransferObserverId());
                        new NotificationService(TeachGrowthApplication.getInstance()).clear(evidence.getTransferObserverId());
                        throw new Throwable("Wifi deve estar conectado!");
                    }
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.v("upload_log", "sendVideoToAmazon Error evidence: " + evidence.getVideoTitle());

//                new NotificationUtils_DEPRECATED(TeachGrowthApplication.getInstance()).clearNotification(evidence.getTransferObserverId());
                new NotificationService(TeachGrowthApplication.getInstance()).clear(evidence.getTransferObserverId());
                evidence.setVideoStatus(EvidenceStatusEnum.STATUS_FAILED_AMAZON.getId());
                TeachGrowthApplication.getInstance().updateUploadEvidence(evidence, Constants.Cycle);
                TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidence);
                Log.v("upload", "erro");
            }
        };
    }

    //creates request object to send to kantum
    private static DataModel<AttributeUploadRequest, Object, Object> getRequest(String fileName) {
        AttributeUploadRequest attributeUploadRequest =
                new AttributeUploadRequest(fileName);
        return new DataModel<>(attributeUploadRequest);
    }

    //creates video request object to send to kantum
    private static DataModel<AttributesCreateVideoRequest, Object, Object> getRequestCreateVideo(OfflineEvidenceModel evidenceModel) {
        AttributesCreateVideoRequest attributeUploadRequest =
                new AttributesCreateVideoRequest(evidenceModel.getVideoTitle(),
                        evidenceModel.getVideoDescription(),
                        evidenceModel.getAmazonInformations().getAttributes().getFileName(),
                        evidenceModel.getVideoDuration(),
                        evidenceModel.getAmazonInformations().getId()
                );
        return new DataModel<>(attributeUploadRequest);
    }

    private static void handlerErrors(OfflineEvidenceModel evidenceModel) {
        Log.v("TESTETAREFA", "ERROR");
        if (evidenceModel.getAmazonInformations() == null) {
            evidenceModel.setVideoStatus(EvidenceStatusEnum.STATUS_FAILED_START_UPLOAD.getId());
        } else {
            evidenceModel.setVideoStatus(EvidenceStatusEnum.STATUS_FAILED_CREATE_VIDEO.getId());
        }
        TeachGrowthApplication.getInstance().updateUploadEvidence(evidenceModel, Constants.Cycle);
        TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidenceModel);
        Log.v("upload", "erro");
    }

    private static void excludeUpload(OfflineEvidenceModel evidenceModel) {
        if (evidenceModel.getTransferObserverId() != 0) {
            transferUtility.pause(evidenceModel.getTransferObserverId());
            transferUtility.deleteTransferRecord(evidenceModel.getTransferObserverId());
            observers = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        }
    }

    public static void createNotification(OfflineEvidenceModel evidence, String content) {
        NotificationService notification = new NotificationService(TeachGrowthApplication.getInstance())
                .configure(evidence.getVideoTitle(), content);
        notification.configureId(evidence.getTransferObserverId());
        notification.show();
    }

    public static void createProgressNotification(OfflineEvidenceModel evidence, String content) {
        progressNotification = new NotificationService(TeachGrowthApplication.getInstance())
                .configure(evidence.getVideoTitle(), content);
        progressNotification.configureId(evidence.getTransferObserverId());
        progressNotification.configureProgressBar();
        progressNotification.show();
    }

    public static void updateProgressNotification(int progress) {
        progressNotification.updateProgress(progress);
    }

    @SuppressLint("DefaultLocale")
    public static String getTimeToFinish(OfflineEvidenceModel evidence, long bytesCurrent, long totalBytes) {
        String timeDuration = evidence.timeDuration == null ? "" : evidence.timeDuration;
        long time = System.currentTimeMillis();
        if (bytesCurrent == 0)
            return timeDuration;

        if (evidence.getCountPartOne() == null) {
            evidence.setCountPartOne(bytesCurrent);
            evidence.setTimePartOne(time);
        } else if (time - evidence.getTimePartOne() > 4000) {
            double deltaTime = time - evidence.getTimePartOne();
            double ms = deltaTime * (totalBytes - bytesCurrent) / (bytesCurrent - evidence.getCountPartOne());
            if (ms < 0)
                ;
            else if (ms / 3600000 > 1)
                timeDuration = String.format("%d horas para terminar", (int) round((ms / 3600000), 1) + 1);
            else if ((ms / 60000) % 60 > 1)
                timeDuration = String.format("%d minutos para terminar", (int) round((ms / 60000) % 60, 1) + 1);
            else
                timeDuration = String.format("%d segundos para terminar", (int) round((ms / 1000) % 60, 1));
            evidence.setCountPartOne(null);
        }
        return timeDuration;
    }

    public static void checkIfAllItemsAlready() {
        List<OfflineEvidenceModel> listEvidence = TeachGrowthApplication.getInstance().getUploadEvidences(Constants.Cycle);
        for (OfflineEvidenceModel evidenceModel : listEvidence)
            if (!evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHED.getId())) {
                TeachGrowthApplication.getInstance().allItemsAlreadySubject.onNext(false);
                Log.d("TESTETAREFA", "on next");
                return;
            }
        TeachGrowthApplication.getInstance().allItemsAlreadySubject.onNext(true);
    }

    private static Pair<Boolean, Handler> matchHandlerWithEvidence(OfflineEvidenceModel evidenceModel) {
        if (idHandlerMap.containsKey(evidenceModel.getId())) {
            return idHandlerMap.get(evidenceModel.getId());
        } else {
            idHandlerMap.put(evidenceModel.getId(), new Pair<>(false, new Handler()));
            return idHandlerMap.get(evidenceModel.getId());
        }
    }


    private static void checkVideoAvailability(OfflineEvidenceModel evidenceModel) {

        Pair<Boolean, Handler> booleanHandlerPair = matchHandlerWithEvidence(evidenceModel);
        final Boolean[] isPeriodicallyChecking = {booleanHandlerPair.first};
        Handler handler = booleanHandlerPair.second;

        RetrofitBase.getInterfaceRetrofit()
                .getVideoInformation(evidenceModel.getAmazonInformations().getAttributes().getFileName())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())//
                .subscribe(attributesVideoResponse -> {
                    if (!attributesVideoResponse.getData().isEmpty()) {
                        Log.d("resultado", attributesVideoResponse.getData().get(0).getVideoResponse().getTitle());
                        if (attributesVideoResponse.getData().get(0).getVideoResponse().getAvailable()) {
                            evidenceModel.setVideoStatus(EvidenceStatusEnum.STATUS_FINISHED.getId());
                            TeachGrowthApplication.getInstance().evidenceSubject.onNext(evidenceModel);
                            idHandlerMap.remove(evidenceModel.getId());
                        } else if (!isPeriodicallyChecking[0]) {
                            idHandlerMap.remove(evidenceModel.getId());
                            idHandlerMap.put(evidenceModel.getId(), new Pair<>(true, handler));
                            handler.removeCallbacks(() -> checkVideoAvailability(evidenceModel));
                            handler.postDelayed(() -> checkVideoAvailability(evidenceModel), 3 * 60 * 1000);
                        }
                    }
                }, throwable -> handlerErrors(evidenceModel));
    }

    @Override
    public void onCreate() {
        super.onCreate();

//        if (Build.VERSION.SDK_INT >= 26) {
//            String CHANNEL_ID = "upload";
//            String CHANNEL_NAME = "Recorder_Upload";
//
//            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
//                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
//            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
//
//            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
//                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.teachgrowth_icon).setPriority(PRIORITY_MIN).build();
//
//            startForeground(101, notification);
//        }
    }

    public void resumeFailedUpload(OfflineEvidenceModel evidenceModel) {
        if (thereIsConnection(this)) {
            Log.d("TESTETAREFA", "STATUS = " + evidenceModel.getVideoStatus() + " ENUM STATUS = " + EvidenceStatusEnum.STATUS_NOT_STARTED.getId() + " next = " + EvidenceStatusEnum.STATUS_FAILED_START_UPLOAD.getId());
            Log.d("TESTETAREFA", isMyServiceRunning(UploadVideoService.class, this) + " AND evidence SIZE = " + (listEvidence != null ? listEvidence.size() : "null") + " observers size = " + observers.size());
            if (evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FAILED_START_UPLOAD.getId())
                    || evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_NOT_STARTED.getId())
                    || evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_PAUSED.getId())) {
                Log.d("TESTETAREFA", "startUpload");
                startuploadVideo(evidenceModel);
            } else if (evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FAILED_AMAZON.getId())) {
                sendVideoToAmazon(evidenceModel);
            } else if (evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FAILED_CREATE_VIDEO.getId())) {
                createVideoKanttum(evidenceModel, getRequestCreateVideo(evidenceModel));
            } else if (evidenceModel.getVideoStatus().equals(EvidenceStatusEnum.STATUS_FINISHING.getId())) {
                checkVideoAvailability(evidenceModel);
            }
        }
        resumeAllUploads();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        if (intent != null) {
            onHandleWork(intent);
        }
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent restartService = new Intent(getApplicationContext(), this.getClass());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(restartService);
        } else {
            startService(restartService);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        prepairEvidencesDowload(getApplicationContext());
        mContext = getApplicationContext();

        Log.d("TESTETAREFA", "HandleIntent");

        TeachGrowthApplication.getInstance()
                .evidenceResumeSubject
                .subscribe(this::resumeFailedUpload);

        TeachGrowthApplication.getInstance()
                .deleteEvidence.subscribe(evidenceModel -> {
            if (observers != null) {
//                new NotificationUtils_DEPRECATED(TeachGrowthApplication.getInstance())
//                        .clearNotification(evidenceModel.getTransferObserverId());
//                excludeUpload(evidenceModel);

                new NotificationService(TeachGrowthApplication.getInstance()).clear(evidenceModel.getTransferObserverId());
                Intent clearIntent = new Intent("CLEAR_NOTIFICATION");
                clearIntent.putExtra("notificationId", evidenceModel.getVideoDuration());
                sendBroadcast(clearIntent);
            }
        });
    }

}
