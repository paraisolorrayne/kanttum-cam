package br.com.teachgrowth.androidapp.helper;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.genius.groupie.GroupAdapter;

import java.util.List;

import br.com.teachgrowth.androidapp.R;
import br.com.teachgrowth.androidapp.ui.widget.EditTextCustom;
import br.com.teachgrowth.androidapp.ui.widget.Point;
import br.com.teachgrowth.androidapp.ui.widget.VideoPlayer;
import de.hdodenhof.circleimageview.CircleImageView;
import rx.subjects.PublishSubject;

/**
 * Created by gabrielaraujo on 19/11/2017.
 */

public class DataBinder {

    private static final int TIMER_SEARCH = 1500;

    @BindingAdapter(value = "error")
    public static void setError(EditTextCustom editTextCustom, ErrorObservable errorObservable) {
        if (errorObservable != null) {
            editTextCustom.setError(errorObservable.get());
        }
    }
//
//    @BindingAdapter(value = "setMask")
//    public static void setMask(EditText editText, String pattener) {
//        editText.addTextChangedListener(Mask.insert(pattener, editText));
//    }

    @BindingAdapter(value = "adapter")
    public static void setAdapter(RecyclerView recyclerView, GroupAdapter groupAdapter) {
        recyclerView.setAdapter(groupAdapter);
    }

    @BindingAdapter(value = "adapterGrid2Column")
    public static void setAdaptergrid4Column(RecyclerView recyclerView, GroupAdapter groupAdapter) {
        GridLayoutManager layoutManager = new GridLayoutManager(recyclerView.getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(groupAdapter);
    }

    @BindingAdapter(value = "setDrawableTint")
    public static void setDrawableTint(TextView view, int color) {
        try {
            DrawableCompat.setTint(view.getCompoundDrawables()[0], ContextCompat.getColor(view.getContext(), color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter(value = "android:textColor")
    public static void setTextColor(TextView textView, int color) {
        textView.setTextColor(ContextCompat.getColor(textView.getContext(), color));
    }

    @BindingAdapter(value = "android:drawableLeft")
    public static void setDrawableLeft(TextView textView, int resDrawable) {
        try {
            Drawable drawable = ContextCompat.getDrawable(textView.getContext(), resDrawable);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        } catch (Exception e) {
            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }

    @BindingAdapter(value = "adapterNested")
    public static void setAdapterNested(RecyclerView recyclerView, GroupAdapter groupAdapter) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(groupAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @BindingAdapter(value = "app:setPoints")
    public static void setPoints(VideoPlayer videoView, List<Point> points) {
        videoView.setPoints(points);
    }

    @BindingAdapter(value = "android:visibility")
    public static void setVisible(View view, boolean isShow) {
        view.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(value = "setImageGalery")
    public static void setImage(ImageView image, String pathImage) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(image.getContext());
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(image.getContext(), R.color.colorPrimary));
        circularProgressDrawable.start();

        Glide.with(image.getContext())
                .load(pathImage)
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(circularProgressDrawable)
                )
                .into(image);

    }

    @BindingAdapter(value = "setImageGalery")
    public static void setImage(CircleImageView image, String pathImage) {
        Glide.with(image.getContext())
                .load(pathImage)
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_launcher_background))
                .into(image);

    }

    @BindingAdapter(value = "android:src")
    public static void setSrc(ImageView image, int pathImage) {
        image.setBackgroundResource(pathImage);
    }

    @BindingAdapter(value = "android:text")
    public static void setText(TextView textView, Integer resString) {
        if (textView != null && resString != null) {
            try {
                textView.setText(textView.getContext().getString(resString));
            } catch (Exception e) {
                textView.setText(String.valueOf(resString));
            }
        }
    }

    @BindingAdapter(value = "android:textSize")
    public static void setTextSize(Button button, Integer value) {
        button.setTextSize(TypedValue.COMPLEX_UNIT_SP, value);
    }

    @BindingAdapter(value = "setImeDone")
    public static void setImeDone(EditText editText, PublishSubject<View> publishSubject) {
        editText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == 6) {
                publishSubject.onNext(textView);
                return true;
            }
            return false;
        });
    }

    @BindingAdapter(value = "app:setCurrentTime")
    public static void setCurrentTime(br.com.teachgrowth.androidapp.ui.widget.VideoPlayer videoPlayer, int currentTime) {
        if (videoPlayer.getPlayerIsPrepared())
            videoPlayer.setCurrentTimer(currentTime);
    }
}
