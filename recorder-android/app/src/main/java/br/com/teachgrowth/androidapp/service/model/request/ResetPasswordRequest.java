package br.com.teachgrowth.androidapp.service.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 05/03/18.
 */

public class ResetPasswordRequest {

    @SerializedName("attributes")
    private Attributes attributes;

    public ResetPasswordRequest(String email) {
        this.attributes = new Attributes(email);
    }

    public class Attributes {

        @SerializedName("email")
        private String email;

        public Attributes(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

    }
}
